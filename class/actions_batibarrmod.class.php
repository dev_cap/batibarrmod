<?php
/* Copyright (C) 2021 SuperAdmin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file    batibarrmod/class/actions_batibarrmod.class.php
 * \ingroup batibarrmod
 * \brief   Example hook overload.
 *
 * Put detailed description here.
 */

/**
 * Class ActionsBatibarrmod
 */
$server = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : 'batibarr-bo.batiactu.com' ;
switch ($server) {
	case 'dev.batibarr.batiactugroupe.com':
	case 'dev.batibarr-16.batiactugroupe.com':
		define('DIR_HTTP','http://'.$_SERVER['SERVER_NAME'].'/');
		define('DIR_HTTP_BO','http://dev.batibarr-bo.batiactu.com/');
	break ;

	case 'batibarr.batiactu.space':
		define('DIR_HTTP','https://'.$_SERVER['SERVER_NAME'].'/');
		define('DIR_HTTP_BO','http://batibarr-bo.batiactu.space/');
	break ;

	case 'batibarr-old.batiactugroupe.com':
		define('DIR_HTTP','https://'.$_SERVER['SERVER_NAME'].'/');
		define('DIR_HTTP_BO','http://batibarr-bo-old.batiactu.com/');
	break ;

	default :
		define('DIR_HTTP','https://'.$_SERVER['SERVER_NAME'].'/');
		define('DIR_HTTP_BO','https://batibarr-bo.batiactu.info/');
}

class ActionsBatibarrmod
{
	/**
	 * @var DoliDB Database handler.
	 */
	public $db;

	/**
	 * @var string Error code (or message)
	 */
	public $error = '';

	/**
	 * @var array Errors
	 */
	public $errors = array();


	/**
	 * @var array Hook results. Propagated to $hookmanager->resArray for later reuse
	 */
	public $results = array();

	/**
	 * @var string String displayed by executeHook() immediately after return
	 */
	public $resprints;


	/**
	 * Constructor
	 *
	 *  @param		DoliDB		$db      Database handler
	 */
	public function __construct($db)
	{
		$this->db = $db;

		$mainmenu = GETPOST('mainmenu', 'aZ09');
		//echo "__construct | mainmenu: ".$mainmenu." | <br>";
		if ($mainmenu == 'agenda')
		{
			if (is_array($_GET))
			{
				foreach ($_GET as $key => $val)
				{
					if (!isset($_POST[$key]))
					{
						$_POST[$key] = $val ;
					}
					//echo '&'.$key.'='.urlencode($val)."<br>";
				}
			}
			//exit() ;
		}
	}


	/**
	 * Execute action
	 *
	 * @param	array			$parameters		Array of parameters
	 * @param	CommonObject    $object         The object to process (an invoice if you are in invoice module, a propale in propale's module, etc...)
	 * @param	string			$action      	'add', 'update', 'view'
	 * @return	int         					<0 if KO,
	 *                           				=0 if OK but we want to process standard actions too,
	 *                            				>0 if OK and we want to replace standard actions.
	 */
	public function getNomUrl($parameters, &$object, &$action)
	{
		global $db, $langs, $conf, $user;
		$this->resprints = '';
		/*echo "!! getNomUrl id: ".$object->id." | action: ".$action." | currentcontext: ".$parameters['currentcontext']." | option: ".(isset($parameters['option']) ? $parameters['option'] : 'customer')." !!<br>" ;
		echo "<textarea>".print_r($parameters, TRUE)."</textarea><br>" ;
		echo "<textarea>".$parameters['getnomurl']."</textarea><br>" ;
		echo "<textarea>".print_r($object, TRUE)."</textarea><br>" ;*/

		if ($parameters['currentcontext']=='propalcard'
			OR $parameters['currentcontext']=='invoicecard'
			OR $parameters['currentcontext']=='projectcard'
			OR $parameters['currentcontext']=='projectcontactcard'
			OR $parameters['currentcontext']=='projectOverview'
			OR $parameters['currentcontext']=='projectticket'
			OR $parameters['currentcontext']=='thirdpartydao'
		) {
			if (
				strpos($parameters['getnomurl'], '/comm/card.php?socid=')!==FALSE
				OR strpos($parameters['getnomurl'], '/societe/card.php?socid=')!==FALSE
				OR strpos($parameters['getnomurl'], '/societe/project.php?socid=')!==FALSE
			){

				$name = $object->name ? $object->name : $object->nom;
				if (!empty($object->name_alias)) {
					$name .= ' ('.$object->name_alias.')';
				}

				$linkstart = ''; $linkend = ''; $label = '';

				if (!empty($object->logo) && class_exists('Form')) {
					$label .= '<div class="photointooltip">';
					$label .= Form::showphoto('societe', $object, 0, 40, 0, '', 'mini', 0); // Important, we must force height so image will have height tags and if image is inside a tooltip, the tooltip manager can calculate height and position correctly the tooltip.
					$label .= '</div><div style="clear: both;"></div>';
				} elseif (!empty($object->logo_squarred) && class_exists('Form')) {
				}

				$label .= '<div class="centpercent">';

				$label .= img_picto('', $object->picto).' <u class="paddingrightonly">';

				$option = isset($parameters['option']) ? $parameters['option'] : 'customer' ;

				if ($option == 'customer' || $option == 'compta' || $option == 'category') {
					$label .= $langs->trans("Customer");
					$linkstart = '<a href="'.DOL_URL_ROOT.'/comm/card.php?socid='.$object->id;
				} elseif ($option == 'prospect' && empty($conf->global->SOCIETE_DISABLE_PROSPECTS)) {
					$label .= $langs->trans("Prospect");
					$linkstart = '<a href="'.DOL_URL_ROOT.'/comm/card.php?socid='.$object->id;
				} elseif ($option == 'supplier' || $option == 'category_supplier') {
					$label .= $langs->trans("Supplier");
					$linkstart = '<a href="'.DOL_URL_ROOT.'/fourn/card.php?socid='.$object->id;
				} elseif ($option == 'agenda') {
					$label .= $langs->trans("ThirdParty");
					$linkstart = '<a href="'.DOL_URL_ROOT.'/societe/agenda.php?socid='.$object->id;
				} elseif ($option == 'project') {
					$label .= $langs->trans("ThirdParty");
					$linkstart = '<a href="'.DOL_URL_ROOT.'/societe/project.php?socid='.$object->id;
				} elseif ($option == 'margin') {
					$label .= $langs->trans("ThirdParty");
					$linkstart = '<a href="'.DOL_URL_ROOT.'/margin/tabs/thirdpartyMargins.php?socid='.$object->id.'&type=1';
				} elseif ($option == 'contact') {
					$label .= $langs->trans("ThirdParty");
					$linkstart = '<a href="'.DOL_URL_ROOT.'/societe/contact.php?socid='.$object->id;
				} elseif ($option == 'ban') {
					$label .= $langs->trans("ThirdParty");
					$linkstart = '<a href="'.DOL_URL_ROOT.'/societe/paymentmodes.php?socid='.$object->id;
				}
				$label .= '</u>';

				// By default
				if (empty($linkstart)) {
					$label .= $langs->trans("ThirdParty").'</u>';
					$linkstart = '<a href="'.DOL_URL_ROOT.'/societe/card.php?socid='.$object->id;
				}

				if (isset($object->status)) {
					$label .= ' '.$object->getLibStatut(5);
				}

				if (!empty($object->name)) {
					$label .= '<br><b>'.$langs->trans('Name').':</b> '.dol_escape_htmltag($object->name);
					if (!empty($object->name_alias)) {
						$label .= ' ('.dol_escape_htmltag($object->name_alias).')';
					}
				}

				if (!empty($object->country_code)) {
					if ($object->country=='') $country = $langs->trans('Country'.$object->country_code) ;
					else $country = $object->country ;
					$label .= '<br><b>'.$langs->trans('Country').':</b> '.$country.' ('.$object->country_code.')';
				}
				if (empty($object->tva_intra) AND empty($object->idprof2) AND $object->element=='societe') {
					$soc = new societe($db);
					$soc->fetch($object->id);

					$object->tva_intra = $soc->tva_intra ;
					$object->idprof2 = $soc->idprof2 ;
				}

				if (!empty($object->tva_intra) ) {
					$label .= '<br><b>'.$langs->trans('VATIntra').':</b> '.dol_escape_htmltag($object->tva_intra);
				}
				if (!empty($object->idprof2)) {
					$label .= '<br><b>Siret:</b> '.$object->idprof2;
				}
				$label .= '</div>';

				// Add type of canvas
				$linkstart .= (!empty($object->canvas) ? '&canvas='.$object->canvas : '');
				// Add param to save lastsearch_values or not
				$add_save_lastsearch_values = ($save_lastsearch_value == 1 ? 1 : 0);
				if ($save_lastsearch_value == -1 && preg_match('/list\.php/', $_SERVER["PHP_SELF"])) {
					$add_save_lastsearch_values = 1;
				}
				if ($add_save_lastsearch_values) {
					$linkstart .= '&save_lastsearch_values=1';
				}
				$linkstart .= '"';

				$linkclose = '';
				if (empty($notooltip)) {
					if (!empty($conf->global->MAIN_OPTIMIZEFORTEXTBROWSER)) {
						$label = $langs->trans("ShowCompany");
						$linkclose .= ' alt="'.dol_escape_htmltag($label, 1).'"';
					}
					$linkclose .= ' title="'.dol_escape_htmltag($label, 1).'"';
					$linkclose .= ' class="classfortooltip refurl"';

				}
				$linkstart .= $linkclose.'>';
				$linkend = '</a>';

				$result = '';
				$result .= $linkstart;
				$result .= img_object($label, ($object->picto ? $object->picto : 'generic'), 'class="paddingright classfortooltip"', 0, 0, $notooltip ? 0 : 1);
				$result .= dol_escape_htmltag($name);
				$result .= $linkend;

				//echo $result ;
				$this->resprints = $result ;
				return 1;
			}
		}
		return 0;
	}

	public function createPropalAuto($parameters, $id_project) {
		global $conf, $user, $langs;
		require_once DOL_DOCUMENT_ROOT.'/comm/propal/class/propal.class.php';
		require_once DOL_DOCUMENT_ROOT.'/core/class/extrafields.class.php';
		require_once DOL_DOCUMENT_ROOT.'/projet/class/project.class.php';

		$myproject = new Project($this->db);
		$result = $myproject->fetch($id_project);
		$id_soc = (int)$myproject->socid ;
		$ba_support_prj = $myproject->array_options['options_ba_support'] ;
		$tab_contact_int = $myproject->liste_contact(-1, 'internal');
		$tab_contact_ext = $myproject->liste_contact(-1, 'external');
		//echo "<pre>".print_r($tab_contact_int, TRUE) ; exit() ;

		$object = new Propal($this->db);
		$extrafields = new ExtraFields($this->db);

		// fetch optionals attributes and labels
		$extrafields->fetch_name_optionals_label($object->table_element);
		$datep = dol_mktime(12, 0, 0, date('n'), date('d'), date('Y'));

		//$object->ref = '';
		$object->ref_client = '';
		$object->datep = $datep;
		$object->date = $datep;
		$object->socid = $id_soc;
		$object->fk_project = $id_project;
		$object->author = $user->id; // deprecated
		$object->fk_bank = $object->fk_account;
		$object->setDocModel($user, 'batiactu');
		//$object->setDocModel($user, 'batiactu_en');

		$object->array_options["options_ba_support"] = $ba_support_prj;
		$object->demand_reason_id = $myproject->array_options['options_demand_reason'] ;
		$object->array_options["options_echeancier"] = $myproject->array_options['options_echeancier'] ;

		$id = $object->create($user);
		//$ret = $object->fetch_thirdparty();
		if ($id>0) {
			//var_dump($myproject->status) ;
			if ($myproject->status==0) {
				$result = $myproject->setValid($user);
			}
			//$result = $object->add_contact($user->id, 'SALESREPFOLL', 'internal'); // pas ajout de user en cours
			$TType_interne = array(
				'PROJECTLEADER'=>'SALESREPFOLL', // Commercial project leader - 161 => 31 - Commercial sales rep foll
				'PROJECTCONTRIBUTOR'=>'SALESREPFOLL', // Client - 160 => 31 - Commercial sales rep foll
				'SALESREPFOLL'=>'SALESREPFOLL',
			) ;

			foreach ($tab_contact_int as $contacttoadd)
			{
				$new_type = (isset($TType_interne[ $contacttoadd['code'] ])) ? $TType_interne[ $contacttoadd['code'] ] : 'SALESREPFOLL' ;
				$result = $object->add_contact($contacttoadd['id'], $new_type, $contacttoadd['source']);
				//echo "add to propal id: ".$id.", id int: ".$contacttoadd['id']." - ".$contacttoadd['code']." => ".$new_type." - res: ".$result." - ".print_r($contacttoadd, TRUE)." |<br />\r\n" ;
			}

			$TType_externe = array(
				'CUSTOMER'=>'CUSTOMER', // Contact client suivi proposition - 171 => 41 - Contact client suivi proposition
				'PROJECTLEADER'=>'CUSTOMER', // Commercial - 170 => 41 - Contact client suivi proposition
				'BILLING'=>'BILLING', // Contact client facturation - 192 => 40 - Contact client facturation
				'PROJECTCONTRIBUTOR'=>'CUSTOMER', // Client - 171 => 41 - Contact client suivi proposition // old
			) ;
			foreach ($tab_contact_ext as $contacttoadd)
			{
				$new_type = (isset($TType_externe[ $contacttoadd['code'] ])) ? $TType_externe[ $contacttoadd['code'] ] : 'CUSTOMER' ;
				$result = $object->add_contact($contacttoadd['id'], $new_type, $contacttoadd['source']);
				//echo "add to propal id: ".$id.", id ext: ".$contacttoadd['id']." - ".$contacttoadd['code']." => ".$new_type." - res: ".$result." - ".print_r($contacttoadd, TRUE)." |<br />\r\n" ;
			}
			//exit() ;

			//$result = $object->generateDocument($object->model_pdf, $langs);
			//setEventMessages($langs->trans("FileGenerated").' | '.$result, null);

			header('Location: '.$_SERVER["PHP_SELF"].'?id='.$id.'');
			exit();

			echo "<hr>CREATE PROPAL".$id." (id_project: ".$id_project." | id_soc: ".$id_soc.") !<br>" ;
			echo "**model_pdf2: ".$object->model_pdf."<br>" ;
		}
		else
		{
			echo "<hr>ERREUR CREATE (id_project: ".$id_project." | id_soc: ".$id_soc.") !<br>" ;
			var_dump($object->error);
		}
		echo "<hr>" ;
		var_dump($object->errors);
		echo "<hr><pre>" ;
		var_dump($object);
		exit();
	}

	public function addMoreActionsButtons($parameters, &$object, &$action, $hookmanager)
	{
		// calcul total annee
		if (($action=='view' OR $action=='') && $parameters['currentcontext']=='thirdpartycard')
		{

			if ( GETPOST('socid') > 0 ) {
				$TAnnee = array() ;
				$TAnnee[date('Y') ]['nb'] = 0;
				$TAnnee[date('Y') ]['montant'] = 0;
				$TAnnee[(date('Y')-1)]['nb'] = 0 ;
				$TAnnee[(date('Y')-1)]['montant'] = 0 ;

				$sql_calc = 'SELECT prj_ext.amount_real as amount_real, prj.fk_opp_status as fk_opp_status, prj_ext.date_signe as date_signe' ;
				$sql_calc.= ' FROM '.MAIN_DB_PREFIX.'projet_extrafields prj_ext JOIN '.MAIN_DB_PREFIX.'projet prj ON prj_ext.fk_object=prj.rowid' ;
				$sql_calc.= ' WHERE prj.fk_opp_status=6 AND prj.fk_soc='.GETPOST('socid') ;
				$resql = $this->db->query($sql_calc);
				if ($resql)
				{
					while ($obj_calc = $this->db->fetch_object($resql)) {
						if ($obj_calc->fk_opp_status==6)
						{
							$year_signe = (int)substr($obj_calc->date_signe,0,4) ;
							if ($year_signe==date('Y')) // 2020-12-09
							{
								$TAnnee[ date('Y') ]['montant']+= (float)$obj_calc->amount_real ;
								$TAnnee[ date('Y') ]['nb']++ ;
							}
							else if ($year_signe==(date('Y')-1)) // 2020-12-09
							{
								$TAnnee[ (date('Y')-1) ]['montant']+= (float)$obj_calc->amount_real ;
								$TAnnee[ (date('Y')-1) ]['nb']++ ;
							}
						}
					}
				}

				//echo $action.' ! addMoreActionsButtons ! id: '.GETPOST('socid').' | '.$sql.'<br>';
				//exit() ;
				$js_calcul = "\n";
				$js_calcul.= '<script type="text/javascript">'."\n" ;
				$js_calcul.= '$(document).ready(function () {'."\n" ;
				//$js_calcul.= '  var col_left = $("#id-right .tabBar .fichehalfleft").html();'."\n" ;
				//$js_calcul.= '  col_left+= "<div>Année 2";'."\n" ;
				//$js_calcul.= '  col_left+= "<div class=\"underbanner clearboth\"></div>";'."\n" ;
				//$js_calcul.= '  $("#id-right .tabBar .fichehalfleft").html(col_left);'."\n" ;
				foreach ($TAnnee as $annne=>$data)
				{
					$js_calcul.= '  $("#id-right .tabBar .fichehalfleft .tableforfield").append("<tr><td>CA '.$annne.'</td><td title=\"Nombre affaires signé : '.$data['nb'].'\">'.number_format($data['montant'],2,',',' ').' €</td></tr>");'."\n" ;
				}
				$js_calcul.= '});'."\n" ;
				$js_calcul.= "</script>\n";
				echo $js_calcul;
			}
		}
	}

	public function printFieldListWhere($parameters, &$object, $hookmanager)
	{
		global $conf, $user, $langs, $sql;
		//if ($user->admin==1) echo "******************** printFieldListWhere currentcontext: ".$parameters['currentcontext']." - user id: ".$user->id." ************<br>\n" ;
		if ($parameters['currentcontext']=='projectlist')
		{
			//printFieldListWhere

			if (isset($_REQUEST['search_options_date_signe']) && $_REQUEST['search_options_date_signe']!='')
			{
				if (strlen($_REQUEST['search_options_date_signe'])==7)
				{
					list($m, $y) = explode('/', $_REQUEST['search_options_date_signe']) ;
					$time_filter = strtotime($y.'-'.$m.'-01') ;
					$new_filter = 'ef.date_signe BETWEEN \''.date('Y-m-', $time_filter).'01 00:00:00\' AND \''.date('Y-m-', $time_filter).date('t', $time_filter).' 23:59:59\'' ;

					$sql.= " AND ".$new_filter ;
					//if ($user->admin==1) echo "".$new_filter." | ".$sql." |<br>" ;//exit() ;
				}
				/*else {
					list($d, $m, $y) = explode('/', $_REQUEST['search_options_date_signe']) ;
					$time_filter = strtotime($y.'-'.$m.'-'.$d) ;
					$key_filter = "ef.date_signe = '".date('Y-m-d', $time_filter)." 00:00:00'" ;

					if ($user->admin==1) echo "".$_REQUEST['search_options_date_signe']." | ".$time_filter." | ".$key_filter." | ".$sql." | <br>" ;//exit() ;

					if (strpos($sql, $key_filter)!== FALSE)
					{
						$new_filter = 'ef.date_signe BETWEEN \''.date('Y-m-', $time_filter).'01 00:00:00\' AND \''.date('Y-m-', $time_filter).date('t', $time_filter).' 23:59:59\'' ;

						$sql = strtr($sql, array($key_filter=>$new_filter)) ;
						if ($user->admin==1) echo "".$key_filter." | ".$new_filter." | ".$sql." |<br>" ;//exit() ;
					}
				}*/
			}
			if (isset($_REQUEST['search_options_date_fin_contrat']) && $_REQUEST['search_options_date_fin_contrat']!='')
			{
				if (strlen($_REQUEST['search_options_date_fin_contrat'])==7)
				{
					list($m, $y) = explode('/', $_REQUEST['search_options_date_fin_contrat']) ;
					$time_filter = strtotime($y.'-'.$m.'-01') ;
					$new_filter = 'ef.date_fin_contrat BETWEEN \''.date('Y-m-', $time_filter).'01 00:00:00\' AND \''.date('Y-m-', $time_filter).date('t', $time_filter).' 23:59:59\'' ;

					$sql.= " AND ".$new_filter ;
					//if ($user->admin==1) echo "".$new_filter." | ".$sql." |<br>" ;//exit() ;
				}
			}
		}

	}

	/**
	 * Overloading the doActions function : replacing the parent's function with the one below
	 *
	 * @param   array           $parameters     Hook metadatas (context, etc...)
	 * @param   CommonObject    $object         The object to process (an invoice if you are in invoice module, a propale in propale's module, etc...)
	 * @param   string          $action         Current action (if set). Generally create or edit or null
	 * @param   HookManager     $hookmanager    Hook manager propagated to allow calling another hook
	 * @return  int                             < 0 on error, 0 on success, 1 to replace standard code
	 */
	public function doActions($parameters, &$object, &$action, $hookmanager)
	{
		global $conf, $user, $langs;
		global $menumanager;

		$error = 0; // Error counter
		$in_context = FALSE ;
		$context_current = '' ;
		$TContext = (isset($parameters['context']) AND $parameters['context']!='') ? explode(':', $parameters['context']) : array() ;

		$TList_page = array(
			'index',
			'accountancycustomerlist',
			'comptafileslist',
			'categoryindex',
			'thirdpartiesindex',
			'thirdpartylist',
			'thirdpartycard',
			'thirdpartycontact',
			'thirdpartydao',
			'consumptionthirdparty',
			'thirdpartyticket',
			'thirdpartycomm',
			'projectthirdparty',
			'thirdpartynotification',
			'thirdpartynote',
			'thirdpartydocument',
			'agendathirdparty',
			'propallist',
			'propalcard',
			'projectsindex',
			'productindex',
			'projectlist',
			'projectcard',
			'projectcontactcard',
			'projectticket',
			'projectOverview',
			'projecttaskscard',
			'projectnote',
			'projectdocument',
			'projectinfo',
			'contactlist',
			'contactcard',
			'consumptioncontact',
			'sendinbluecontactcard',
			'contactnote',
			'contactdocument',
			'contactagenda',
			'usercard',
			'productcard',
			'productstatsinvoice',
			'productpricecard',
			'productstatscard',
			'agenda',
			'agendalist',
			'mailingcard',
			'ticketsindex',
			'ticketlist',
			'ticketcard',
			'mail',
			'sendMail',
			'invoicelist',
			'invoicecard',
			'paymentlist',
			'actioncard',
			//'globallist',
			//'globalcard',
		) ;

		foreach ($TContext as $context)
		{
			if (in_array($context, $TList_page))	    // do something only for the context 'somecontext1' or 'somecontext2'
			{
				$in_context = TRUE ;
				if ($context!=$parameters['currentcontext']) $context_current = $context ;
			}
		}

		if ($user->admin==1)
		{
			//echo "******************** doActions currentcontext: ".$parameters['currentcontext'].($in_context ? ' OR Context: '.$context_current : '')." - action: ".$action." - GETPOST action: ".GETPOST('action')."".(isset($massaction) ? ' - massaction: '.$massaction : '')." - user id: ".$user->id." - id responsable".$user->fk_user." ************<br>\n" ;
		}
		//echo "doAction page: ".$parameters['currentcontext']." | action: ".$action."<br>" ;
		/* print_r($parameters); print_r($object); echo "action: " . $action; */
		if ($in_context || in_array($parameters['currentcontext'], $TList_page))	    // do something only for the context 'somecontext1' or 'somecontext2'
		{
			// Do what you want here...
			// You can for example call global vars like $fieldstosearchall to overwrite them, or update database depending on $action and $_POST values.

			/*if ($parameters['currentcontext']=='thirdpartiesindex')
			{
				header("Location: ".dol_buildpath('/societe/list.php',1)."?leftmenu=thirdparties");
				exit();
			}
			if ($parameters['currentcontext']=='projectsindex')
			{
				header("Location: ".dol_buildpath('/projet/list.php',1)."?leftmenu=projets&search_status=99");
				exit();
			}
			if ($parameters['currentcontext']=='productindex')
			{
				header("Location: ".dol_buildpath('/product/list.php',1)."?leftmenu=service&type=1");
				exit();
			}*/

			//if ($parameters['currentcontext']=='projectcard') { var_dump( $menumanager );exit(); }
			//if ($parameters['currentcontext']=='thirdpartycard') {print_r($object) ;exit() ;}
			if ($action=='create' && $parameters['currentcontext']=='thirdpartycard')
			{
				//$object->client = 2 ;
				$_POST["type"] = 'p' ;
				//print_r($object) ;exit() ;
			}
			if ($action=='presend' && $parameters['currentcontext']=='thirdpartycard')
			{

			}
			if ($action=='create' && ($context_current=='ticketcard' OR $parameters['currentcontext']=='ticketcard') )
			{
				$id_prj_ticket = (int)GETPOST('originid', 'int') ;
				if ($id_prj_ticket > 0)
				{
					global $socid ; // variable de la page ticket/card.php
					$id_soc_ticket = (int)GETPOST('socid', 'int') ;
					//echo "# new ticket ".$object->fk_soc." | originid ".$id_prj_ticket." ok | socid: ".$id_soc_ticket." #<hr>" ;
					if ($id_soc_ticket == 0)
					{
						$myproject = new Project($this->db);
						$result = $myproject->fetch($id_prj_ticket);
						$id_soc_ticket = (int)$myproject->socid ;
						if ($id_soc_ticket > 0)
						{
							$_GET['socid'] = $id_soc_ticket ;
							$_REQUEST['socid'] = $id_soc_ticket ;
							//echo "# new ticket socid ".$id_soc." | ".$id_soc_ticket." #<hr>" ;
						}
					}
					$socid = $id_soc_ticket;
				}
				else
				{
					$url = dol_buildpath('/projet/list.php',1).'?mainmenu=project&leftmenu=&sortfield=p.ref&sortorder=asc&contextpage=projectlist&search_opp_status=6' ;
					header('Location: '.$url);
					//echo $url ;
					exit();
				}

			}
			if ($action=='create' && $parameters['currentcontext']=='contactcard')
			{
				//echo "doAction socid: ".$_GET['socid']." | action: ".$action."<br>" ;
				if (isset($_GET['socid']) && $_GET['socid']>0) {
					// on a une societe
				}
				else {
					//var_dump($object);exit();
					//GETPOSTISSET('socid') ;
					$lib_filtre = '&mainmenu=companies' ;

					setEventMessage("Vous devez choisir une société", "warnings");
					header("Location: ".dol_buildpath('/societe/list.php',1)."?leftmenu=thirdparties".$lib_filtre);
					exit() ;
				}
			}

			if ($parameters['currentcontext']=='thirdpartycomm')
			{
				if (GETPOST('socid', 'aZ09') > 0) {
					// on a une societe
					if ($user->admin!=1) { // non admin
						header("Location: ".dol_buildpath('/societe/card.php',1)."?socid=".GETPOST('socid', 'aZ09'));
						exit() ;
					}
				}
				else {
					setEventMessage("Pas de société trouvé", "warnings");
					header("Location: ".dol_buildpath('/societe/list.php',1)."?mainmenu=companies&leftmenu=thirdparties");
					exit() ;
				}
			}

			if ($action=='update' && $parameters['currentcontext']=='projectcard')
			{
				$echeancier_projet = $object->array_options['options_echeancier'] ;
				if (isset($_REQUEST['options_echeancier'])) $echeancier_projet = $_REQUEST['options_echeancier'] ;
				//echo "doAction socid: ".$_REQUEST['socid']." (".$object->socid.") | id projet: ".$object->id." | ".$echeancier_projet." | action: ".$action."<br>" ;
				$this->set_echancier_all_extra($object->id, $echeancier_projet) ;

				//echo "<pre>".print_r($object, TRUE)."</pre><hr />\n" ;
				//exit() ;
			}

			if ($action=='update_extras' && $parameters['currentcontext']=='invoicecard')
			{
				$echeancier_projet = $object->array_options['options_echeancier'] ;
				if (isset($_REQUEST['options_echeancier'])) $echeancier_projet = $_REQUEST['options_echeancier'] ;
				//echo "doAction id facture: ".$object->id." | affaire: ".$object->fk_project." | ".$echeancier_projet." | action: ".$action."<br>" ;

				$this->set_echancier_all_extra($object->fk_project, $echeancier_projet) ;
				//echo "<pre>".print_r($object, TRUE)."</pre><hr />\n" ;
				//exit() ;
			}

			if ($action=='update_extras' && $parameters['currentcontext']=='propalcard')
			{
				$echeancier_projet = $object->array_options['options_echeancier'] ;
				if (isset($_REQUEST['options_echeancier'])) $echeancier_projet = $_REQUEST['options_echeancier'] ;
				//echo "doAction id propal: ".$object->id." | affaire: ".$object->fk_project." | ".$echeancier_projet." | action: ".$action."<br>" ;

				$this->set_echancier_all_extra($object->fk_project, $echeancier_projet) ;
				//echo "<pre>".print_r($object, TRUE)."</pre><hr />\n" ;
				//exit() ;
			}

			if ($action=='create' && $parameters['currentcontext']=='projectcard')
			{
				//echo "doAction socid: ".$_GET['socid']." | action: ".$action."<br>" ;
				if (isset($_GET['socid']) && $_GET['socid']>0) {
					// on a une societe
				}
				else {
					//var_dump($object);exit();
					//GETPOSTISSET('socid') ;
					$lib_filtre = '&mainmenu=companies' ;

					setEventMessage("Vous devez choisir une société", "warnings");
					header("Location: ".dol_buildpath('/societe/list.php',1)."?leftmenu=thirdparties".$lib_filtre);
					exit() ;
				}
			}
			/*if ($action=='confirm_close' && $parameters['currentcontext']=='projectcard')
			{
				$confirm = GETPOST('confirm', 'aZ09');
				if ($confirm == 'yes')
				{
					$motif_perdu = GETPOST('motif_perdu', 'aZ09');
					echo "* action:".$action." | currentcontext:".$parameters['currentcontext']." | confirm:".$confirm." | motif_perdu:".$motif_perdu." | opp_status:".$object->opp_status ;
					exit() ;
					$object->opp_status = 7 ; // perdu
					$object->opp_percent = 0 ; // perdu
					$object->date_end = dol_mktime(0, 0, 0, date('n'), date('j'), date('Y')); // perdu
					//$result = $object->setClose($user);
					//if ($result <= 0) setEventMessages($object->error, $object->errors, 'errors');
				}
			}*/

			if ($action=='add' && $parameters['currentcontext']=='projectcard')
			{
				if (isset($_REQUEST['error']) && $_REQUEST['error'] > 0)
				{
					// error in creation project
				} else
				{
					// create the project
					//echo "<pre>".print_r( $object, TRUE)."</pre><br>\r\n";
					//exit();
				}
			}

			if ($action=='create' && $parameters['currentcontext']=='propalcard')
			{
				$object->public = 1 ;
				//echo "doAction origin: ".$_GET['origin']." | id: ".$_GET['originid']." | action: ".$action."<br>" ;
				if (isset($_GET['origin']) && $_GET['origin']=='project' && isset($_GET['originid']) && $_GET['originid']>0) {
					// on a un projet
					$this->createPropalAuto($parameters, $_GET['originid']) ;
				}
				else {
					//var_dump($object);exit();
					//GETPOSTISSET('socid') ;
					$lib_filtre = '&mainmenu=project' ;
					if (GETPOSTISSET('socid') && $_GET['socid'] > 0) {
						$lib_filtre = '&socid='.$_GET['socid'] ;
					}

					setEventMessage("Vous devez choisir une affaire", "warnings");
					header("Location: ".dol_buildpath('/projet/list.php',1)."?leftmenu=projets".$lib_filtre);
					exit() ;
				}
			}
			else if ($action=='add' && $parameters['currentcontext']=='propalcard')
			{
				// Validation automatique du projet lors de la creation de la proposition
				$id_prj = GETPOST('projectid', 'int');
				if ($id_prj > 0) {
					require_once DOL_DOCUMENT_ROOT.'/projet/class/project.class.php';
					$myproject = new Project($this->db);
					$result = $myproject->fetch($id_prj);
					if ($result < 0) {
						$this->errors[] = $myproject->error;
					}
					else {
						//var_dump($myproject->status) ;
						if ($myproject->status==0) {
							$result = $myproject->setValid($user);
						}
					}
					//var_dump($id_prj) ;
				}
				//var_dump($action) ; exit() ;
			}
			else if ($action=='confirm_closeas' && $parameters['currentcontext']=='propalcard')
			{
				if (!(GETPOST('statut', 'int') > 0))
				{
					setEventMessages($langs->trans("ErrorFieldRequired", $langs->transnoentitiesnoconv("CloseAs")), null, 'errors');
					$action = 'closeas';
				} else {
					// prevent browser refresh from closing proposal several times
					if ($object->statut == $object::STATUS_VALIDATED)
					{
						$var_statut = GETPOST('statut', 'int') ;
						//var_dump($object::STATUS_SIGNED);
						//var_dump($var_statut);

						$this->db->begin();
						if ($var_statut==$object::STATUS_NOTSIGNED) { // analyse des autres propal uniquement si signe
							//close_project_refus
							if (GETPOST('close_project_refus', 'string') =='on')
							{
								//echo "NOT SIGNED REFUSED | ".$_GET['motif_perdu']."<br>" ;
								if ($_GET['motif_perdu']=='' || $_GET['motif_perdu']==-1
								) {
									$url_back_empty = "../propal/card.php?id=".$_GET['id']."&action=closeas&nosign" ;
									if ($_GET['close_project_refus']!='') $url_back_empty.= '&close_project_refus='.urlencode($_GET['close_project_refus']) ;
									if ($_GET['motif_perdu']!='') $url_back_empty.= '&motif_perdu='.urlencode($_GET['motif_perdu']) ;

									setEventMessage("Vous devez saisir tous les champs obligatoires", "warnings");
									header("Location: ".$url_back_empty);
									exit() ;
								}
								//var_dump($_GET);exit();
							}
							else
							{
								//echo "NOT SIGNED NOT REFUSED<br>" ;
								//var_dump($_GET);exit();
							}
						}
						else if ($var_statut==$object::STATUS_SIGNED) { // analyse des autres propal uniquement si signe
							//&action=closeas&sign
							if ($_GET['date_signe']==''
								|| $_GET['type_contrat']<0
								|| $_GET['echeancier']==''
								|| $_GET['ca_annee']==''
								|| $_GET['duree_contrat']<0
								|| $_GET['date_fin_contrat']==''
							) {
								$url_back_empty = "../propal/card.php?id=".$_GET['id']."&action=closeas&sign" ;
								//if ($_GET['date_signe']!='') $url_back_empty.= '&date_signe='.urlencode($_GET['date_signe']) ;
								if ($_GET['date_signeday']!='') $url_back_empty.= '&date_signeday='.urlencode($_GET['date_signeday']) ;
								if ($_GET['date_signemonth']!='') $url_back_empty.= '&date_signemonth='.urlencode($_GET['date_signemonth']) ;
								if ($_GET['date_signeyear']!='') $url_back_empty.= '&date_signeyear='.urlencode($_GET['date_signeyear']) ;

								if ($_GET['type_contrat']!='') $url_back_empty.= '&type_contrat='.urlencode($_GET['type_contrat']) ;
								if ($_GET['echeancier']!='') $url_back_empty.= '&echeancier='.urlencode($_GET['echeancier']) ;
								if ($_GET['ca_annee']!='') $url_back_empty.= '&ca_annee='.urlencode($_GET['ca_annee']) ;
								if ($_GET['duree_contrat']!='') $url_back_empty.= '&duree_contrat='.urlencode($_GET['duree_contrat']) ;

								//if ($_GET['date_fin_contrat']!='') $url_back_empty.= '&date_fin_contrat='.urlencode($_GET['date_fin_contrat']) ;
								if ($_GET['date_fin_contratmonth']!='') $url_back_empty.= '&date_fin_contratmonth='.urlencode($_GET['date_fin_contratmonth']) ;
								if ($_GET['date_fin_contratday']!='') $url_back_empty.= '&date_fin_contratday='.urlencode($_GET['date_fin_contratday']) ;
								if ($_GET['date_fin_contratyear']!='') $url_back_empty.= '&date_fin_contratyear='.urlencode($_GET['date_fin_contratyear']) ;

								if ($_GET['note_private']!='') $url_back_empty.= '&note_private='.urlencode($_GET['note_private']) ;
								if ($_GET['ref_client']!='') $url_back_empty.= '&ref_client='.urlencode($_GET['ref_client']) ;
								if ($_GET['particularite_facture']!='') $url_back_empty.= '&particularite_facture='.urlencode($_GET['particularite_facture']) ;

								setEventMessage("Vous devez saisir tous les champs obligatoires", "warnings");
								header("Location: ".$url_back_empty);
								exit() ;
							}
						}
						//exit();
						if ($var_statut==$object::STATUS_SIGNED // analyse des autres propal si signe
							|| ($var_statut==$object::STATUS_NOTSIGNED && GETPOST('close_project_refus', 'string') =='on') // analyse des autres propal si non signe et perdu
						) {
							//var_dump($_GET);exit();
							foreach ($_GET as $key=>$val) {
								if (substr($key,0,7)=='propal_') {
									$idpropal = substr($key,7) ;
									//var_dump($idpropal) ;
									if ($object->id==$idpropal) {

									}
									else {
										if ($val=='on') {
											$othersPropal = new Propal($this->db);
											$result = $othersPropal->fetch($idpropal);
											if ($result < 0) {
												$this->errors[] = $othersPropal->error;
												$error++;
											} else {
												//var_dump($othersPropal->rowid.' | propal statut: '.$othersPropal->statut.' | STATUS_DRAFT: '.$othersPropal::STATUS_DRAFT);
												if ($othersPropal->statut == $othersPropal::STATUS_DRAFT) $othersPropal->valid($user);

												if (substr(DOL_VERSION,0,2)=='13') $result = $othersPropal->cloture($user, Propal::STATUS_NOTSIGNED, 'Fermer Auto');
												else $result = $othersPropal->closeProposal($user, Propal::STATUS_NOTSIGNED, 'Fermer Auto');

												if ($result < 0) {
													$this->errors[] = $othersPropal->error;
													$error++;
												}
											}
										}
									}
								}
							}
						}

						if (substr(DOL_VERSION,0,2)=='13') $result = $object->cloture($user, $var_statut, ''); //GETPOST('note_private', 'restricthtml')
						else $result = $object->closeProposal($user, $var_statut, ''); //GETPOST('note_private', 'restricthtml')

						if ($result < 0)
						{
							setEventMessages($object->error, $object->errors, 'errors');
							$error++;
						}

						if (!$error)
						{
							$this->db->commit();
						} else {
							$this->db->rollback();
						}
					}
				}
			}
			// Validation
			elseif ($action == 'confirm_validate' && $parameters['currentcontext']=='propalcard')
			{
				// Validation automatique du projet lors de la validation de la proposition
				$confirm = GETPOST('confirm', 'alpha');
				$usercancreate = $user->rights->propal->creer;
				$usercanvalidate = ((empty($conf->global->MAIN_USE_ADVANCED_PERMS) && $usercancreate) || (!empty($conf->global->MAIN_USE_ADVANCED_PERMS) && !empty($user->rights->propal->propal_advance->validate)));
				//var_dump($confirm.' | '.$usercanvalidate) ;
				if ($confirm == 'yes' && $usercanvalidate)
				{
					require_once DOL_DOCUMENT_ROOT.'/projet/class/project.class.php';
					$myproject = new Project($this->db);
					$result = $myproject->fetch($object->fk_project);
					if ($result < 0) {
						$this->errors[] = $myproject->error;
					}
					else {
						if ($myproject->status==0) {
							$result = $myproject->setValid($user);
						}
					}
				}
			}
		}

		if (!$error) {
			$this->results = array('myreturn' => 999);
			$this->resprints = 'A text to show';
			return 0; // or return 1 to replace standard code
		} else {
			$this->errors[] = 'Error message';
			return -1;
		}
	}

	public function sendMail($parameters, &$object, &$action, $hookmanager)
	{
		global $conf, $user, $langs;

		$TList_support = array() ;
		$sql = 'SELECT code, label FROM '.MAIN_DB_PREFIX.'c_ba_support' ;
		$resql = $this->db->query($sql);
		if ($resql)
		{
			$num = $this->db->num_rows($resql);
			while ($i < $num)
			{
				$obj = $this->db->fetch_object($resql);
				$TList_support[$obj->code] = $obj->label ;
				$i++;
			}
		}

		/*if ($user->admin==1) {
			echo "sendMail !<br />\n" ;
			echo "<pre>".print_r($TList_support, TRUE)."</pre><hr />\n" ;
			echo "<pre>".print_r($object, TRUE)."</pre><hr />\n" ;
		}*/

		$object->smtps->setSubject( strtr($object->subject, $TList_support) ) ;

		$msg = $object->html;
		$msg = strtr($msg, $TList_support) ;
		$msg = preg_replace('/(\r|\n)\.(\r|\n)/ims', '\1..\2', $msg);

		if ($object->msgishtml) $object->smtps->setBodyContent($msg, 'html');
		else $object->smtps->setBodyContent($msg, 'plain');

		return 0 ;
		//exit();
	}

	/**
	 * Overloading the doMassActions function : replacing the parent's function with the one below
	 *
	 * @param   array           $parameters     Hook metadatas (context, etc...)
	 * @param   CommonObject    $object         The object to process (an invoice if you are in invoice module, a propale in propale's module, etc...)
	 * @param   string          $action         Current action (if set). Generally create or edit or null
	 * @param   HookManager     $hookmanager    Hook manager propagated to allow calling another hook
	 * @return  int                             < 0 on error, 0 on success, 1 to replace standard code
	 */
	public function doMassActions($parameters, &$object, &$action, $hookmanager)
	{
		global $conf, $user, $langs;

		$error = 0; // Error counter

		/* print_r($parameters); print_r($object); echo "action: " . $action; */
		if (in_array($parameters['currentcontext'], array('somecontext1', 'somecontext2')))		// do something only for the context 'somecontext1' or 'somecontext2'
		{
			foreach ($parameters['toselect'] as $objectid)
			{
				// Do action on each object id
			}
		}

		if (!$error) {
			$this->results = array('myreturn' => 999);
			$this->resprints = 'A text to show';
			return 0; // or return 1 to replace standard code
		} else {
			$this->errors[] = 'Error message';
			return -1;
		}
	}


	/**
	 * Overloading the addMoreMassActions function : replacing the parent's function with the one below
	 *
	 * @param   array           $parameters     Hook metadatas (context, etc...)
	 * @param   CommonObject    $object         The object to process (an invoice if you are in invoice module, a propale in propale's module, etc...)
	 * @param   string          $action         Current action (if set). Generally create or edit or null
	 * @param   HookManager     $hookmanager    Hook manager propagated to allow calling another hook
	 * @return  int                             < 0 on error, 0 on success, 1 to replace standard code
	 */
	public function addMoreMassActions($parameters, &$object, &$action, $hookmanager)
	{
		global $conf, $user, $langs;

		$error = 0; // Error counter

		/* print_r($parameters); print_r($object); echo "action: " . $action; */
		if (in_array($parameters['currentcontext'], array('somecontext1', 'somecontext2')))		// do something only for the context 'somecontext1' or 'somecontext2'
		{
			$this->resprints = '<option value="0"'.($disabled ? ' disabled="disabled"' : '').'>'.$langs->trans("BatibarrmodMassAction").'</option>';
		}

		if (!$error) {
			return 0; // or return 1 to replace standard code
		} else {
			$this->errors[] = 'Error message';
			return -1;
		}
	}



	/**
	 * Execute action
	 *
	 * @param	array	$parameters     Array of parameters
	 * @param   Object	$object		   	Object output on PDF
	 * @param   string	$action     	'add', 'update', 'view'
	 * @return  int 		        	<0 if KO,
	 *                          		=0 if OK but we want to process standard actions too,
	 *  	                            >0 if OK and we want to replace standard actions.
	 */
	public function beforePDFCreation($parameters, &$object, &$action)
	{
		global $conf, $user, $langs;
		global $hookmanager;

		$outputlangs = $langs;

		$ret = 0; $deltemp = array();
		dol_syslog(get_class($this).'::executeHooks action='.$action);

		/* print_r($parameters); print_r($object); echo "action: " . $action; */
		if (in_array($parameters['currentcontext'], array('somecontext1', 'somecontext2')))		// do something only for the context 'somecontext1' or 'somecontext2'
		{
		}

		return $ret;
	}

	/**
	 * Execute action
	 *
	 * @param	array	$parameters     Array of parameters
	 * @param   Object	$pdfhandler     PDF builder handler
	 * @param   string	$action         'add', 'update', 'view'
	 * @return  int 		            <0 if KO,
	 *                                  =0 if OK but we want to process standard actions too,
	 *                                  >0 if OK and we want to replace standard actions.
	 */
	public function afterPDFCreation($parameters, &$pdfhandler, &$action)
	{
		global $conf, $user, $langs;
		global $hookmanager;

		$outputlangs = $langs;

		$ret = 0; $deltemp = array();
		dol_syslog(get_class($this).'::executeHooks action='.$action);

		/* print_r($parameters); print_r($object); echo "action: " . $action; */
		if (in_array($parameters['currentcontext'], array('somecontext1', 'somecontext2')))		// do something only for the context 'somecontext1' or 'somecontext2'
		{
		}

		return $ret;
	}



	/**
	 * Overloading the loadDataForCustomReports function : returns data to complete the customreport tool
	 *
	 * @param   array           $parameters     Hook metadatas (context, etc...)
	 * @param   string          $action         Current action (if set). Generally create or edit or null
	 * @param   HookManager     $hookmanager    Hook manager propagated to allow calling another hook
	 * @return  int                             < 0 on error, 0 on success, 1 to replace standard code
	 */
	public function loadDataForCustomReports($parameters, &$action, $hookmanager)
	{
		global $conf, $user, $langs;

		$langs->load("batibarrmod@batibarrmod");

		$this->results = array();

		$head = array();
		$h = 0;

		if ($parameters['tabfamily'] == 'batibarrmod') {
			$head[$h][0] = dol_buildpath('/module/index.php', 1);
			$head[$h][1] = $langs->trans("Home");
			$head[$h][2] = 'home';
			$h++;

			$this->results['title'] = $langs->trans("Batibarrmod");
			$this->results['picto'] = 'batibarrmod@batibarrmod';
		}

		$head[$h][0] = 'customreports.php?objecttype='.$parameters['objecttype'].(empty($parameters['tabfamily']) ? '' : '&tabfamily='.$parameters['tabfamily']);
		$head[$h][1] = $langs->trans("CustomReports");
		$head[$h][2] = 'customreports';

		$this->results['head'] = $head;

		return 1;
	}

	/**
	 * Overloading the restrictedArea function : check permission on an object
	 *
	 * @param   array           $parameters     Hook metadatas (context, etc...)
	 * @param   string          $action         Current action (if set). Generally create or edit or null
	 * @param   HookManager     $hookmanager    Hook manager propagated to allow calling another hook
	 * @return  int                             <0 if KO,
	 *                                          =0 if OK but we want to process standard actions too,
	 *                                          >0 if OK and we want to replace standard actions.
	 */
	public function restrictedArea($parameters, &$action, $hookmanager)
	{
		//echo "!! restrictedArea module<br />" ;
		global $user;

		if ($parameters['features'] == 'myobject') {
			if ($user->rights->batibarrmod->myobject->read) {
				$this->results['result'] = 1;
				return 1;
			} else {
				$this->results['result'] = 0;
				return 1;
			}
		}

		return 0;
	}

	public function formConfirm($parameters, &$object, &$action, $hookmanager) {
		global $langs, $conf;
		//$parameters['formConfirm'];
		//propalcard

		$form = new Form($this->db);
		$TContext = (isset($parameters['context']) AND $parameters['context']!='') ? explode(':', $parameters['context']) : array() ;

		//echo "test: ".$parameters['currentcontext']."<br /><pre>".print_r($TContext, TRUE)."" ;exit();

		/*if (in_array($parameters['currentcontext'], array('projectcard')))	    // do something only for the context 'somecontext1' or 'somecontext2'
		{
			if ($action == 'close')
			{
				$text = '' ;
				$TList_motif = array(
					'AUTRE'=>"Autre ou sans explication",
					'ABANDON'=>"projet de communication abandonné",
					'BUDGET'=>"Manque de budget",
					'CONCURRENT'=>"Choix d'un concurrent",
					'INTERLOCUTEUR'=>"Changement d'interlocuteur",
				) ;

				$formquestion = array();
				$formquestion[] = array('type' => 'other', 'tdclass'=>'for_cloture tagtd_cloture', 'value'=>'<br>', 'label' => $langs->trans("ConfirmCloseAProject")) ;
				$formquestion[] = array('type' => 'select', 'tdclass'=>'more_info_perdu', 'name' => 'motif_perdu', 'label' => 'Motif', 'values' => $TList_motif, 'default' => '') ;

				$formconfirm = $form->formconfirm($_SERVER["PHP_SELF"]."?id=".$object->id, $langs->trans("CloseAProject"), $text, "confirm_close", $formquestion, '', 1);

				$this->resprints =  $formconfirm ;
				return 1;
			}
		}*/

		if (in_array($parameters['currentcontext'], array('invoicecard')) OR array_search('invoicecard', $TContext)!==FALSE)	    // do something only for the context 'somecontext1' or 'somecontext2'
		{
			//echo "action: ".$action." | param currentcontext: ".$parameters['currentcontext']."<br />\r\n param context: <pre>".print_r($TContext, TRUE)."</pre><br />\r\n" ;
			if ($action == 'valid')
			{
				if ($object->mode_reglement_id == 0)
				{
					//setEventMessage("Vous devez choisir un mode de règlement", "warnings");
					//header("Location: ".dol_buildpath('/compta/facture/card.php',1)."?facid=".$object->id);
					//return 1;

				}
				//exit($object->mode_reglement_id) ;
				if ($object->cond_reglement_id == 0 && $object->type!=2)
				{
					setEventMessage("Vous devez choisir les conditions de règlement", "warnings");
					return 1;

				}
				//exit($object->cond_reglement_id) ;
			}
		}

		if (in_array($parameters['currentcontext'], array('propalcard')))	    // do something only for the context 'somecontext1' or 'somecontext2'
		{
			//var_dump($action);
			if ($action == 'validate')
			{
			//var_dump($parameters);exit();
			//var_dump($object);exit();
				if ($object->cond_reglement_id == 0)
				{
					setEventMessage("Vous devez choisir les conditions de règlement", "warnings");
					return 1;
				}
				//exit($object->cond_reglement_id) ;
				$error = 0;

				// We verify whether the object is provisionally numbering
				$ref = substr($object->ref, 1, 4);
				$ref_prj = '';
				if ($ref == 'PROV') {
					$numref = $object->getNextNumRef($soc);
					if (empty($numref)) {
						$error++;
						setEventMessages($object->error, $object->errors, 'errors');
					}
				} else {
					$numref = $object->ref;
				}

				require_once DOL_DOCUMENT_ROOT.'/projet/class/project.class.php';
				$myproject = new Project($this->db);
				$result = $myproject->fetch($object->fk_project);
				if ($result < 0) {
					$this->errors[] = $myproject->error;
					$error++;
				}

				if (empty($error)) {
					//var_dump($myproject);echo"<hr>" ;
					//var_dump($myproject->status.' | '.$myproject::STATUS_VALIDATED);exit();
					if ($myproject->status==0) {
						$ref_prj = $myproject->ref ;
					}
				}

				$text = $langs->trans('ConfirmValidateProp', $numref);
				if ($ref_prj!='') {
					$text.= '<br>Le projet <b>'.$ref_prj.'</b> sera validé également.<br>';
				}
				if (!empty($conf->notification->enabled)) {
					require_once DOL_DOCUMENT_ROOT.'/core/class/notify.class.php';
					$notify = new Notify($this->db);
					$text.= '<br>';
					$text.= $notify->confirmMessage('PROPAL_VALIDATE'
						, $object->socid
						, $object);
				}

				if (!$error)
					$formconfirm = $form->formconfirm($_SERVER["PHP_SELF"].'?id='.$object->id, $langs->trans('ValidateProp'), $text, 'confirm_validate', '', 0, 1, 350);

				$this->resprints =  $formconfirm ;
				return 1;
			}

			if ($action == 'closeas' OR $action == 'statut')
			{
				$nb_propal_plus = 0 ;
				$height_pop = 250 ;
				$text = '' ;
				//Form to close proposal (signed or not)
				if(isset($_REQUEST['sign'])) $def_statut = $object::STATUS_SIGNED ;
				else if(isset($_REQUEST['nosign'])) $def_statut = $object::STATUS_NOTSIGNED ;
				else $def_statut = '' ;
				$def_note = (isset($_GET['note_private']) && $_GET['note_private']!='') ? $_GET['note_private'] : '' ;
				$def_ref_client = (isset($_GET['ref_client']) && $_GET['ref_client']!='') ? $_GET['ref_client'] : $object->ref_client ;
				$formquestion = array(
					array('type' => 'select', 'name' => 'statut', 'label' => '<span class="fieldrequired">'.$langs->trans("CloseAs").'</span>', 'values' => array($object::STATUS_SIGNED => $object->LibStatut($object::STATUS_SIGNED), $object::STATUS_NOTSIGNED => $object->LibStatut($object::STATUS_NOTSIGNED)), 'default'=>$def_statut), //
					//array('type' => 'text', 'name' => 'note_private', 'label' => "Note", 'value' => $def_note),				// Field to complete private note (not replace)
					//array('type' => 'text', 'name' => 'note_private_2', 'label' => $langs->trans("Note"), 'value' => 't')	,			// Field to complete private note (not replace)
				);
				require_once DOL_DOCUMENT_ROOT.'/projet/class/project.class.php';
				$myproject = new Project($this->db);
				$result = $myproject->fetch($object->fk_project);
				if ($result < 0) {
					$this->errors[] = $myproject->error;
					$error++;
				}
				$formcloture = array() ;

				//var_dump($action);exit();

				if (empty($error)) {
					//$TList_contrat = array('Standard', 'Tacite', 'Partenaire') ;
					$extrafields = new ExtraFields($this->db);
					$extrafields->fetch_name_optionals_label('projet'); // fetch optionals attributes and labels
					//echo "<pre>".print_r($extrafields->attributes['projet']['param']['type_contrat']['options'], TRUE)."</pre>" ;
					//var_dump($extrafields->attributes['projet']);exit();

					$TList_contrat = $extrafields->attributes['projet']['param']['type_contrat']['options'] ;
					$TList_duree = array(1=>'1 mois', 3=>'3 mois', 6=>'6 mois', 9=>'9 mois', 12=>'12 mois', 24=>'24 mois', 36=>'36 mois') ;
					$TList_annee = array() ;
					if (date('n')==1) $end_year = date('Y')-1 ;
					else $end_year = date('Y') ;
					for ($ind = (date('Y')+1) ; $ind>=$end_year ; $ind--) $TList_annee[$ind] = $ind ;

					//if (isset($_GET['date_signe']) && $_GET['date_signe']!='') $myproject->array_options['options_date_signe'] = $_GET['date_signe'] ;
					if (isset($_GET['date_signeyear']) && $_GET['date_signeyear']!='') $myproject->array_options['options_date_signe'] = dol_mktime(0, 0, 0, GETPOST('date_signemonth', 'int'), GETPOST('date_signeday', 'int'), GETPOST('date_signeyear', 'int'));
					if (isset($_GET['type_contrat']) && $_GET['type_contrat']!='') $myproject->array_options['options_type_contrat'] = $_GET['type_contrat'] ;
					if (isset($_GET['ca_annee']) && $_GET['ca_annee']!='') $myproject->array_options['options_ca_annee'] = $_GET['ca_annee'] ;
					if (isset($_GET['particularite_facture']) && $_GET['particularite_facture']!='') $myproject->array_options['options_particularite_facture'] = $_GET['particularite_facture'] ;
					if (isset($_GET['duree_contrat']) && $_GET['duree_contrat']!='') $myproject->array_options['options_duree_contrat'] = $_GET['duree_contrat'] ;
					//if (isset($_GET['date_fin_contrat']) && $_GET['date_fin_contrat']!='') $myproject->array_options['options_date_fin_contrat'] = $_GET['date_fin_contrat'] ;
					if (isset($_GET['date_fin_contratyear']) && $_GET['date_fin_contratyear']!='') $myproject->array_options['options_date_fin_contrat'] = dol_mktime(0, 0, 0, GETPOST('date_fin_contratmonth', 'int'), GETPOST('date_fin_contratday', 'int'), GETPOST('date_fin_contratyear', 'int')); ;
					if (isset($_GET['echeancier']) && $_GET['echeancier']!='') {
						$myproject->array_options['options_echeancier'] = GETPOST('echeancier', 'string');
					}
					else {
						$myproject->array_options['options_echeancier'] = $object->array_options['options_echeancier'] ;
					}

					$TList_motif = array() ;

					$sql_motif = "SELECT rowid, code, label";
					$sql_motif.= " FROM ".MAIN_DB_PREFIX.'c_ba_motif_perdu';
					$sql_motif.= " WHERE active=1 ORDER BY position ASC";
					$resql = $this->db->query($sql_motif);
					if ($resql)
					{
						while ($obj_motif = $this->db->fetch_object($resql)) {
							$TList_motif[$obj_motif->code] = $obj_motif->label ;
						}
					}

					$formquestion[] = array('type' => 'checkbox', 'tdclass'=>'more_info_nosigne', 'name' => 'close_project_refus', 'label' => 'L\'affaire est perdue', 'value' => ((isset($_GET['close_project_refus']) && $_GET['close_project_refus']=='on') ? 1 : 0)) ;
					$formquestion[] = array('type' => 'select', 'tdclass'=>'more_info_nosigne_perdu fieldrequired', 'name' => 'motif_perdu', 'label' => 'Motif', 'values' => $TList_motif, 'default' => '') ;

					//echo "<pre>".print_r($myproject, TRUE)."</pre>" ;exit();
					$formquestion[] = array('type' => 'text', 'tdclass'=>'more_info_signe', 'name' => 'ref_client', 'label' => "Référence client", 'value' => $def_ref_client) ;				// Field to complete private note (not replace)
					//$formquestion[] = array('type' => 'other', 'tdclass'=>'more_info_signe tagtd_cloture', 'value'=>'<br><br>', 'label' => "<hr><b>Merci de renseigner</b>") ;
					$formquestion[] = array('type' => 'date', 'tdclass'=>'more_info_signe fieldrequired', 'name' => 'date_signe', 'label' => 'Date de signature', 'value' => ($myproject->array_options['options_date_signe']=='' ? time() : $myproject->array_options['options_date_signe'])) ;
					$height_pop+= 29 ;
					$formquestion[] = array('type' => 'select', 'tdclass'=>'more_info_signe fieldrequired', 'name' => 'type_contrat', 'label' => 'Type de contrat', 'values' => $TList_contrat, 'default' => ($myproject->array_options['options_type_contrat']=='' ? '' : $myproject->array_options['options_type_contrat'])) ;
					$height_pop+= 29 ;
					$formquestion[] = array('type' => 'text', 'tdclass'=>'more_info_signe fieldrequired', 'name' => 'echeancier', 'label' => 'Echéancier de Facturation', 'value' => ($myproject->array_options['options_echeancier']=='' ? '' : $myproject->array_options['options_echeancier'])) ;
					$height_pop+= 29 ;
					$formquestion[] = array('type' => 'select', 'tdclass'=>'more_info_signe fieldrequired', 'name' => 'ca_annee', 'label' => 'CA facturé sur', 'values' => $TList_annee, 'default' => ($myproject->array_options['options_ca_annee']=='' ? date('Y') : $myproject->array_options['options_ca_annee']), 'select_show_empty' => 0) ;
					$height_pop+= 29 ;
					$formquestion[] = array('type' => 'select', 'tdclass'=>'more_info_signe fieldrequired', 'name' => 'duree_contrat', 'label' => 'Durée contrat en mois', 'values' => $TList_duree, 'default' => ($myproject->array_options['options_duree_contrat']=='' ? date('Y') : $myproject->array_options['options_duree_contrat'])) ;
					$height_pop+= 29 ;
					$formquestion[] = array('type' => 'date', 'tdclass'=>'more_info_signe fieldrequired', 'name' => 'date_fin_contrat', 'label' => 'Date de fin de contrat', 'value' => ($myproject->array_options['options_date_fin_contrat']=='' ? '' : $myproject->array_options['options_date_fin_contrat'])) ;
					$height_pop+= 29 ;
					$formquestion[] = array('type' => 'text', 'tdclass'=>'more_info_signe', 'name' => 'particularite_facture', 'label' => 'Particularité de Facturation', 'values' => $myproject->array_options['options_particularite_facture'], 'size'=>'20" maxlength="255') ;
					$height_pop+= 29 ;
					$formquestion[] = array('type' => 'checkbox', 'tdclass'=>'more_info_signe', 'name' => 'signe_tacite_reconduction', 'label' => 'Tacite reconduction', 'value' => ((isset($_GET['signe_tacite_reconduction']) && $_GET['signe_tacite_reconduction']=='on') ? 1 : 0)) ;
					$height_pop+= 29 ;

					$elementarray = $myproject->get_element_list('propal', 'propal', 'datep', '', '',  'fk_projet');
					if (!is_array($elementarray) && $elementarray < 0) {
						$this->errors[] = $this->db->lasterror;
						$error++;
					} else {

						$TStatut_propal = array($object::STATUS_DRAFT => $object->LibStatut($object::STATUS_DRAFT), $object::STATUS_VALIDATED => $object->LibStatut($object::STATUS_VALIDATED), $object::STATUS_SIGNED => $object->LibStatut($object::STATUS_SIGNED), $object::STATUS_NOTSIGNED => $object->LibStatut($object::STATUS_NOTSIGNED)) ;
						$Tid_propal_check = array() ;
						foreach ($_GET as $key=>$val) {
							if (substr($key, 0, 7) == 'propal_') {
								$Tid_propal_check[substr($key, 7, 1)] = $val;
							}
						}

						foreach ($elementarray as $key=>$data) {
							$tmp = explode('_', $data);
							$idofelement = $tmp[0];
							if ($object->id==$idofelement) { // current propal

							}
							else {
								$othersPropal = new Propal($this->db);
								$result = $othersPropal->fetch($idofelement);
								if ($result < 0) {
									$this->errors[] = $othersPropal->error;
									$error++;
								} else {
									$checked = 1 ;
									//var_dump($Tid_propal_check);exit();
									if (isset($Tid_propal_check[$othersPropal->id]) && $Tid_propal_check[$othersPropal->id]=='') {
										$checked = 0 ;
									}
									if ($nb_propal_plus==0) {
										//$formquestion[] = array('type' => 'onecolumn', 'value' => "<hr>Sélectionner les éléments à cloturer automatiquement") ;
									}
									$lib_propal_other = $othersPropal->ref ;
									if ($othersPropal->ref_client!='') {
										if ($lib_propal_other!='') $lib_propal_other.= ' - ' ;
										$lib_propal_other.= $othersPropal->ref_client ;
									}
									if ($lib_propal_other!='') $lib_propal_other.= ' - ' ;
									$lib_propal_other.= isset($TStatut_propal[$othersPropal->statut]) ? $TStatut_propal[$othersPropal->statut] : $othersPropal->statut ;
									$lib_propal_other = $this->_str_cut($lib_propal_other.' '.$lib_propal_other) ;

									//$formquestion[] = array('type' => 'text', 'name' => 'propal_'.$idofelement, 'label' => $langs->trans("propal"), 'value' => $idofelement) ;
									$formcloture[] = array('type' => 'checkbox', 'tdclass'=>'for_cloture tagtd_cloture', 'name' => 'propal_'.$othersPropal->id, 'label' => $lib_propal_other, 'value' => $checked, 'morecss'=>'propal_sign_plus mask_chp') ;
									$nb_propal_plus++ ;
								}
							}
						}
					}
				}

				$formquestion[] = array('type' => 'other', 'tdclass'=>'for_cloture tagtd_cloture', 'value'=>'<br><br>', 'label' => "<hr><b>L'affaire ".$myproject->ref." va être automatiquement clôturée</b>") ;
				$height_pop+= 19.3 ;
				if ($nb_propal_plus>0) {
					$formquestion[] = array('type' => 'other', 'tdclass'=>'for_cloture tagtd_cloture', 'value'=>'<br><br>', 'label' => "<hr><b>Ces propositions seront clôturées automatiquement en perdu</b>") ;
					$formquestion = array_merge($formquestion, $formcloture);
					$height_pop+= 50 ;
					$height_pop+= 19.3 * $nb_propal_plus ;
				}

				$js_cloture = "\n";
				$js_cloture.= '<script type="text/javascript">'."\n" ;
				$js_cloture.= '$(document).ready(function () {'."\n" ;
				$js_cloture.= '  $(".propal_sign_plus").parent().addClass("tagtd_empty");'."\n" ;
				//$js_cloture.= '  $(".for_cloture").parent().addClass("mask_chp");'."\n" ;
				//$js_cloture.= '  alert( $("#statut option:selected").val() );'."\n" ;
				$js_cloture.= '  $("#statut").change( function() {'."\n" ;
				$js_cloture.= '    select_statut_change();'."\n" ;
				$js_cloture.= '    select_nosigne();'."\n" ;
				$js_cloture.= '  });'."\n" ;
				$js_cloture.= '  $("#close_project_refus").change( function() {'."\n" ;
				$js_cloture.= '    select_statut_change();'."\n" ;
				$js_cloture.= '    select_nosigne();'."\n" ;
				$js_cloture.= '  });'."\n" ;
				$js_cloture.= '  select_statut_change();'."\n" ;
				$js_cloture.= '  select_nosigne();'."\n" ;
				$js_cloture.= '});'."\n" ;
				$js_cloture.= '  function select_statut_change() {'."\n" ;
				$js_cloture.= '    var stat_sel = $("#statut option:selected").val() ;'."\n" ;
				//$js_cloture.= '    alert( stat_sel );'."\n" ;
				$js_cloture.= '    $(".more_info_nosigne").parent().addClass("mask_chp");'."\n" ;
				$js_cloture.= '    $(".more_info_nosigne_perdu").parent().addClass("mask_chp");'."\n" ;
				//$js_cloture.= '    $(".more_info_nons").parent().addClass("mask_chp");'."\n" ;
				$js_cloture.= '    $(".more_info_signe").parent().addClass("mask_chp");'."\n" ;
				$js_cloture.= '    $(".for_cloture").parent().addClass("mask_chp");'."\n" ;
				$js_cloture.= '    if (stat_sel==2) {'."\n" ;
				$js_cloture.= '      $(".more_info_signe").parent().removeClass("mask_chp");'."\n" ;
				$js_cloture.= '      $(".for_cloture").parent().removeClass("mask_chp");'."\n" ;
				//$js_cloture.= '    alert( "statut change" );'."\n" ;
				$js_cloture.= '    }'."\n" ;
				$js_cloture.= '    else if (stat_sel==3) {'."\n" ;
				$js_cloture.= '      $(".more_info_nosigne").parent().removeClass("mask_chp");'."\n" ;
				$js_cloture.= '    }'."\n" ;
				$js_cloture.= '    else {'."\n" ;
				$js_cloture.= '    }'."\n" ;
				$js_cloture.= '  }'."\n" ;
				$js_cloture.= '  function select_nosigne() {'."\n" ;
				$js_cloture.= '    var stat_sel = $("#statut option:selected").val() ;'."\n" ;
				$js_cloture.= '    var refus_sel = $("#close_project_refus").prop( "checked" ) ;'."\n" ;
				$js_cloture.= '    if (stat_sel==3 && refus_sel==true) {'."\n" ;
				//$js_cloture.= '      alert( "checked: "+refus_sel );'."\n" ;
				$js_cloture.= '      $(".more_info_nosigne_perdu").parent().removeClass("mask_chp");'."\n" ;
				$js_cloture.= '      $(".for_cloture").parent().removeClass("mask_chp");'."\n" ;
				$js_cloture.= '    }'."\n" ;
				/*$js_cloture.= '    else {'."\n" ;
				//$js_cloture.= '      alert( "checked else: "+refus_sel );'."\n" ;
				$js_cloture.= '      $(".more_info_nosigne_perdu").parent().addClass("mask_chp");'."\n" ;
				$js_cloture.= '    }'."\n" ;*/
				if ($myproject->array_options['options_date_fin_contrat']=='')
				{
					$js_cloture.= '    $("#date_fin_contrat").val("") ;'."\n" ;
				}
				$js_cloture.= '  }'."\n" ;
				$js_cloture.= "</script>\n";
				echo $js_cloture;

				/*if (!empty($conf->notification->enabled))
				{
					require_once DOL_DOCUMENT_ROOT.'/core/class/notify.class.php';
					$notify = new Notify($this->db);
					$formquestion = array_merge($formquestion, array(
						array('type' => 'onecolumn', 'value' => $notify->confirmMessage('PROPAL_CLOSE_SIGNED', $object->socid, $object)),
					));
				}*/
				//echo "<pre>".print_r($formquestion, TRUE)."</pre>" ;

				$formconfirm = $form->formconfirm($_SERVER["PHP_SELF"].'?id='.$object->id, $langs->trans('SetAcceptedRefused'), $text, 'confirm_closeas', $formquestion, '', 1, $height_pop);

				$this->resprints =  $formconfirm ;
				return 1;
			}
		}
	}

	public function addSQLWhereFilterOnSelectSalesRep($parameters, &$formother, &$action, $hookmanager) {
		global $langs, $user;
		if ($user->admin==1) { }
		else {
			// gestion du filtre 'Tiers ayant pour commercial' sur la liste des affaires
			$hookmanager->resArray[0] = " OR u.fk_user = ".$user->id;
		}
		//echo "HOOK addSQLWhereFilterOnSelectSalesRep - ".$hookmanager->resArray[0]."<br>" ;
		return 1 ;
	}

	public function addSQLWhereFilterOnSelectUsers($parameters, &$formother, &$action, $hookmanager) {
		global $langs, $user;
		if ($user->admin==1) { }
		else {
			// gestion du filtre 'Tiers ayant pour commercial' sur la liste des affaires
			$hookmanager->resPrint = " OR u.fk_user = ".$user->id;
		}
		//echo "HOOK addSQLWhereFilterOnSelectUsers - select_dolusers - ".$hookmanager->resPrint."<br>" ;
		return 1 ;
	}

	public function printCommonFooter($parameters, &$action, $hookmanager) {
		global $langs, $conf, $user ;
		global $menumanager;
		global $object;
		$trans_apo = array("'"=>"\\'") ;
		$in_context = FALSE ;
		$context_current = '' ;
		$massaction = GETPOST('massaction', 'alpha');
		$TContext = (isset($parameters['context']) AND $parameters['context']!='') ? explode(':', $parameters['context']) : array() ;
		include_once DOL_DOCUMENT_ROOT.'/comm/propal/class/propal.class.php';

		$TList_page = array(
			'index',
			'accountancycustomerlist',
			'comptafileslist',
			'categoryindex',
			'thirdpartiesindex',
			'thirdpartylist',
			'thirdpartycard',
			'thirdpartycontact',
			'consumptionthirdparty',
			'thirdpartyticket',
			'thirdpartycomm',
			'projectthirdparty',
			'thirdpartynotification',
			'thirdpartynote',
			'thirdpartydocument',
			'agendathirdparty',
			'commercialindex',
			'propallist',
			'propalcard',
			'projectsindex',
			'productindex',
			'projectlist',
			'projectcard',
			'projectcontactcard',
			'projectticket',
			'projectOverview',
			'projecttaskscard',
			'projectnote',
			'projectdocument',
			'projectinfo',
			'contactlist',
			'contactcard',
			'consumptioncontact',
			'projectcontact',
			'sendinbluecontactcard',
			'contactnote',
			'contactdocument',
			'contactagenda',
			'usercard',
			'productcard',
			'productstatsinvoice',
			'productpricecard',
			'productstatscard',
			'agenda',
			'agendalist',
			'mailingcard',
			'ticketsindex',
			'ticketlist',
			'ticketcard',
			'mail',
			'sendMail',
			'invoicelist',
			'invoicecard',
			'paymentlist',
			'actioncard',
			'thirdpartydao',
			//'globallist',
			//'globalcard',
		) ;

		foreach ($TContext as $context)
		{
			if (in_array($context, $TList_page))	    // do something only for the context 'somecontext1' or 'somecontext2'
			{
				$in_context = TRUE ;
				if ($context!=$parameters['currentcontext']) $context_current = $context ;
			}
		}

		if ($user->admin==1)
		{
			echo "************ printCommonFooter parameters[currentcontext]: ".$parameters['currentcontext']
			.($in_context ? ' | Context_current: '.$context_current : '')
			." | action: ".$action
			." | GETPOST action: ".GETPOST('action')."".(isset($massaction) ? ' - massaction: '.$massaction : '')
			." | user id: ".$user->id." (son responsable: ".$user->fk_user.")"
			." | parameters[context]: ".$parameters['context']
			." ********<br>\r\n" ;
			//echo "<pre>".print_r($parameters, TRUE)."</pre><br />\r\n" ;
			//echo "<pre>".print_r($TContext, TRUE)."</pre><br />\r\n" ;
			//echo "<pre>".print_r($_REQUEST, TRUE)."</pre><br />\r\n" ;
		}
		//echo (!empty($conf->global->COMPANY_USE_SEARCH_TO_SELECT) ? 1 : 0)." |<br><pre>".print_r($conf->global, TRUE)."</pre><br>" ;
		/*for ($ind=0 ; $ind<4100 ; $ind++) {
			echo "<style>.fa-test".$ind."::before {content: \"\\f".dechex ($ind)."\";font-size: 1.5em;font-weight: 900;}</style>\n";
			echo "&nbsp;&nbsp;| <span class=\"fas fa-test".$ind." infobox-project\"></span>\n";
		}
		echo "<br><br>\n";*/
		//exit() ;

		if ($in_context || in_array($parameters['currentcontext'], $TList_page))	    // do something only for the context 'somecontext1' or 'somecontext2'
		{
			$js_pdp = '' ;
			$js_pdp.= "<!-- add js perso -->\n";
			$js_pdp.= '<script type="text/javascript">'."\n" ;
			if ($parameters['currentcontext']=='thirdpartylist')
			{
				$js_pdp.= '$(".maxwidth75").addClass("maxwidth300");'."\n" ;
				$js_pdp.= '$(".maxwidth75").removeClass("maxwidth75");'."\n" ;
			}
			$js_pdp.= '$(document).ready(function () {'."\n" ;

			$TChp_require = array() ; // en gras
			$TMask_chp = array() ;

			if ( ($context_current=='agendalist' OR $parameters['currentcontext']=='agendalist') )
			{
				$js_pdp.= 'agenda_add_other_user() ;'."\n" ;

			}

			if ( ($context_current=='accountancycustomerlist' OR $parameters['currentcontext']=='accountancycustomerlist') )
			{
				//if ($user->admin==1) echo "!!! ".$context_current." !!<br>\r\n" ;

				if ($user->admin==1) { // admin
					//$js_pdp.= '$(document).ready(function () {'."\n" ;
					$js_pdp.= 'auto_compta_produit() ;'."\n" ;
					//$js_pdp.= '});'."\n" ;
				}
			}

			if ( ( ($context_current=='actioncard' OR $context_current=='thirdpartydao' OR $parameters['currentcontext']=='thirdpartydao') &&
					(GETPOST('action')=='create'
					OR GETPOST('action')=='add'
					)
				)
				OR
				( ($context_current=='actioncard' OR $parameters['currentcontext']=='actioncard')
					&& (GETPOST('action')=='edit'
						OR GETPOST('action')=='update'
					)
					//&& $object->type_code=='FAC'
				)
			)
			{ // cf batibarrmod.js.php
				$js_pdp.= '  $( "#actioncode" ).change(function() { select_actioncode() ; });' ."\n" ;

				$js_pdp.= '$(document).ready(function () {'."\n" ;
				//$js_pdp.= 'alert("test") ;'."\n" ;
				$js_pdp.= 'select_actioncode() ;'."\n" ;
				$js_pdp.= '});'."\n" ;
			}

			if ( ($context_current=='invoicecard' OR $parameters['currentcontext']=='invoicecard') /*&&
				(empty(GETPOST('action'))
				OR GETPOST('action')=='view'
				)*/
			)
			{
				// add bouton copier fichiers joints
				$js_pdp.= '  $("input[name=\'downloadexternallink\']").each(function() {;'."\n" ;
				$js_pdp.= '    var key = $(this).attr("id");'."\n" ;
				//$js_pdp.= '  key = key.replace("downloadlink","");'."\n" ;
				//$js_pdp.= '  alert(key);'."\n" ;
				$js_pdp.= '    $(this).after(\'<span class="fas fa-copy js-copy" data-target="#tocopy" onclick="docopy(\'+key+\');" title="Copier ce lien dans le presse papier"></span>\')'."\n" ;
				$js_pdp.= '  });'."\n" ;
				
				$js_pdp.= '  $("td.fieldname_type:contains(\''.html_entity_decode($langs->trans('Type'), ENT_COMPAT, 'UTF-8').'\')").html("Type de facture");'."\n" ;
				if (GETPOSTISSET('facid'))
				{
					$obj_fact = new Facture($this->db);
					$ret = $obj_fact->fetch(GETPOST('facid'));
					if ($ret > 0) {
						if ($user->admin==1) echo "******** invoicecard id: ".GETPOST('facid')." | fk_user_author: ".$obj_fact->fk_user_author." ********<br>\r\n" ;

						require_once DOL_DOCUMENT_ROOT.'/projet/class/project.class.php';
						$myproject = new Project($this->db);
						$result = $myproject->fetch($obj_fact->fk_project);
						if ($result < 0) {
						}
						else {
							$js_pdp.= '  $("div.fichehalfleft").children("table").first().after(\'<table class="border tableforfield" width="100%"><tr>'
								.'<td class="titlefield fieldname_type">Particularité de Facturation</td>'
								.'<td class="valuefield fieldname_type">'
								//.'<a href="../projet/card.php?id='.$myproject->array_options["options_particularite_facture"].'">'
								.$myproject->array_options["options_particularite_facture"]
								//.'</a>'
								.'</td>'
								.'<td class="right">'
								.'<a href="/projet/card.php?id='.$myproject->id.'">'
								.'<span class="fas fa-pencil-alt" style=" color: #444; float: right" title="Modifier dans l\\\'affaire"></span></a></td>'
								.'</tr></table>\');'."\n" ;
						}
						//echo "<pre>".print_r($myproject, TRUE) ;

						if ($obj_fact->type==2) { // avoir
						}
						else {
							//$js_pdp.= '  $("td:contains(\''.html_entity_decode($langs->trans("PaymentMode"), ENT_COMPAT, 'UTF-8').'\')").addClass("fieldrequired");'."\n" ; // mode_reglement_id
							$js_pdp.= '  $("td:contains(\''.html_entity_decode($langs->trans("PaymentConditions"), ENT_COMPAT, 'UTF-8').'\')").addClass("fieldrequired");'."\n" ; // cond_reglement_id
						}
					} else {
						setEventMessages("MOD ERROR LOAD: ".$obj_fact->error, $obj_fact->errors, 'errors');
					}

				}
				else { // nouvelle facture
					//$js_pdp.= '  $("td:contains(\''.html_entity_decode($langs->trans("PaymentMode"), ENT_COMPAT, 'UTF-8').'\')").addClass("fieldrequired");'."\n" ; // mode_reglement_id
					$js_pdp.= '  $("td:contains(\''.html_entity_decode($langs->trans("PaymentConditions"), ENT_COMPAT, 'UTF-8').'\')").addClass("fieldrequired ");'."\n" ; // cond_reglement_id
				}

				$f_del_bt_creer_avoir = TRUE ;
				if ($user->admin==1) $f_del_bt_creer_avoir = FALSE ;
				else if ($user->id > 0)
				{
					$usergroup = new UserGroup($this->db);
					$groupslist = $usergroup->listGroupsForUser($user->id);
					foreach ($groupslist as $id_gr=>$TData)
					{
						if ($id_gr==8) $f_del_bt_creer_avoir = FALSE ; // Paiement et comptabilité
					}
					//echo "<hr>* user id: ".$user->id." (res: ".($f_del_bt_creer_avoir ? 'del' : 'no del').") => ".print_r($groupslist, TRUE)."<hr />" ;
				}

				//exit('user !') ;
				if ($f_del_bt_creer_avoir)
				{
					$js_pdp.= '  $("a:contains(\''.html_entity_decode($langs->trans('CreateCreditNote'), ENT_COMPAT, 'UTF-8').'\')").remove();'."\n" ; //remove() addClass("mask_chp") // Créer facture avoir
				}

				if ($user->admin!=1) $js_pdp.= '  $("a:contains(\''.html_entity_decode($langs->trans('ChangeIntoRepeatableInvoice'), ENT_COMPAT, 'UTF-8').'\')").remove();'."\n" ;
				if ($user->admin==0) {
					$TMask_chp['prod_entry_mode_free'] = array('type'=>'input', 'nb_parent'=>2, 'red'=>1) ; // Select ligne libre

					$js_pdp.= '  $("#prod_entry_mode_free").parent().parent().parent().children("br").first().addClass("tabs_mask_chp_admin");'."\n" ; // br entre les 2 selecteurs
					$js_pdp.= '  $("#prod_entry_mode_free").parent().parent().addClass("tabs_mask_chp_admin");'."\n" ; // Select ligne libre
				}
				else {
					$TMask_chp['prod_entry_mode_free'] = array('type'=>'input', 'nb_parent'=>2, 'mask'=>1, 'remove'=>0) ; // Select ligne libre

					$js_pdp.= '  $("#prod_entry_mode_free").parent().parent().parent().children("br").first().remove();'."\n" ; // br entre les 2 selecteurs
					$js_pdp.= '  $("#prod_entry_mode_free").parent().parent().addClass("mask_chp");'."\n" ; // Select ligne libre
				}
			}

			if ( ($context_current=='invoicecard' OR $parameters['currentcontext']=='invoicecard') && (
				GETPOST('action')=='create'
				|| GETPOST('action')=='add'
				)
			)
			{
				$js_pdp.= '  $("td.tdtop:contains(\''.html_entity_decode($langs->trans('Type'), ENT_COMPAT, 'UTF-8').'\')").html("Type de facture");'."\n" ;
				$js_pdp.= '  $("td:contains(\''.html_entity_decode($langs->trans('NotePrivate'), ENT_COMPAT, 'UTF-8').'\')").html("Note");'."\n" ;

				if ($user->admin==1) { // admin
					$js_pdp.= '  $("td:contains(\''.html_entity_decode($langs->trans('Discounts'), ENT_COMPAT, 'UTF-8').'\')").parent().addClass("tabs_mask_chp_admin");'."\n" ; // v13
					$js_pdp.= '  $("td:contains(\''.html_entity_decode($langs->trans('DiscountStillRemaining'), ENT_COMPAT, 'UTF-8').'\')").parent().addClass("tabs_mask_chp_admin");'."\n" ; // v17
					$js_pdp.= '  $("td:contains(\''.html_entity_decode($langs->trans('AmountHT'), ENT_COMPAT, 'UTF-8').'\')").parent().addClass("tabs_mask_chp_admin");'."\n" ;
					$js_pdp.= '  $("td:contains(\''.html_entity_decode($langs->trans('AmountVAT'), ENT_COMPAT, 'UTF-8').'\')").parent().addClass("tabs_mask_chp_admin");'."\n" ;
					$js_pdp.= '  $("td:contains(\''.html_entity_decode($langs->trans('AmountLT1'), ENT_COMPAT, 'UTF-8').'\')").parent().addClass("tabs_mask_chp_admin");'."\n" ;
					$js_pdp.= '  $("td:contains(\''.html_entity_decode($langs->trans('AmountLT2'), ENT_COMPAT, 'UTF-8').'\')").parent().addClass("tabs_mask_chp_admin");'."\n" ;
					$js_pdp.= '  $("td:contains(\''.html_entity_decode($langs->trans('AmountTTC'), ENT_COMPAT, 'UTF-8').'\')").parent().addClass("tabs_mask_chp_admin");'."\n" ;

					$js_pdp.= '  $("#options_ba_support").parent().parent().addClass("tabs_mask_chp_admin");'."\n" ;
					$js_pdp.= '  $("#options_titre_pdf").parent().parent().addClass("tabs_mask_chp_admin");'."\n" ;
					$js_pdp.= '  $("#note_public").parent().parent().addClass("tabs_mask_chp_admin");'."\n" ;
					$js_pdp.= '  $("div.listofinvoicetype").parent().parent().parent(\'tr\').addClass("tabs_mask_chp_admin");'."\n" ;
				}
				else {
					$js_pdp.= '  $("td:contains(\''.html_entity_decode($langs->trans('Discounts'), ENT_COMPAT, 'UTF-8').'\')").parent().remove();'."\n" ; // v13
					$js_pdp.= '  $("td:contains(\''.html_entity_decode($langs->trans('DiscountStillRemaining'), ENT_COMPAT, 'UTF-8').'\')").parent().remove();'."\n" ; // v17
					$js_pdp.= '  $("td:contains(\''.html_entity_decode($langs->trans('AmountHT'), ENT_COMPAT, 'UTF-8').'\')").parent().remove();'."\n" ;
					$js_pdp.= '  $("td:contains(\''.html_entity_decode($langs->trans('AmountVAT'), ENT_COMPAT, 'UTF-8').'\')").parent().remove();'."\n" ;
					$js_pdp.= '  $("td:contains(\''.html_entity_decode($langs->trans('AmountLT1'), ENT_COMPAT, 'UTF-8').'\')").parent().remove();'."\n" ;
					$js_pdp.= '  $("td:contains(\''.html_entity_decode($langs->trans('AmountLT2'), ENT_COMPAT, 'UTF-8').'\')").parent().remove();'."\n" ;
					$js_pdp.= '  $("td:contains(\''.html_entity_decode($langs->trans('AmountTTC'), ENT_COMPAT, 'UTF-8').'\')").parent().remove();'."\n" ;

					$js_pdp.= '  $("#options_ba_support").parent().parent().addClass("mask_chp");'."\n" ;
					$js_pdp.= '  $("#options_titre_pdf").parent().parent().addClass("mask_chp");'."\n" ;
					$js_pdp.= '  $("#note_public").parent().parent().addClass("mask_chp");'."\n" ;
					$js_pdp.= '  $("div.listofinvoicetype").parent().parent().parent(\'tr\').addClass("mask_chp");'."\n" ;
				}
			}

			if ( ($context_current=='ticketcard' OR $parameters['currentcontext']=='ticketcard') && GETPOST('action')=='create')
			{
				if ($user->admin==1) { // admin
					$js_pdp.= '  $("#notify_tiers_at_create").parent().parent().addClass("tabs_mask_chp_admin");'."\n" ;
					$js_pdp.= '  $("#fk_user_assign").parent().parent().addClass("tabs_mask_chp_admin");'."\n" ;
				}
				else {
					$js_pdp.= '  $("#notify_tiers_at_create").parent().parent().addClass("mask_chp");'."\n" ;
					$js_pdp.= '  $("#fk_user_assign").parent().parent().addClass("mask_chp");'."\n" ;
				}
				$js_pdp.= '  $("td:contains(\''.html_entity_decode($langs->trans("ThirdParty"), ENT_COMPAT, 'UTF-8').'\')").addClass("fieldrequired");'."\n" ;
				$js_pdp.= '  $("#projectid").parent().parent().children("td:contains(\''.html_entity_decode($langs->trans('Project'), ENT_COMPAT, 'UTF-8').'\')").addClass("fieldrequired");'."\n" ;
			}

			if ( ($context_current=='ticketcard' OR $parameters['currentcontext']=='ticketcard') && (
					GETPOST('action')=='presend_addmessage'
					OR GETPOST('action')=='add_message'
				)
			) {
				$js_pdp.= '  $("#send_msg_email").parent().parent().addClass("mask_chp");'."\n" ;
				$js_pdp.= '  $("#private_message").parent().parent().addClass("mask_chp");'."\n" ;
			}

			if ( ($context_current=='ticketcard' OR $parameters['currentcontext']=='ticketcard') &&
				(empty(GETPOST('action'))
				OR GETPOST('action')=='view'
				OR GETPOST('action')=='presend_addmessage'
				OR GETPOST('action')=='add_message'
				OR GETPOST('action')=='presend'
				OR GETPOST('action')=='addlink'
				OR GETPOST('action')=='dellink'
				OR GETPOST('action')=='close'
				OR GETPOST('action')=='delete'
				OR GETPOST('action')=='editcustomer'
				OR GETPOST('action')=='progression'
				OR GETPOST('action')=='reopen'
				OR GETPOST('action')=='set_status'
				)
			)
			{
				$TList_id_propal_ticket = array() ;
				$TList_propal_ticket = array() ;
				$id_prj_ticket = 0 ;
				$statut_obj = 8 ;
				if (GETPOST('id') > 0)
				{
					$obj_Ticket = new Ticket($this->db);
					$res = $obj_Ticket->fetch( GETPOST('id') );
					if ($res >= 0)
					{
						$id_prj_ticket = $obj_Ticket->fk_project ;
						$statut_obj = $obj_Ticket->fk_statut ;
					}
				}
				else if (GETPOST('track_id', 'alpha') !='')
				{
					$obj_Ticket = new Ticket($this->db);
					$ret = $obj_Ticket->fetch('', '', GETPOST('track_id', 'alpha'));
					if ($res >= 0)
					{
						$id_prj_ticket = $obj_Ticket->fk_project ;
						$statut_obj = $obj_Ticket->fk_statut ;
					}
				}

				if ($id_prj_ticket > 0)
				{
					//$propal_static = new Propal($this->db);

					$sql = "SELECT rowid FROM ".MAIN_DB_PREFIX."propal WHERE fk_projet=".$id_prj_ticket ;
					$result = $this->db->query($sql);
					if ($result)
					{
						while ($obj_prod = $this->db->fetch_object($result)) {
							$TList_id_propal_ticket[] = $obj_prod->rowid ;
							//fk_statut 0: Brouillon | 1: Validé (ouvert) | 2: Signée | 3: Non Signée | 4: Facturée (fermé)

						}
					}
				}

				if (count($TList_id_propal_ticket) > 0) {
					foreach($TList_id_propal_ticket as $id_prop) {
						$propal_ticket = new Propal($this->db);
						$result = $propal_ticket->fetch( $id_prop );
						if ($result < 0) {
							$this->errors[] = $propal_ticket->error;
							$error++;
						} else {
							$TList_propal_ticket[] = $propal_ticket ;
						}
					}
				}

				if (isset($_REQUEST['DEBUG'])) echo "<hr />\r\n* nb propal: ".count($TList_propal_ticket)."<br /><br />\r\n" ;
				if (count($TList_propal_ticket) > 0) {
					// affichage des propositions lies au projet du ticket
					$js_pdp.= '  var add_list_prop = "";'."\n" ;

					$js_pdp.= '  $(document).on(\'click\',\'.toselect_line\',function (e) {'."\n" ;
					$js_pdp.= '    if(this.checked) var check_stat = true ;'."\n" ;
					$js_pdp.= '    else var check_stat = false ;'."\n" ;
					//$js_pdp.= '    alert($(this).attr("id_propal")) ;'."\n" ;

					$js_pdp.= "    $.ajax({\n" ;
					$js_pdp.= "    url: '".DIR_HTTP."custom/batibarrmod/scripts/interface.php'\n" ;
					$js_pdp.= "      ,data: ({mode:'SET_STATUT_LINE_TICKET',id_line:$(this).val(),id_propal:$(this).attr(\"id_propal\"),statut:check_stat})\n" ;
					$js_pdp.= "      ,contentType: 'application/json; charset=ISO-8859-1'\n" ;
					$js_pdp.= "      ,cache: true\n" ;
					$js_pdp.= "      ,async: false\n" ;
					$js_pdp.= "      ,dataType:'json'\n" ;

					$js_pdp.= "      , success: function(TRes){\n" ;
					//$js_pdp.= '        alert( "ok: "+TRes["RES"] ) ;'."\n" ;
					$js_pdp.= "      }\n" ;
					$js_pdp.= "      ,error: function(){\n" ;
					$js_pdp.= '        alert( "error" ) ;'."\n" ;
					$js_pdp.= "      }\n" ;
					$js_pdp.= "    });\n" ; // ajax
					//$js_pdp.= '        alert( "err: "+$(this).val()+" | "+check_stat ) ;'."\n" ;

					$js_pdp.= '  });'."\n" ; // click

					//$js_pdp.= '  add_list_prop+= "<div style=\"clear:both;\"></div>";'."\n" ;
					$js_pdp.= '  add_list_prop+= "<div class=\"fichehalfleft\">";'."\n" ;
					$js_pdp.= '  add_list_prop+= "<table class=\"centpercent notopnoleftnoright table-fiche-title showlinkedobjectblock\"><tr class=\"titre\"><td class=\"nobordernopadding valignmiddle col-title\"><div class=\"titre inline-block\">'.$langs->trans("Commercial").'</div></td></tr></table>";'."\n" ;
					$js_pdp.= '  add_list_prop+= "<div class=\"div-table-responsive-no-min\">";'."\n" ;

					$nb_p = 0 ;
					foreach($TList_propal_ticket as $propal_ticket)
					{
						//0: Brouillon | 1: Validé (ouvert) | 2: Signée | 3: Non Signée | 4: Facturée (fermé)
						if ($propal_ticket->statut==2 || $propal_ticket->statut==4) {
							//if ($nb_p > 0) $js_pdp.= '  add_list_prop+= " | "'."\n" ;
							$url_statut_line_ticket = DIR_HTTP_BO.'/scripts/interface.php?mode=GET_STATUT_LINE_TICKET&id_propal='.$propal_ticket->id ;
							$res = file_get_contents($url_statut_line_ticket) ;
							$TRes = json_decode($res) ;
							$TStatut = $TRes->TStatut ;

							$title = '' ;
							$title.= '<span class=\'fas fa-file-signature infobox-propal\'></span>' ;
							$title.= ' <u class=\'paddingrightonly\'>'.$langs->trans('ProjectOverview').'</u>' ;
							$title.= ' <span class=\'badge  badge-status'.$propal_ticket->fk_statut.' badge-status\' title=\'Ouvert\'>'.$propal_ticket->LibStatut($propal_ticket->fk_statut).'</span><br>' ;
							$title.= '<b>Réf.: </b>'.$propal_ticket->ref.'<br>' ;
							$title.= '<b>Statut: </b>'. $propal_ticket->LibStatut($propal_ticket->statut).'<br>' ;
							$title.= '<b>Montant HT: </b>'.price($propal_ticket->total_ht, 0, $langs, 1, -1, -1, $conf->currency).'<br>' ;
							$title.= '<b>TVA: </b>'.price($propal_ticket->total_tva, 0, $langs, 1, -1, -1, $conf->currency).'<br>' ;
							$title.= '<b>Montant TTC: </b>'.price($propal_ticket->total_ttc, 0, $langs, 1, -1, -1, $conf->currency) ;

							$link_pdf = $this->get_list_pdf_propal($propal_ticket) ;

							$js_pdp.= '  add_list_prop+= "<table class=\"noborder noshadow\">";'."\n" ;
							$js_pdp.= '  add_list_prop+= "<thead><tr class=\"liste_titre nodrag nodrop\">";'."\n" ;
							$js_pdp.= '  add_list_prop+= "<td>'.$langs->trans("Commercial").'</td>";'."\n" ;
							$js_pdp.= '  add_list_prop+= "<td>PDF</td>";'."\n" ;
							$js_pdp.= '  add_list_prop+= "</tr></thead>"'."\n" ;

							$js_pdp.= '  add_list_prop+= "<tr class=\"oddeven\">";'."\n" ;
							$js_pdp.= '  add_list_prop+= "<td class=\"titlefield nowrap\">";'."\n" ;
							$js_pdp.= '  add_list_prop+= "<a href=\"/comm/propal/card.php?id='.$propal_ticket->id.'\" title=\"'.$title.'\" class=\"classfortooltip\">'.$propal_ticket->ref.'</a>"'."\n" ;
							$js_pdp.= '  add_list_prop+= "</td>";'."\n" ;

							$js_pdp.= '  add_list_prop+= "<td class=\"titlefield\">'.($link_pdf=='' ? '&nbsp;' : $link_pdf).'</td>";'."\n" ;
							$js_pdp.= '  add_list_prop+= "</tr>";'."\n" ;
							$js_pdp.= '  add_list_prop+= "</table>"'."\n" ;

							$js_pdp.= '  add_list_prop+= "<table class=\"border tableforfield centpercent\"><tr><td class=\"titlefield\">Article(s):</td></table>";'."\n" ;

							$js_pdp.= '  add_list_prop+= "<table class=\"noborder noshadow\">";'."\n" ;
							$nb_line = 0 ;
							foreach ($propal_ticket->lines as $line) {
								if ($line->product_type==8) {
									// ignore les reductions
								}
								else {
									$id_line = $line->rowid ;
									$product_label = $line->product_label ;
									$product_label = strtr($product_label, array('"'=>'\"', "'"=>'&apos;')) ;

									$product_description = ($line->description ? $line->description : $line->desc) ;
									$product_description = strtr($product_description, array('"'=>'\"', "'"=>'&apos;')) ;
									$checked_line = (isset($TStatut->{$id_line}) AND $TStatut->{$id_line}==1) ? ' checked' : '' ;

									$line_lib = '' ;
									$line_lib.= '<a href=\"/product/card.php?id='.$line->fk_product.'\" class=\"nowraponall classfortooltip\" title=\"<span class=&quot;fas fa-concierge-bell&quot; style=&quot; color: #a69944;&quot;></span> <u class=&quot;paddingrightonly&quot;>Article</u><br><b>Réf. produit:</b> '.strip_tags($line->ref).'\">' ;
									$line_lib.= '<span class=\"fas fa-concierge-bell paddingright classfortooltip\" style=\"color: #a69944;\"></span><strong>'.strip_tags($line->ref).'</strong>' ;
									$line_lib.= '</a>' ;
									$line_lib.= ' - <strong>'.$product_label.'</strong>' ;
									$line_lib.= '<br>' ;
									$line_lib.= $product_description ;
									$line_lib = strtr($line_lib, array("\r"=>'', "\n"=>'')) ;

									if (isset($_REQUEST['DEBUG'])) echo "<hr />\r\n* propal: ".$propal_ticket->id." | pos: ".$nb_line." | ".$propal_ticket->ref." | statut: ".$propal_ticket->statut." | ".$line_lib."<br />\r\n" ;
									if (isset($_REQUEST['DEBUG'])) echo print_r($line, TRUE)."<br />\r\n" ;

									$js_pdp.= '  add_list_prop+= "<tr>";'."\n" ;
									$js_pdp.= '  add_list_prop+= "<td class=\"\">";'."\n" ; //nowrap
									$js_pdp.= '  add_list_prop+= "'. $line_lib .'"'."\n" ; //'.$langs->trans('Description').':
									$js_pdp.= '  add_list_prop+= "</td>";'."\n" ;
									$js_pdp.= '  add_list_prop+= "<td class=\"nowrap\">'.$langs->trans('Qty').': '.price($line->qty, 0, '', 0, 0).'</td>";'."\n" ;
									$js_pdp.= '  add_list_prop+= "<td class=\"nowrap\"><input id=\"toselect_line_'.$id_line.'\" class=\"toselect_line flat checkforselect\" type=\"checkbox\" name=\"toselect_line_'.$id_line.'\" value=\"'.$id_line.'\" id_propal=\"'.$propal_ticket->id.'\"'.$checked_line.'></td>";'."\n" ;
									//$js_pdp.= '  add_list_prop+= "<td class=\"titlefield\">'.$langs->trans('TotalHTShort').': '.price($line->total_ht).'</td>";'."\n" ;
									$js_pdp.= '  add_list_prop+= "</tr>";'."\n" ;
								}

								$nb_line++ ;
							}
							$js_pdp.= '  add_list_prop+= "</table>"'."\n" ;

							$nb_p++ ;
						}
					}

					if ($nb_p==0) $js_pdp.= '  add_list_prop+= "<tr><td colspan=2>Aucune proposition signée</td></tr>"'."\n" ;

					/*if (isset($_REQUEST['test_pos0'])) {
					}
					else if (!isset($_REQUEST['test_pos1']) && !isset($_REQUEST['test_pos2'])) {
						$js_pdp.= '  add_list_prop+= "</div>"'."\n" ;
					}
					else {
					}*/
					$js_pdp.= '  add_list_prop+= "</div>"'."\n" ;
					$js_pdp.= '  add_list_prop+= "</div>"'."\n" ;
					$js_pdp.= '  var data_case = $("div.fichecenter").last().children("div.fichehalfleft").first().html()'.";\n" ;
					$js_pdp.= '  data_case = "<div class=\"fichehalfright\">"+data_case+"</div>"'.";\n" ;
					$js_pdp.= '  $("div.fichecenter").last().append(data_case);'."\n" ;
					$js_pdp.= '  var data_case = $("div.fichecenter").last().children("div.fichehalfleft").first().html("")'.";\n" ;

					//$js_pdp.= '  $("div.fichecenter").last().children("div.fichehalfleft").first().addClass("fichehalfright")'.";\n" ;
					//$js_pdp.= '  $("div.fichecenter").last().children("div.fichehalfright").last().addClass("fichehalfleft")'.";\n" ;

					//$js_pdp.= '  $("div.fichecenter").last().children("div.fichehalfleft").first().removeClass("fichehalfleft")'.";\n" ;
					//$js_pdp.= '  $("div.fichecenter").last().children("div.fichehalfright").last().removeClass("fichehalfright")'.";\n" ;

					/*if (isset($_REQUEST['test_pos1'])) $js_pdp.= '  $("div.tabsAction").first().after(add_list_prop);'."\n" ;
					else if (isset($_REQUEST['test_pos2'])) $js_pdp.= '  $("div.fiche").first().append(add_list_prop);'."\n" ;
					else if (isset($_REQUEST['test_pos0'])) $js_pdp.= '  $("div.fichehalfleft").first().append(add_list_prop);'."\n" ;
					else */
					$js_pdp.= '  $("div.fichecenter").last().prepend(add_list_prop);'."\n" ;

					//$js_pdp.= '  $("td:contains(\''.html_entity_decode($langs->trans("Progression"), ENT_COMPAT, 'UTF-8').'\')").parent().parent().children("tr").append(add_list_prop);'."\n" ;
					//$js_pdp.= '  alert(add_list_prop);'."\n" ;
				}
				/**/

				if ($user->admin==1) {
					if ($statut_obj==8) $TMask_chp['ReOpen'] = array('type'=>'a', 'nb_parent'=>0, 'mask'=>0, 'red'=>1) ;
					$TMask_chp['Categories'] = array('type'=>'td', 'nb_parent'=>1, 'red'=>1) ;
					//$TMask_chp['AssignedTo'] = array('type'=>'td', 'nb_parent'=>1, 'red'=>1) ;
				}
				else {
					if ($statut_obj==8) {
						$TMask_chp['ReOpen'] = array('type'=>'a', 'nb_parent'=>0, 'mask'=>0, 'remove'=>1) ;

						$js_pdp.= '  $(".editfielda").each(function() {'."\n" ; // supprime le stylet de partout
						$js_pdp.= '    $(this).remove();'."\n" ;
						$js_pdp.= "  });\n" ; // each
					}
					$TMask_chp['Categories'] = array('type'=>'td', 'nb_parent'=>1, 'mask'=>1, 'remove'=>0) ;
					//$TMask_chp['AssignedTo'] = array('type'=>'td', 'nb_parent'=>1, 'mask'=>1, 'remove'=>0) ;
				}

				//$js_pdp.= '$(".butActionDelete").parent().remove();'."\n" ;
				$js_pdp.= '$(".badge-status0").parent().remove();'."\n" ;
				$js_pdp.= '$(".badge-status7").parent().remove();'."\n" ;
				$TMask_chp['TicketTrackId'] = array('type'=>'td', 'nb_parent'=>1, 'mask'=>1, 'remove'=>0) ;
				$TMask_chp['AbandonTicket'] = array('type'=>'a', 'nb_parent'=>0, 'mask'=>1, 'remove'=>0) ;
				$js_pdp.= "  $('td:contains(\"".strtr (html_entity_decode($langs->trans("TicketCloseOn"), ENT_COMPAT, 'UTF-8'), array("'"=>"\'") )."\")').addClass(\"titlefield\");\n" ;
			}

			if ($parameters['currentcontext']=='thirdpartynote' || $parameters['currentcontext']=='contactnote')
			{
				$js_pdp.= '  $(".editfielda").parent().parent().children("td:contains(\''.html_entity_decode($langs->trans('NotePrivate'), ENT_COMPAT, 'UTF-8').'\')").html("Archives");'."\n" ;
				$TMask_chp['NotePublic'] = array('type'=>'td', 'nb_parent'=>5, 'mask'=>0, 'remove'=>1) ;
				$js_pdp.= '  $(".editfielda").parent().remove();'."\n" ; // supprime le stylet de modification pour le note
			}

			if ($parameters['currentcontext']=='thirdpartycard'
				|| $parameters['currentcontext']=='thirdpartycontact'
				|| $parameters['currentcontext']=='thirdpartyticket'
				|| $parameters['currentcontext']=='thirdpartycomm'
				|| $parameters['currentcontext']=='projectthirdparty'
				|| $parameters['currentcontext']=='consumptionthirdparty'
				|| $parameters['currentcontext']=='thirdpartynotification'
				|| $parameters['currentcontext']=='thirdpartynote'
				|| $parameters['currentcontext']=='thirdpartydocument'
				|| $parameters['currentcontext']=='agendathirdparty'
			)
			{
				// masque les onglets de la societe
				if ($user->admin==1) {
					$TMask_chp['customer'] = array('type'=>'a', 'nb_parent'=>0, 'red'=>1) ;
					$TMask_chp['Prospect'] = array('type'=>'a', 'nb_parent'=>0, 'red'=>1) ;
					$TMask_chp['Supplier'] = array('type'=>'a', 'nb_parent'=>0, 'red'=>1) ;
					$TMask_chp['Documents'] = array('type'=>'a', 'nb_parent'=>0, 'red'=>1) ;
					$TMask_chp['NatureOfThirdParty'] = array('type'=>'td', 'nb_parent'=>1, 'red'=>1) ;
					$js_pdp.= '  $("a#ticket").addClass("tabs_mask_chp_admin");'."\n" ; // supprime l onglet ticket
				}
				else {
					$TMask_chp['customer'] = array('type'=>'a', 'nb_parent'=>1, 'mask'=>0, 'remove'=>1) ;
					$TMask_chp['Prospect'] = array('type'=>'a', 'nb_parent'=>1, 'mask'=>0, 'remove'=>1) ;
					//$TMask_chp['Customer'] = array('type'=>'a', 'nb_parent'=>1, 'mask'=>0, 'remove'=>1) ;
					$TMask_chp['Supplier'] = array('type'=>'a', 'nb_parent'=>1, 'mask'=>0, 'remove'=>1) ;
					$TMask_chp['Documents'] = array('type'=>'a', 'nb_parent'=>1, 'mask'=>0, 'remove'=>1) ;
					$TMask_chp['NatureOfThirdParty'] = array('type'=>'td', 'nb_parent'=>1, 'red'=>0, 'remove'=>1) ;
					$js_pdp.= '  $("a#ticket").parent().remove();'."\n" ; // supprime l onglet ticket
				}

				// onglet note renomme
				$js_pdp.= '  $("a#note").html("Archives Notes Eudonet");'."\n" ;

				// Onglet Agenda renomme pour enlever Événements
				$js_pdp.= "  $('a:contains(\"".strtr (html_entity_decode($langs->trans("Events").'/'. $langs->trans("Agenda"), ENT_COMPAT, 'UTF-8'), array("'"=>"\'") )."\")').html(\"".strtr (html_entity_decode($langs->trans("Agenda"), ENT_COMPAT, 'UTF-8'), array("'"=>"\'") )."\");" ;
				//$js_pdp.= "  alert($('tr.liste_titre').html() );" ;
			}

			if ($parameters['currentcontext']=='thirdpartylist'
				AND isset($_REQUEST['massaction'])
				AND ($_REQUEST['massaction']=='presend' OR $_REQUEST['massaction']=='confirm_presend')
			) {
				$this->add_bouton_select_all_email() ;
			}
			//if ($parameters['currentcontext']=='thirdpartylist') {
				//$this->add_filter_tag_affaire() ;
			//}
			//if ($parameters['currentcontext']=='thirdpartylist') print_r($_REQUEST) ;

			if ($parameters['currentcontext']=='projectthirdparty')
			{
				// Ajout de colonnes dans la liste des affaires d'un contact
				$js_pdp.= "  $('tr.liste_titre').append( '<td>Montant réel</td>' );\n" ;
				$js_pdp.= "  $('tr.liste_titre').append( '<td>Contact</td>' );\n" ;
				$js_pdp.= "  $('tr.liste_titre').append( '<td>Support</td>' );\n" ;

				$js_pdp.= '  $("tr.oddeven").each(function() {'."\n" ;
				$js_pdp.= '    var l_table = this ;'."\n" ;
				$js_pdp.= '    if( $(this).children("td").children("span").hasClass("opacitymedium")) { /* empty */ }'."\n" ;
				$js_pdp.= '    else {'."\n" ;
				$js_pdp.= '    $(this).children("td").attr("nowrap","") ;'."\n" ;
				$js_pdp.= '    var url_l = $(this).children("td").children("a").attr("href") ;'."\n" ;
				$js_pdp.= '    var id_l = url_l.replace("/projet/card.php?id=","") ;'."\n" ;
				//$js_pdp.= "    $(this).append('<td>'+id_l+'</td>') ;\n" ;
				$js_pdp.= "    $.ajax({\n" ;
				$js_pdp.= "    url: '".DIR_HTTP."custom/batibarrmod/scripts/interface.php'\n" ;
				$js_pdp.= "      ,data: ({mode:'GET_EXTRA_AFFAIRES',id_aff:id_l})\n" ;
				$js_pdp.= "      ,contentType: 'application/json; charset=ISO-8859-1'\n" ;
				$js_pdp.= "      ,cache: true\n" ;
				$js_pdp.= "      ,async: false\n" ;
				$js_pdp.= "      ,dataType:'json'\n" ;

				$js_pdp.= "      , success: function(TRes){\n" ;
				$js_pdp.= "        $(l_table).append('<td>'+TRes['LIST']['lib_amount_real']+'</td>') ;\n" ;
				$js_pdp.= "        if (TRes['LIST']['id_contact'] > 0) {\n" ;
				$js_pdp.= "          var bulle_contact=TRes['LIST']['bulle'] ;\n" ;
				$js_pdp.= "          $(l_table).append('<td><a href=\"/user/card.php?id='+TRes['LIST']['id_contact']+'\" class=\"classfortooltip valignmiddle\" title=\''+bulle_contact+'\'><span class=\"nopadding usertext valignmiddle\">'+TRes['LIST']['prenom']+' '+TRes['LIST']['nom']+'</span></a></td>') ;\n" ;
				$js_pdp.= "        }\n" ;
				$js_pdp.= "        else $(l_table).append('<td>&nbsp;</td>') ;\n" ;
				$js_pdp.= "        $(l_table).append('<td>'+TRes['LIST']['lib_support']+'</td>') ;\n" ;

				$js_pdp.= "      }\n" ;
				$js_pdp.= "      ,error: function(){\n" ;
				$js_pdp.= "        $(l_table).append('<td>Error Ajax</td>') ;\n" ;
				$js_pdp.= "        $(l_table).append('<td>&nbsp;</td>') ;\n" ;
				$js_pdp.= "        $(l_table).append('<td>&nbsp;</td>') ;\n" ;
				$js_pdp.= "      }\n" ;
				$js_pdp.= "    });\n" ; // ajax
				$js_pdp.= '    }'."\n" ; // not empty

				//$js_pdp.= '    alert( id_l+" | "+url_l ) ;'."\n" ;

				$js_pdp.= '  });'."\n" ; // each

			}

			if ($parameters['currentcontext']=='productcard'
				|| $parameters['currentcontext']=='productstatsinvoice'
				|| $parameters['currentcontext']=='productpricecard'
				|| $parameters['currentcontext']=='productstatscard'
			)
			{
				// masque les onglets des produits
				if ($user->admin==1) {
					$TMask_chp['Documents'] = array('type'=>'a', 'nb_parent'=>0, 'red'=>1) ;
					$TMask_chp['Notes'] = array('type'=>'a', 'nb_parent'=>0, 'red'=>1) ;
					// Onglet Agenda remove
					$js_pdp.= "  $('a:contains(\"".strtr (html_entity_decode($langs->trans("Events").'/'. $langs->trans("Agenda"), ENT_COMPAT, 'UTF-8'), array("'"=>"\'") )."\")').addClass('tabs_mask_chp_admin');" ;
				}
				else {
					$TMask_chp['Documents'] = array('type'=>'a', 'nb_parent'=>1, 'mask'=>0, 'remove'=>1) ;
					$TMask_chp['Notes'] = array('type'=>'a', 'nb_parent'=>1, 'mask'=>0, 'remove'=>1) ;
					// Onglet Agenda remove
					$js_pdp.= "  $('a:contains(\"".strtr (html_entity_decode($langs->trans("Events").'/'. $langs->trans("Agenda"), ENT_COMPAT, 'UTF-8'), array("'"=>"\'") )."\")').parent().remove();" ;
				}
			}

			if ($parameters['currentcontext']=='productstatscard')
			{
				//$TMask_chp['id'] = array('type'=>'select', 'nb_parent'=>2, 'mask'=>1) ; // ne fonctionne pas en red mode admin
				//$TMask_chp['search_socid'] = array('type'=>'select', 'nb_parent'=>1, 'red'=>1) ; // ne fonctionne pas en red mode admin
				if ($user->admin==1) {
					$js_pdp.= '  $("#id").parent().addClass("tabs_mask_chp_admin");'."\n" ;
					$js_pdp.= '  $("#search_socid").parent().addClass("tabs_mask_chp_admin");'."\n" ;
				}
				else {
					$js_pdp.= '  $("#id").parent().parent().addClass("mask_chp");'."\n" ;
					$js_pdp.= '  $("#search_socid").parent().parent().addClass("mask_chp");'."\n" ;
				}
			}

			if ($parameters['currentcontext']=='thirdpartycard')
			{
				if ($user->admin==1) {
					$js_pdp.= '  $(".butActionDelete").addClass("tabs_mask_chp_admin");'."\n" ;
				}
				else {
					$js_pdp.= '  $(".butActionDelete").remove();'."\n" ;
				}
			}

			if ($parameters['currentcontext']=='projectlist')
			{
				if ($user->admin==1) { }
				else {
					$js_pdp.= '  $("#search_opp_status option").each(function() {'."\n" ;
					$js_pdp.= '    if ($(this).html().substring(0, 3)=="-- ") $(this).remove() ;'."\n" ;
					//$js_pdp.= '    alert($(this).html().substring(0, 3)) ;'."\n" ;
					$js_pdp.= '  });'."\n" ;
				}

				$js_pdp.= '  var ind_col = 0 ;'."\n" ;
				$js_pdp.= '  var ind_col_real = -1 ;'."\n" ;
				$js_pdp.= '  $(\'table.nobottomiftotal td\').each(function() {'."\n" ;
				$js_pdp.= '    if ($(this).parent("tr").attr("class")=="oddeven") {'."\n" ;
				$js_pdp.= '    var key = $(this).attr("data-key")'."\n" ;
				//$js_pdp.= '    alert(key+" | class: "+$(this).attr("class"));'."\n" ;
				$js_pdp.= '    if ($(this).attr("data-key")=="amount_real" && ind_col_real==-1) {'."\n" ;
				$js_pdp.= '      ind_col_real = ind_col ;'."\n" ;
				$js_pdp.= '    }'."\n" ;
				$js_pdp.= '    ind_col++ ;'."\n" ;
				$js_pdp.= '    }'."\n" ;
				$js_pdp.= '  });'."\n" ;
				//$js_pdp.= 'alert(ind_col_real);'."\n" ;

				$js_pdp.= '  if (ind_col_real > 0) {'."\n" ;
				$js_pdp.= '    var tot_col = 0 ;'."\n" ;
				$js_pdp.= '    $(\'[data-key="amount_real"]\').each(function() {'."\n" ;
				$js_pdp.= '      var val_l = $(this).html() ;'."\n" ;
				$js_pdp.= '      if (val_l=="") val_l = "0" ;'."\n" ;
				$js_pdp.= '      val_l = val_l.replace(\',\',\'.\') ;'."\n" ;
				$js_pdp.= '      val_l = val_l.replace(\' \',\'\') ;'."\n" ;
				$js_pdp.= '      val_l = parseFloat(val_l) ;'."\n" ;
				$js_pdp.= '      tot_col+= val_l ;'."\n" ;
				//$js_pdp.= 'alert( val_l );'."\n" ;
				$js_pdp.= '    });'."\n" ;
				//$js_pdp.= 'alert(tot_col);'."\n" ;

				$js_pdp.= '    var ind_col = 0 ;'."\n" ;
				//$js_pdp.= '    var last_l = $("table.nobottomiftotal").find("tr").last();'."\n" ;
				$js_pdp.= '    $(\'tr.liste_total td\').each(function() {'."\n" ;
				$js_pdp.= '      if (ind_col_real==ind_col) {'."\n" ;
				$js_pdp.= '        $(this).attr("class", "right");'."\n" ;
				$js_pdp.= '        $(this).html(tot_col.toLocaleString()) ;'."\n" ;
				$js_pdp.= '      }'."\n" ;
				//$js_pdp.= 'alert( ind_col_real+" == "+ind_col+" | "+$(this).html()+" | "+tot_col );'."\n" ;
				$js_pdp.= '      ind_col++ ;'."\n" ;
				$js_pdp.= '    });'."\n" ;
				$js_pdp.= '  }'."\n" ;
			}

			if ($parameters['currentcontext']=='contactcard')
			{
				if ($user->admin==1) {
					$TMask_chp['CreateDolibarrLogin'] = array('type'=>'a', 'nb_parent'=>0, 'red'=>1) ;
				}
				else {
					$TMask_chp['CreateDolibarrLogin'] = array('type'=>'a', 'nb_parent'=>0, 'mask'=>1, 'remove'=>0) ;
					//$TMask_chp['Delete'] = array('type'=>'a', 'nb_parent'=>0, 'mask'=>0, 'remove'=>1) ;
				}
			}

			if ($parameters['currentcontext']=='contactcard'
				|| $parameters['currentcontext']=='consumptioncontact'
				|| $context_current=='projectcontact'
				|| $parameters['currentcontext']=='sendinbluecontactcard'
			)
			{
				// masque les onglets des contacts
				//$js_pdp.= '  $("a#perso").remove();'."\n" ;
				if ($user->admin==1) {
					$TMask_chp['Documents'] = array('type'=>'a', 'nb_parent'=>0, 'red'=>1) ;
					$TMask_chp['Note'] = array('type'=>'a', 'nb_parent'=>0, 'red'=>1) ;
					$TMask_chp['PersonalInformations'] = array('type'=>'a', 'nb_parent'=>1, 'red'=>1) ;
					// Onglet Agenda remove
					$js_pdp.= "  $('a:contains(\"".strtr (html_entity_decode($langs->trans("Events").'/'. $langs->trans("Agenda"), ENT_COMPAT, 'UTF-8'), array("'"=>"\'") )."\")').addClass('tabs_mask_chp_admin');" ;
				}
				else {
					$TMask_chp['Documents'] = array('type'=>'a', 'nb_parent'=>1, 'mask'=>0, 'remove'=>1) ;
					$TMask_chp['Note'] = array('type'=>'a', 'nb_parent'=>1, 'mask'=>0, 'remove'=>1) ;
					$TMask_chp['PersonalInformations'] = array('type'=>'a', 'nb_parent'=>1, 'mask'=>0, 'remove'=>1) ;
					// Onglet Agenda remove
					$js_pdp.= "  $('a:contains(\"".strtr (html_entity_decode($langs->trans("Events").'/'. $langs->trans("Agenda"), ENT_COMPAT, 'UTF-8'), array("'"=>"\'") )."\")').parent().remove();" ;
				}
			}

			//var_dump($user->admin) ;exit() ;
			if (  $parameters['currentcontext']=='projectcard'
				|| $parameters['currentcontext']=='projectcontactcard'
				|| $parameters['currentcontext']=='projectOverview'
				)
			{
				// Gestion bouton Reouvrir affaire
				$f_del_bt_reopen = TRUE ;
				if ($user->id > 0)
				{
					$usergroup = new UserGroup($this->db);
					$groupslist = $usergroup->listGroupsForUser($user->id);
					foreach ($groupslist as $id_gr=>$TData)
					{
						if ($id_gr==8) { // Paiement et comptabilité
							$f_del_bt_reopen = FALSE ;
							break;
						}
					}
					//echo "<hr>* user id: ".$user->id." (res: ".($f_del_bt_reopen ? 'del' : 'no del').") ".$id_gr." => ".print_r($groupslist, TRUE)."<hr />" ;
				}

				if ($f_del_bt_reopen) {
					if ($user->admin==1) {
						$TMask_chp['ReOpen'] = array('type'=>'a', 'nb_parent'=>0, 'red'=>1) ;
						$TMask_chp['Close'] = array('type'=>'a', 'nb_parent'=>0, 'red'=>1) ;
					}
					else {
						$TMask_chp['ReOpen'] = array('type'=>'a', 'nb_parent'=>0, 'mask'=>0, 'remove'=>1) ;
						$TMask_chp['Close'] = array('type'=>'a', 'nb_parent'=>0, 'mask'=>0, 'remove'=>1) ;
					}
				}
				//////////////////////////////////////

				if ($user->admin==1) {
					$TMask_chp['ToClone'] = array('type'=>'a', 'nb_parent'=>0, 'red'=>1) ;
					$TMask_chp['Usage'] = array('type'=>'td', 'nb_parent'=>1, 'red'=>1) ; // usage_opportunity
					$TMask_chp['Validate'] = array('type'=>'a', 'nb_parent'=>0, 'red'=>1) ;
					//$TMask_chp['Close'] = array('type'=>'a', 'nb_parent'=>0, 'red'=>1) ;
				}
				else {
					$TMask_chp['ToClone'] = array('type'=>'a', 'nb_parent'=>0, 'mask'=>0, 'remove'=>1) ;
					$TMask_chp['Usage'] = array('type'=>'td', 'nb_parent'=>1, 'mask'=>1, 'remove'=>0) ; // usage_opportunity
					$TMask_chp['Validate'] = array('type'=>'a', 'nb_parent'=>0, 'mask'=>0, 'remove'=>1) ;
					//$TMask_chp['Close'] = array('type'=>'a', 'nb_parent'=>0, 'mask'=>0, 'remove'=>1) ;
				}
			}

			if ($parameters['currentcontext']=='propalcard')
			{
				/********************************/
				// Gestion des services/produits par support
				if ( GETPOST('id') > 0 ) {
					$Propal = new Propal($this->db);
					$result = $Propal->fetch( GETPOST('id') );
					if ($result < 0) {
						$this->errors[] = $Propal->error;
						$error++;
					} else {
						$supportPropal = $Propal->array_options["options_ba_support"] ;
						if ($supportPropal!='')
						{
							$TList_product_support = array() ;
							$sql = "SELECT fk_object,ba_support FROM ".MAIN_DB_PREFIX."product_extrafields" ;
							$result = $this->db->query($sql);
							if ($result)
							{
								while ($obj_prod = $this->db->fetch_object($result)) {
									if ($obj_prod->ba_support==$supportPropal)// || $obj_prod->ba_support==''
									{
										$TList_product_support[$obj_prod->fk_object] = $obj_prod->ba_support ;
									}
								}
								//echo $sql."<br><pre>".print_r($TList_product_support, TRUE) ;
								if (count($TList_product_support) > 0)
								{
									$js_test_id = '$(this).val()==0' ;
									foreach ($TList_product_support as $id_p=>$s)
									{
										if ($js_test_id!='') $js_test_id.= ' || ' ;
										$js_test_id.= '$(this).val()=='.$id_p ;
									}

									$js_pdp.= '  $("#idprod option").each(function() {'."\n" ;
									$js_pdp.= '    if ('.$js_test_id.') {'."\n" ;
									//$js_pdp.= '      alert("67: "+$(this).val()) ;'."\n" ;
									$js_pdp.= '    }'."\n" ;
									$js_pdp.= '    else {'."\n" ;
									$js_pdp.= '      $(this).remove() ;'."\n" ;
									//$js_pdp.= '      alert("remove: "+$(this).val()) ;'."\n" ;
									$js_pdp.= '    }'."\n" ;
									$js_pdp.= '  });'."\n" ;
								}
								else {
									$js_pdp.= '  $("#idprod option").each(function() {'."\n" ;
									$js_pdp.= '    $(this).remove() ;'."\n" ;
									$js_pdp.= '  });'."\n" ;
								}
							}
						}

						if ( GETPOST('action')=='edit' || GETPOST('action')=='create' || GETPOST('action')=='add' )
						{ }
						else {
							if (
								$Propal->statut==2
								|| $Propal->statut==4
								|| $user->admin==1
								|| (isset($_REQUEST['DEBUG']) AND $_REQUEST['DEBUG']=='admin')
							) { // 1=Validée (proposition ouverte), 2=Signée, 3=Non signé (ferme), 4=Facturée
								$urlbacktopage = $_SERVER['PHP_SELF'].'?id='.$Propal->id ;
								$url_add_event = DOL_URL_ROOT.'/comm/action/card.php?action=create'
									.'&datep='.urlencode(dol_print_date(dol_now(), 'dayhourlog', 'tzuser'))
									.'&origin=propal'
									.'&originid='.urlencode($Propal->id)
									.($Propal->socid > 0 ? '&socid='.urlencode($Propal->socid) : '')
									.($Propal->fk_project > 0 ? '&projectid='.urlencode($Propal->fk_project) : '')
									.'&actioncode=FAC&fullday=fullday&complete=0&offsetvalue=5'
									.'&backtopage='.urlencode($urlbacktopage) ;


								$js_pdp.= '  var act_html = $(".tabsAction").html();'."\n" ;
								$js_pdp.= '  bt_add_fiche = "";'."\n" ;
								$js_pdp.= '  bt_add_fiche+= "<a class=\"butAction'
									.( $user->admin==1 || (isset($_REQUEST['DEBUG']) AND $_REQUEST['DEBUG']=='admin') ? ' tabs_mask_chp_admin' : '')
									.'\" href=\"'.$url_add_event.'\" title=\"Statut:'.$Propal->statut.'\">Fiche de factu</a> ";'."\n" ;
								$js_pdp.= '  act_html = bt_add_fiche+act_html;'."\n" ;
								$js_pdp.= '  $(".tabsAction").html(act_html);'."\n" ;
								//$js_pdp.= '  alert("'.$url_add_event."\");\n" ;
							}
						}

						// Gestion ajout bouton Declasser facturee pages prosposition
						$f_add_bt_declasser = FALSE ;
						if ($user->admin==1) {
							$f_add_bt_declasser = TRUE ;

							//$TMask_chp['Documents'] = array('type'=>'a', 'nb_parent'=>0, 'red'=>1) ;
							//$TMask_chp['Note'] = array('type'=>'a', 'nb_parent'=>0, 'red'=>1) ;
						}
						else if ($user->id > 0)
						{
							$usergroup = new UserGroup($this->db);
							$groupslist = $usergroup->listGroupsForUser($user->id);
							foreach ($groupslist as $id_gr=>$TData)
							{
								if ($id_gr==8) { // Paiement et comptabilité
									$f_add_bt_declasser = TRUE ;
									break;
								}
							}
							//echo "<hr>* user id: ".$user->id." (res: ".($f_add_bt_declasser ? 'add' : 'no add').") ".$id_gr." => ".print_r($groupslist, TRUE)."<hr />" ;
							
							//$TMask_chp['Documents'] = array('type'=>'a', 'nb_parent'=>1, 'mask'=>0, 'remove'=>1) ;
							//$TMask_chp['Note'] = array('type'=>'a', 'nb_parent'=>1, 'mask'=>0, 'remove'=>1) ;
						}

						if ($f_add_bt_declasser)
						{
							if ( GETPOST('action')=='' && $Propal->statut==4)
							{
								$js_pdp.= '  var act_html = $(".tabsAction").html();'."\n" ;
								$js_pdp.= '  bt_change_statut = "";'."\n" ;
								$js_pdp.= '  bt_change_statut+= "<a class=\"butAction'.( $user->admin==1 ? ' tabs_mask_chp_admin' : '').'\" href=\"javascript:modif_statut_propal('.$Propal->id.', 2);\" title=\"Modifier le statut en Signée\">Déclasser Facturée</a> ";'."\n" ;
								$js_pdp.= '  act_html = bt_change_statut+act_html;'."\n" ;
								$js_pdp.= '  $(".tabsAction").html(act_html);'."\n" ;
							}
							/*if ( GETPOST('action')=='' && $Propal->statut==2)
							{
								$js_pdp.= '  var act_html = $(".tabsAction").html();'."\n" ;
								$js_pdp.= '  bt_change_statut = "";'."\n" ;
								$js_pdp.= '  bt_change_statut+= "<a class=\"butAction tabs_mask_chp_admin\" href=\"javascript:modif_statut_propal('.$Propal->id.', 4);\" title=\"Modifier le statut en Facturée\">Classer Facturée</a> ";'."\n" ;
								$js_pdp.= '  act_html = bt_change_statut+act_html;'."\n" ;
								$js_pdp.= '  $(".tabsAction").html(act_html);'."\n" ;
							}*/
						}
						//////////////////////////////////////////////////////////////

						if ($Propal->statut == 2 OR $Propal->statut == 4)
						{
							if (isset($user->rights->ticket) && $user->rights->ticket->write==1)
							{
								$js_pdp.= '  var act_html = $(".tabsAction").html();'."\n" ;
								$js_pdp.= '  bt_add_ticket = "";'."\n" ; //'.$Propal->statut.' ;
								$js_pdp.= '  bt_add_ticket+= "<a class=\"butAction\" href=\"/ticket/card.php?action=create&mainmenu=ticket&origin=projet_project&originid='.$Propal->fk_project.'&socid='.$Propal->socid.'\">Créer un ticket</a> ";'."\n" ;
								$js_pdp.= '  act_html = bt_add_ticket+act_html;'."\n" ;
								$js_pdp.= '  $(".tabsAction").html(act_html);'."\n" ;
							}
						}
					}

					$f_del_bt_classer = TRUE ;

					if ($user->id > 0) {
						if ($user->admin==1) {
							$f_del_bt_classer = FALSE ;
							$js_pdp.= '  $("a:contains(\''.html_entity_decode($langs->trans('ClassifyBilled'), ENT_COMPAT, 'UTF-8').'\')").addClass("tabs_mask_chp_admin");'."\n" ; //remove() addClass("mask_chp") // Classer facturé
						}
						else
						{
							$usergroup = new UserGroup($this->db);
							$groupslist = $usergroup->listGroupsForUser($user->id);
							foreach ($groupslist as $id_gr=>$TData)
							{
								if ($id_gr==7) $f_del_bt_classer = FALSE ; // groupe facturation
							}
						}
					}

					//echo "<hr>* user id: ".$user->id." (res: ".($f_del_bt_classer ? 'del' : 'no del').") => ".print_r($groupslist, TRUE)."<hr />" ;
					//exit('user !') ;
					if ($f_del_bt_classer)
					{
						$js_pdp.= '  $("a:contains(\''.html_entity_decode($langs->trans('ClassifyBilled'), ENT_COMPAT, 'UTF-8').'\')").remove();'."\n" ; //remove() addClass("mask_chp") // Classer facturé
					}
				}
				/********************************/

				//$TMask_chp['ToClone'] = array('type'=>'a', 'nb_parent'=>0, 'mask'=>0, 'remove'=>1) ;

				// Gestion bouton Reouvrir proposition
				/*$f_del_bt_reopen = TRUE ;
				if ($user->id > 0) {
					$usergroup = new UserGroup($this->db);
					$groupslist = $usergroup->listGroupsForUser($user->id);
					foreach ($groupslist as $id_gr=>$TData)
					{
						if ($id_gr==8) { // Paiement et comptabilité
							$f_del_bt_reopen = FALSE ;
							break;
						}
					}
					//echo "<hr>* user id: ".$user->id." (res: ".($f_del_bt_reopen ? 'del' : 'no del').") ".$id_gr." => ".print_r($groupslist, TRUE)."<hr />" ;
				}

				if ($f_del_bt_reopen) {
					if ($user->admin==1) {
						$TMask_chp['ReOpen'] = array('type'=>'a', 'nb_parent'=>0, 'red'=>1) ;
					}
					else {
						$TMask_chp['ReOpen'] = array('type'=>'a', 'nb_parent'=>0, 'mask'=>0, 'remove'=>1) ;
					}
				}*/
				//////////////////////////////////////

				if ($user->admin==1) {
					$TMask_chp['ReOpen'] = array('type'=>'a', 'nb_parent'=>0, 'red'=>1) ;
					$TMask_chp['Discounts'] = array('type'=>'td', 'nb_parent'=>1, 'red'=>1) ; // Remises
					$TMask_chp['prod_entry_mode_free'] = array('type'=>'input', 'nb_parent'=>2, 'red'=>1) ; // Select ligne libre

					$js_pdp.= '  $("#prod_entry_mode_free").parent().parent().parent().children("br").first().addClass("tabs_mask_chp_admin");'."\n" ; // br entre les 2 selecteurs
					$js_pdp.= '  $("#prod_entry_mode_free").parent().parent().addClass("tabs_mask_chp_admin");'."\n" ; // Select ligne libre
				}
				else {
					$TMask_chp['ReOpen'] = array('type'=>'a', 'nb_parent'=>0, 'mask'=>0, 'remove'=>1) ;
					$TMask_chp['Discounts'] = array('type'=>'td', 'nb_parent'=>1, 'mask'=>1, 'remove'=>0) ; // Remises
					$TMask_chp['prod_entry_mode_free'] = array('type'=>'input', 'nb_parent'=>2, 'mask'=>1, 'remove'=>0) ; // Select ligne libre

					$js_pdp.= '  $("#prod_entry_mode_free").parent().parent().parent().children("br").first().remove();'."\n" ; // br entre les 2 selecteurs
					$js_pdp.= '  $("#prod_entry_mode_free").parent().parent().addClass("mask_chp");'."\n" ; // Select ligne libre
				}

				$js_pdp.= '  $("#select_type option[value=0]").remove();'."\n" ; // Visibilite - Tout le monde

				if ( GETPOST('id') > 0 ) {
					$js_pdp.= '  $(".editfielda").each(function() {'."\n" ;
					$js_pdp.= '    if ($(this).attr("href")=="/comm/propal/card.php?action=classify&mainmenu=commercial&id='.GETPOST('id') .'") {'."\n" ;
					//$js_pdp.= '      alert($(this).attr("href")) ;'."\n" ;
					$js_pdp.= '      $(this).remove() ;'."\n" ;
					$js_pdp.= '    }'."\n" ;
					$js_pdp.= '  });'."\n" ;
				}

				$js_pdp.= '  $("td:contains(\''.html_entity_decode($langs->trans('OutstandingBill'), ENT_COMPAT, 'UTF-8').'\')").parent().remove();'."\n" ;
				$js_pdp.= '  $("td:contains(\''.html_entity_decode($langs->trans('DeliveryDate'), ENT_COMPAT, 'UTF-8').'\')").parent().remove();'."\n" ;
				$js_pdp.= '  $("td:contains(\''.html_entity_decode($langs->trans('AvailabilityPeriod'), ENT_COMPAT, 'UTF-8').'\')").parent().remove();'."\n" ;
				//$js_pdp.= '  $("td:contains(\''.html_entity_decode($langs->trans('PaymentMode'), ENT_COMPAT, 'UTF-8').'\')").parent().remove();'."\n" ;
				$js_pdp.= '  $("td:contains(\''.html_entity_decode($langs->trans("PaymentConditions"), ENT_COMPAT, 'UTF-8').'\')").addClass("fieldrequired");'."\n" ; // cond_reglement_id
				$js_pdp.= '  $("td:contains(\''.html_entity_decode($langs->trans('Source'), ENT_COMPAT, 'UTF-8').'\')").parent().remove();'."\n" ; // demand_reason_id

				// add bouton copier fichiers joints
				$js_pdp.= '  $("input[name=\'downloadexternallink\']").each(function() {;'."\n" ;
				$js_pdp.= '    var key = $(this).attr("id");'."\n" ;
				//$js_pdp.= '  key = key.replace("downloadlink","");'."\n" ;
				//$js_pdp.= '  alert(key);'."\n" ;
				$js_pdp.= '    $(this).after(\'<span class="fas fa-copy js-copy" data-target="#tocopy" onclick="docopy(\'+key+\');" title="Copier ce lien dans le presse papier"></span>\')'."\n" ;
				$js_pdp.= '  });'."\n" ;
			}

			if ( (GETPOST('action')=='remise') && $parameters['currentcontext']=='propalcard')
			{
				$js_pdp.= '  $("#only_product").parent().addClass("mask_chp");'."\n" ;
				$js_pdp.= '  $("#tva_tx").addClass("mask_chp");'."\n" ;
			}

			if ( (GETPOST('action')=='edit' || GETPOST('action')=='create' || GETPOST('action')=='add') && $parameters['currentcontext']=='propalcard')
			{
				$js_pdp.= '  $("#availability_id").parent().parent().addClass("mask_chp");'."\n" ;
				$js_pdp.= '  $("#date_livraison").parent().parent().parent().addClass("mask_chp");'."\n" ;
			}
			else if ($parameters['currentcontext']=='propalcard')
			{
				if ($user->admin==1) {
					$js_pdp.= '  $("td:contains(\''.html_entity_decode($langs->trans('PaymentMode'), ENT_COMPAT, 'UTF-8').'\')").parent().addClass("tabs_mask_chp_admin");'."\n" ;
				}
				else {
					$js_pdp.= '  $("td:contains(\''.html_entity_decode($langs->trans('PaymentMode'), ENT_COMPAT, 'UTF-8').'\')").parent().addClass("mask_chp");'."\n" ;
				}
			}

			if ($parameters['currentcontext']=='projectcard') //(GETPOST('action')=='view' || GETPOST('action')=='' || GETPOST('action')=='confirm_reopen' || GETPOST('action')=='close' || GETPOST('action')=='clone' || GETPOST('action')=='delete') &&
			{
				require_once DOL_DOCUMENT_ROOT.'/projet/class/project.class.php';
				$myproject = new Project($this->db);
				$id_prj = GETPOST('id', 'int');
				if ($id_prj > 0) {
					$result = $myproject->fetch($id_prj);
					if ($result < 0) {
						$this->errors[] = $myproject->error;
						exit('Error load id '.$id_prj) ;
					}
				}
			}

			//echo "<pre>".print_r($_SESSION, TRUE)."\n".print_r($_REQUEST, TRUE) ;
			if ( (GETPOST('action')=='edit' || GETPOST('action')=='create' || (GETPOST('action')=='update' AND $_REQUEST['error']> 0) || GETPOST('action')=='add') && $parameters['currentcontext']=='projectcard')
			{
				if (!empty($conf->global->COMPANY_USE_SEARCH_TO_SELECT)) // pas de combo pour la selection de la compagnie
				{
					$js_pdp.= '  if ($("#socid").val() > 0) {'."\n" ; // si une societe est bien selectionnee
					$js_pdp.= '    $("#search_socid").parent().html( $("#search_socid").parent().html()+$("#search_socid").val() );'."\n" ; // affiche le libelle apres la recherche societe
					$js_pdp.= '    $("#search_socid").remove();'."\n" ; // recherche societes desactive
					//$js_pdp.= 'alert($("#socid").val()+" | "+$("#search_socid").val());'."\n" ;
					//$js_pdp.= 'alert($("#search_socid").parent().html());'."\n" ;
					$js_pdp.= '  }'."\n" ;
				}

				if ($user->admin==1) {
					$js_pdp.= '  $("#usage_opportunity").parent().parent().addClass("tabs_mask_chp_admin");'."\n" ; // suivre opportunite
					$js_pdp.= '  $("#public").parent().parent().addClass("tabs_mask_chp_admin");'."\n" ; // Visibilite - Tout le monde
					$js_pdp.= '  $("select[name=\'status\']").parent().parent().addClass("tabs_mask_chp_admin");'."\n" ;
				}
				else {
					$js_pdp.= '  $("#usage_opportunity").parent().parent().addClass("mask_chp");'."\n" ; // suivre opportunite
					$js_pdp.= '  $("#public").parent().parent().addClass("mask_chp");'."\n" ; // Visibilite - Tout le monde
					if ($myproject->opp_status!=6) $js_pdp.= '  $("#opp_status option[value=6]").remove();'."\n" ;
					$js_pdp.= '  $("select[name=\'status\']").parent().parent().addClass("mask_chp");'."\n" ;
				}

				if ($myproject->opp_status!=6) { // pas signe
					$js_pdp.= '  $("#options_amount_real").parent().parent().addClass("mask_chp");'."\n" ;
					$js_pdp.= '  $("#options_date_signe").parent().parent().parent().addClass("mask_chp");'."\n" ;
					$js_pdp.= '  $("#options_echeancier").parent().parent().addClass("mask_chp");'."\n" ;
					$js_pdp.= '  $("#options_ca_annee").parent().parent().addClass("mask_chp");'."\n" ;
					$js_pdp.= '  $("#options_particularite_facture").parent().parent().addClass("mask_chp");'."\n" ;
					$js_pdp.= '  $("#options_duree_contrat").parent().parent().addClass("mask_chp");'."\n" ;
					$js_pdp.= '  $("#options_date_fin_contrat").parent().parent().parent().addClass("mask_chp");'."\n" ;
				}

				if ($myproject->opp_status==7) { // perdu
				}
				else {
					$js_pdp.= '  $("#options_motif_perdu").parent().parent().addClass("mask_chp");'."\n" ;
				}

				// en gras
				$TChp_require['opp_status']=array('lib'=>'', 'trans'=>'OpportunityStatus', 'nb_parent'=>2) ;
				$TChp_require['projectstart']=array('lib'=>'', 'trans'=>'DateStart', 'nb_parent'=>3, 'type'=>'contains') ;
				$TChp_require['projectend']=array('lib'=>'', 'trans'=>'DateEnd', 'nb_parent'=>3) ;
				$TChp_require['options_ba_support']=array('lib'=>'Support', 'trans'=>'', 'nb_parent'=>2) ;
				$TChp_require['options_demand_reason']=array('lib'=>'Origine', 'trans'=>'', 'nb_parent'=>2) ;
				$TChp_require['options_tacite_reconduction']=array('lib'=>'Tacite reconduction', 'trans'=>'', 'nb_parent'=>2) ;

				// pas id sur ces 2 champs projet
				//$js_pdp.= '    $("input[name=\'budget_amount\']").val(0);'."\n" ;
				$js_pdp.= '    $("input[name=\'budget_amount\']").parent().parent().children("td:contains(\''.strtr( html_entity_decode($langs->trans("Budget"), ENT_COMPAT, 'UTF-8'), array("'"=>"\\\'") ).'\')").addClass("fieldrequired");'."\n" ;
				$js_pdp.= '    $("input[name=\'opp_amount\']").parent().parent().children("td:contains(\''.strtr( html_entity_decode($langs->trans("OpportunityAmount"), ENT_COMPAT, 'UTF-8'), array("'"=>"\\\'") ).'\')").addClass("fieldrequired");'."\n" ;

				$js_pdp.= '  $("#opp_status").change( function() {'."\n" ;
				$js_pdp.= '    var stat_sel = $("#opp_status option:selected").val() ;'."\n" ;
				$js_pdp.= '    if (stat_sel==7) {'."\n" ;
				$js_pdp.= '      $("#options_motif_perdu").parent().parent().removeClass("mask_chp");'."\n" ;
				$js_pdp.= '    }'."\n" ;
				$js_pdp.= '    else {'."\n" ;
				$js_pdp.= '      $("#options_motif_perdu").parent().parent().addClass("mask_chp");'."\n" ;
				$js_pdp.= '    }'."\n" ;
				$js_pdp.= '  });'."\n" ;
			}
			else if ($parameters['currentcontext']=='projectcard') //(GETPOST('action')=='view' || GETPOST('action')=='' || GETPOST('action')=='confirm_reopen' || GETPOST('action')=='close' || GETPOST('action')=='clone' || GETPOST('action')=='delete') &&
			{
				$id_prj = GETPOST('id', 'int');
				if ($myproject->status==2) { // Clôturé
					if ($myproject->opp_status == 6) // signe
					{
						$js_pdp.= '  var act_html = $(".tabsAction").html();'."\n" ;
						$js_pdp.= '  bt_add_ticket = "";'."\n" ;
						$js_pdp.= '  bt_add_ticket+= "<a class=\"butAction\" href=\"/ticket/card.php?action=create&mainmenu=ticket&origin=projet_project&originid='.$myproject->id.'&socid='.$myproject->socid.'\">Créer un ticket</a> ";'."\n" ;
						$js_pdp.= '  act_html = bt_add_ticket+act_html;'."\n" ;
						$js_pdp.= '  $(".tabsAction").html(act_html);'."\n" ;
					}
				}
				else {
					$js_pdp.= '  var act_html = $(".tabsAction").html();'."\n" ;
					$js_pdp.= '  bt_add_propo = "";'."\n" ;
					/*$js_propo = '' ;
					$js_propo.= '  function go_new_propo() {'."\n" ;
					$js_propo.= '    if (confirm("Voulez-vous vraiment créer une nouvelle proposition ?")) {'."\n" ;
					$js_propo.= '      var url_cree = "../comm/propal/card.php?action=create&origin=project&mainmenu=commercial&originid='.$id_prj.'&socid='.$myproject->socid.'" ;'."\n" ;
					$js_propo.= '      document.location=url_cree ;'."\n" ;
					$js_propo.= '    }'."\n" ;
					$js_propo.= '  }'."\n" ;
					$js_pdp.= '  bt_add_propo+= "<a class=\"butAction\" href=\"javascript:go_new_propo();\">Nouvelle proposition</a> ";'."\n" ;
					*/
					$js_pdp.= '  bt_add_propo+= "<a class=\"butAction\" href=\"../comm/propal/card.php?action=create&origin=project&mainmenu=commercial&originid='.$id_prj.'&socid='.$myproject->socid.'\">Nouvelle proposition</a> ";'."\n" ;
					$js_pdp.= '  act_html = bt_add_propo+act_html;'."\n" ;
					$js_pdp.= '  $(".tabsAction").html(act_html);'."\n" ;
				}

				if ($myproject->opp_status==6) { // signe
					//OpportunityWeightedAmount
					$js_pdp.= ' $("td:contains(\''.strtr( html_entity_decode($langs->trans("OpportunityWeightedAmount"), ENT_COMPAT, 'UTF-8'), array("'"=>"\\\'") ).'\')").parent().children("td:contains(\''.price( ($myproject->opp_amount * $myproject->opp_percent / 100), 0, $langs, 1, 0, -1, $conf->currency).'\')").html("'.price( $myproject->array_options['options_amount_real'], 0, $langs, 1, 0, -1, $conf->currency).'");'."\n" ;
					//$js_pdp.= ' alert("test status '.$myproject->opp_status.' => '.$myproject->array_options['options_amount_real'].' | '.price( ($myproject->opp_amount * $myproject->opp_percent / 100), 0, $langs, 1, 0, -1, $conf->currency).' | OpportunityWeightedAmount: '.html_entity_decode($langs->trans("OpportunityWeightedAmount"), ENT_COMPAT, 'UTF-8').' | "+$("td:contains(\''.strtr( html_entity_decode($langs->trans("OpportunityWeightedAmount"), ENT_COMPAT, 'UTF-8'), array("'"=>"\\\'") ).'\')").html() );'."\n" ;
				}

				/*require_once DOL_DOCUMENT_ROOT.'/core/class/extrafields.class.php';
				$extrafields = new ExtraFields($this->db);
				$extrafields->fetch_name_optionals_label('projet');
				//echo "<pre>".print_r($extrafields->attributes['projet'], TRUE) ; exit() ;
				$label_motif = $extrafields->attributes['projet']['label']['motif_perdu'] ;*/

				$TMask_chp['Visibility'] = array('type'=>'td', 'nb_parent'=>1, 'mask'=>0, 'remove'=>1) ;
				$TMask_chp['ReOpenAProject'] = array('type'=>'a', 'nb_parent'=>0, 'mask'=>0, 'remove'=>1) ;
				$js_pdp.= '  $("#project_extras_amount_real_'.$id_prj.'").html( $("#project_extras_amount_real_'.$id_prj.'").html()+" €");'."\n" ;
				$js_pdp.= '  $("a:contains(\''.html_entity_decode($langs->trans("ReOpenAProject"), ENT_COMPAT, 'UTF-8').'\')").addClass("mask_chp");'."\n" ;
				if ($myproject->opp_status==7) { // perdu
				}
				else {
					$js_pdp.= '  $("td:contains(\'affaire perdu\')").parent().addClass("mask_chp");'."\n" ;
					//$js_pdp.= '  alert( $("td:contains(\''.strtr (html_entity_decode($label_motif, ENT_COMPAT, 'UTF-8'), array("'"=>"\'") ).'\')").html() );'."\n" ;
				}
			}

			if ($parameters['currentcontext']=='projectOverview')
			{
				$id_prj = GETPOST('id', 'int');
				if ($id_prj > 0) {
					require_once DOL_DOCUMENT_ROOT.'/projet/class/project.class.php';
					$myproject = new Project($this->db);
					$result = $myproject->fetch($id_prj);
					if ($result < 0) {
						$this->errors[] = $myproject->error;
					}
					else {
						if ($myproject->status==2) {
							echo $langs->trans('AddProp')."***<br>\n" ;
							$js_pdp.= "  $('span:contains(\"".strtr (html_entity_decode($langs->trans('AddProp'), ENT_COMPAT, 'UTF-8'), array("'"=>"\'") )."\")').parent().remove();" ;
						}
					}
				}
			}

			if ($parameters['currentcontext']=='contactlist')
			{
				//$js_pdp.= "  $('[title*=\"".strtr (html_entity_decode($langs->trans('NewContactAddress'), ENT_COMPAT, 'UTF-8'), array("'"=>"\'") )."\"]').parent().remove();" ;
			}

			if (
				(GETPOST('action')=='edit' || GETPOST('action')=='create' || (GETPOST('action')=='update' AND $_REQUEST['error']> 0) || GETPOST('action')=='add')
				&& ($parameters['currentcontext']=='contactcard')
			) {
				if ($user->admin==1) { // admin
					$TMask_chp['DolibarrLogin'] = array('type'=>'td', 'nb_parent'=>1, 'red'=>1) ;
					$js_pdp.= '  $("#phone_mobile").parent().parent().addClass("tabs_mask_chp_admin");'."\n" ;
					//$js_pdp.= '  $("#address").parent().parent().addClass("tabs_mask_chp_admin");'."\n" ; // attention, difference entre local et en ligne
					$js_pdp.= '  $("#photoinput").parent().parent().parent().parent().parent().parent().addClass("tabs_mask_chp_admin");'."\n" ;
					$js_pdp.= '  $("#priv").parent().parent().addClass("tabs_mask_chp_admin");'."\n" ;
					//$js_pdp.= '  $("#zipcode").parent().parent().addClass("tabs_mask_chp_admin");'."\n" ;
					$js_pdp.= '  $("#note_public").parent().parent().addClass("tabs_mask_chp_admin");'."\n" ;
					$js_pdp.= '  $("#roles").parent().parent().addClass("tabs_mask_chp_admin");'."\n" ;
					//$js_pdp.= '  $("#title").parent().parent().addClass("tabs_mask_chp_admin");'."\n" ;
					$js_pdp.= '  $("#birthday").parent().parent().parent().addClass("tabs_mask_chp_admin");'."\n" ;
					$js_pdp.= '  $("#copyaddressfromsoc").parent().addClass("tabs_mask_chp_admin");'."\n" ;
				}
				else {
					$TMask_chp['DolibarrLogin'] = array('type'=>'td', 'nb_parent'=>1, 'mask'=>0, 'remove'=>1) ;
					$js_pdp.= '  $("#phone_mobile").parent().parent().addClass("mask_chp");'."\n" ;
					//$js_pdp.= '  $("#address").parent().parent().addClass("mask_chp");'."\n" ; // attention, difference entre local et en ligne
					$js_pdp.= '  $("#photoinput").parent().parent().parent().parent().parent().parent().addClass("mask_chp");'."\n" ;
					$js_pdp.= '  $("#priv").parent().parent().addClass("mask_chp");'."\n" ;
					//$js_pdp.= '  $("#zipcode").parent().parent().addClass("mask_chp");'."\n" ;
					$js_pdp.= '  $("#note_public").parent().parent().addClass("mask_chp");'."\n" ;
					$js_pdp.= '  $("#roles").parent().parent().addClass("mask_chp");'."\n" ;
					//$js_pdp.= '  $("#title").parent().parent().addClass("mask_chp");'."\n" ;
					$js_pdp.= '  $("#birthday").parent().parent().parent().remove();'."\n" ;
					$js_pdp.= '  $("#copyaddressfromsoc").parent().html("");'."\n" ;
				}
				//$TMask_chp['PersonalInformations'] = array('type'=>'td', 'nb_parent'=>2, 'mask'=>0, 'remove'=>1) ;
				//$js_pdp.= '  $("#options_ideudonet").parent().parent().addClass("mask_chp");'."\n" ;
				$js_pdp.= '  $("#title").parent().parent().html("<td colspan=4 style=\"text-align:center;font-weight:bold;color:red;font-size: 20px;\">Remplir l\'adresse du contact uniquement si diff&eacute;rente de l\'adresse de la soci&eacute;t&eacute;</td>");'."\n" ;

				// en gras
				$TChp_require['civility_code']=array('lib'=>'Civilité', 'trans'=>'', 'nb_parent'=>2) ; // ne fonctionne pas en UserTitle cause htmlentities
				$TChp_require['options_ba_fonction']=array('lib'=>'Fonction', 'trans'=>'', 'nb_parent'=>2) ;
			}
			else if ($parameters['currentcontext']=='contactcard') //(GETPOST('action')=='view' || GETPOST('action')=='') &&
			{
				if ($user->admin==1) { // admin
					$TMask_chp['ContactVisibility'] = array('type'=>'td', 'nb_parent'=>1, 'red'=>1) ;
					$TMask_chp['VCard'] = array('type'=>'td', 'nb_parent'=>1, 'red'=>1) ;
					$TMask_chp['ContactByDefaultFor'] = array('type'=>'td', 'nb_parent'=>1, 'red'=>1) ;
					$TMask_chp['DolibarrLogin'] = array('type'=>'td', 'nb_parent'=>1, 'red'=>1) ;
					$TMask_chp['PostOrFunction'] = array('type'=>'td', 'nb_parent'=>1, 'red'=>1) ;
				}
				else {
					$TMask_chp['ContactVisibility'] = array('type'=>'td', 'nb_parent'=>1, 'mask'=>0, 'remove'=>1) ;
					$TMask_chp['VCard'] = array('type'=>'td', 'nb_parent'=>1, 'mask'=>0, 'remove'=>1) ;
					$TMask_chp['ContactByDefaultFor'] = array('type'=>'td', 'nb_parent'=>1, 'mask'=>0, 'remove'=>1) ;
					$TMask_chp['DolibarrLogin'] = array('type'=>'td', 'nb_parent'=>1, 'mask'=>0, 'remove'=>1) ;
					$TMask_chp['PostOrFunction'] = array('type'=>'td', 'nb_parent'=>1, 'mask'=>0, 'remove'=>1) ;
				}
			}

			if ( (GETPOST('action')=='edit' || GETPOST('action')=='create' || GETPOST('action')=='add') && $parameters['currentcontext']=='usercard')
			{
				$js_pdp.= '  $("#dateofbirth").parent().parent().parent().addClass("mask_chp");'."\n" ;
			}
			else if ($parameters['currentcontext']=='usercard') //(GETPOST('action')=='view' || GETPOST('action')=='') &&
			{
				$js_pdp.= '  $("td:contains(\''.html_entity_decode($langs->trans('DateOfBirth'), ENT_COMPAT, 'UTF-8').'\')").parent().addClass("mask_chp");'."\n" ;
				$js_pdp.= '  $("td:contains(\''.html_entity_decode($langs->trans('VCard'), ENT_COMPAT, 'UTF-8').'\')").parent().addClass("mask_chp");'."\n" ;
			}

			/*$js_pdp.= '  // $("#zipcode").attr(\'required\',\'required\');
			$("#town").parent().parent().children("td:contains(\''.$langs->trans('Town').'\')").addClass("fieldrequired");
			$("#address").parent().parent().children("td:contains(\''.$langs->trans('Adress').'\')").addClass("fieldrequired");

			$("#custcats").parent().parent().children("td:contains(\'Tag\')").addClass("fieldrequired");
			//Todo Check how to do
			//$("#custcats").attr(\'required\',\'required\');'."\n" ;*/

			if ($context_current=='mailingcard' OR $parameters['currentcontext']=='mailingcard')
			{
				$js_pdp.= "  $('a:contains(\"".strtr (html_entity_decode($langs->trans("SendMailing"), ENT_COMPAT, 'UTF-8'), array("'"=>"\'") )."\")').html(\"".strtr (html_entity_decode($langs->trans("Agenda"), ENT_COMPAT, 'UTF-8'), array("'"=>"\'") )."\");" ;
				//$js_pdp.= '  $(a[href="/comm/mailing/card.php?action=sendall&id='.GETPOST('id', 'int').'"]).remove();'."\n" ;
				//$js_pdp.= '  alert( $(a[href=\'/comm/mailing/card.php?action=sendall&id='.GETPOST('id', 'int').'\']).html() );'."\n" ;
				//echo GETPOST('id', 'int') ;
			}
			if ($context_current=='thirdpartycomm' OR $parameters['currentcontext']=='thirdpartycomm')
			{
				if ($user->admin!=1) { // admin
				}
				//$TMask_chp['VATIntra'] = array('type'=>'td', 'nb_parent'=>1, 'mask'=>1, 'remove'=>0) ;
				$TMask_chp['CustomerAbsoluteDiscountShort'] = array('type'=>'td', 'nb_parent'=>1, 'mask'=>1, 'remove'=>0) ; // Remise fixe
				$TMask_chp['CustomerRelativeDiscountShort'] = array('type'=>'td', 'nb_parent'=>1, 'mask'=>1, 'remove'=>0) ; // Remise relative
				//$TMask_chp['CustomerAccountancyCode'] = array('type'=>'td', 'nb_parent'=>1, 'mask'=>1, 'remove'=>0) ;
				//$js_pdp.= 'alert("'.html_entity_decode($langs->trans('CustomerAccountancyCode'), ENT_COMPAT, 'UTF-8').'");'."\n" ;
				//$js_pdp.= 'alert( $("a[href=\'/comm/propal/card.php?socid='.GETPOST('socid', 'int').'&action=create\']").html() );'."\n" ;
				$js_pdp.= '  $("a[href=\'/comm/propal/card.php?socid='.GETPOST('socid', 'int').'&action=create\']").attr("href","/projet/card.php?action=create&socid='.GETPOST('socid', 'int').'");'."\n" ;
				$js_pdp.= '  $("a[href=\'/projet/card.php?action=create&socid='.GETPOST('socid', 'int').'\']").html("Créer une affaire") ;'."\n" ;
			}

			if (
				(GETPOST('action')=='' || GETPOST('action')=='view' || GETPOST('action')=='editthirdpartytype')
				&& ($context_current=='thirdpartycard' OR $parameters['currentcontext']=='thirdpartycard')
			)
			{
				if ($user->admin==1) { // admin
					$js_pdp.= '  $(".societe_extras_ba_not_support").parent().addClass("tabs_mask_chp_admin");'."\n" ;
				}
				else {
					$js_pdp.= '  $(".societe_extras_ba_not_support").parent().remove();'."\n" ;
				}
			}
			if (
				(GETPOST('action')=='edit' || GETPOST('action')=='create' || GETPOST('action')=='add'
				|| (GETPOST('action')=='update' AND $_REQUEST['error']> 0))
				&& ($context_current=='thirdpartycard' OR $parameters['currentcontext']=='thirdpartycard')
			)
			{
				$js_pdp.= '  $("#fax").parent().addClass("mask_chp");'."\n" ;
				$js_pdp.= '  $("#select2-forme_juridique_code-container").parent().parent().parent().parent().parent().parent().addClass("mask_chp");'."\n" ;
				$js_pdp.= '  $("#fax").parent().parent().children("td:contains(\''.html_entity_decode($langs->trans('Fax'), ENT_COMPAT, 'UTF-8').'\')").addClass("mask_chp");'."\n" ;
				$js_pdp.= '  $("#capital").parent().parent().remove();'."\n" ;
				$js_pdp.= '  $("#email").parent().parent().remove();'."\n" ;
				$js_pdp.= '  $("#customerprospect").parent().children("span.select2-container").addClass("mask_chp");'."\n" ;
				$js_pdp.= '  var info_pro = $("#customerprospect option:selected").html();'."\n" ;
				//$js_pdp.= '  alert( info_pro ) ;'."\n" ;

				$js_pdp.= '  $("#customerprospect").parent().append( info_pro ) ;'."\n" ;

				if ($user->admin==1) { // admin
					$js_pdp.= '  $("#idprof1").parent().addClass("tabs_mask_chp_admin");'."\n" ; // masque le 1, laisse le SIRET // texte : ProfId1FR
					$TMask_chp['ProfId1FR'] = array('type'=>'td', 'nb_parent'=>0, 'mask'=>0, 'remove'=>0, 'red'=>1) ;

					$js_pdp.= '  $("#idprof3").parent().parent().addClass("tabs_mask_chp_admin");'."\n" ; // masque le 3 et le 4 // texte : ProfId1FR
					$js_pdp.= '  $("#idprof5").parent().parent().addClass("tabs_mask_chp_admin");'."\n" ; // masque le 5 // texte : ProfId1FR
					//$TMask_chp['Workforce'] = array('type'=>'td', 'nb_parent'=>0, 'red'=>1) ; // Effectifs titre
					//$js_pdp.= '  $("select[name=\'effectif_id\']").parent().addClass("tabs_mask_chp_admin");'."\n" ;

					$js_pdp.= '  $("#options_ba_support").parent().parent().addClass("tabs_mask_chp_admin");'."\n" ; // masque le support client de
					$js_pdp.= '  $("#options_ba_not_support").parent().parent().addClass("tabs_mask_chp_admin");'."\n" ; // masque le support non client de
				}
				else {
					//$js_pdp.= '  $("#idprof1").parent().parent().remove();'."\n" ; // masque le 1 et le 2 // texte : ProfId1FR
					$js_pdp.= '  $("#idprof1").parent().remove();'."\n" ; // masque le 1, laisse le SIRET // texte : ProfId1FR
					$TMask_chp['ProfId1FR'] = array('type'=>'td', 'nb_parent'=>0, 'mask'=>0, 'remove'=>1) ;
					$js_pdp.= '  $("#idprof3").parent().parent().remove();'."\n" ; // masque le 3 et le 4 // texte : ProfId1FR
					$js_pdp.= '  $("#idprof5").parent().parent().remove();'."\n" ; // masque le 5 // texte : ProfId1FR
					//$TMask_chp['Workforce'] = array('type'=>'td', 'nb_parent'=>0, 'mask'=>0, 'remove'=>1) ; // Effectifs titre
					//$js_pdp.= '  $("select[name=\'effectif_id\']").parent().remove();'."\n" ;

					$js_pdp.= '  $("#options_ba_support").parent().parent().remove();'."\n" ; // masque le support client de
					$js_pdp.= '  $("#options_ba_not_support").parent().parent().remove();'."\n" ; // masque le support non client de
				}

				if ($user->admin==1 // admin
					OR $user->id==3 // xavier
					OR $user->id==17 // vincent
					OR $user->id==12 // albane
					OR $user->fk_user==12 // responsable albane
				) {
					$js_pdp.= '  $("#options_note_dossier").parent().parent().addClass("tabs_mask_chp_admin");'."\n" ;
				}
				else $js_pdp.= '  $("#options_note_dossier").parent().parent().addClass("mask_chp");'."\n" ;

				if ($_REQUEST['action']=='create' || GETPOST('action')=='add') {
					if ($user->admin==1){
						$js_pdp.= '  $("#assujtva_value").parent().addClass("tabs_mask_chp_admin");'."\n" ;
						$js_pdp.= '  $("#assujtva_value").parent().parent().children("td:contains(\''.$langs->trans('VATIsUsed').'\')").addClass("tabs_mask_chp_admin");'."\n" ;
						$js_pdp.= '  $("#assujtva_value").parent().parent().children("td:contains(\''.html_entity_decode($langs->trans('VATIsUsed'), ENT_COMPAT, 'UTF-8').'\')").addClass("tabs_mask_chp_admin");'."\n" ;
					}
					else {
						$js_pdp.= '  $("#assujtva_value").parent().addClass("mask_chp");'."\n" ;
						$js_pdp.= '  $("#assujtva_value").parent().parent().children("td:contains(\''.$langs->trans('VATIsUsed').'\')").addClass("mask_chp");'."\n" ;
						$js_pdp.= '  $("#assujtva_value").parent().parent().children("td:contains(\''.html_entity_decode($langs->trans('VATIsUsed'), ENT_COMPAT, 'UTF-8').'\')").addClass("mask_chp");'."\n" ;
					}
				}
				else { // edit or update error
					if ($user->admin==1){
						$js_pdp.= '  $("#assujtva_value").parent().parent().addClass("tabs_mask_chp_admin");'."\n" ;
					}
					else {
						$js_pdp.= '  $("#assujtva_value").parent().parent().addClass("mask_chp");'."\n" ;
					}
				}

				// en gras
				$TChp_require['address']=array('lib'=>'', 'trans'=>'Address', 'nb_parent'=>2) ;
				$TChp_require['town']=array('lib'=>'', 'trans'=>'Town', 'nb_parent'=>2) ;
				$TChp_require['zipcode']=array('lib'=>'', 'trans'=>'Zip', 'nb_parent'=>2) ;
				$TChp_require['selectcountry_id']=array('lib'=>'', 'trans'=>'Country', 'nb_parent'=>2) ;
				$TChp_require['typent_id']=array('lib'=>'', 'trans'=>'ThirdPartyType', 'nb_parent'=>2) ;
			}
			else if ( (GETPOST('action')=='view' || GETPOST('action')=='' || GETPOST('action')=='editthirdpartytype') && $parameters['currentcontext']=='thirdpartycard')
			{
				if ($user->admin==1){
					$TMask_chp['JuridicalStatus'] = array('type'=>'td', 'nb_parent'=>1, 'mask'=>0, 'remove'=>0, 'red'=>1) ;
					$TMask_chp['Capital'] = array('type'=>'td', 'nb_parent'=>1, 'mask'=>0, 'remove'=>0, 'red'=>1) ;
					//$TMask_chp['Workforce'] = array('type'=>'td', 'nb_parent'=>1, 'mask'=>0, 'remove'=>0, 'red'=>1) ;
					$TMask_chp['ParentCompany'] = array('type'=>'td', 'nb_parent'=>1, 'mask'=>0, 'remove'=>0, 'red'=>1) ;
				}
				else {
					$TMask_chp['JuridicalStatus'] = array('type'=>'td', 'nb_parent'=>1, 'mask'=>1, 'remove'=>0) ;
					$TMask_chp['Capital'] = array('type'=>'td', 'nb_parent'=>1, 'mask'=>1, 'remove'=>0) ;
					//$TMask_chp['Workforce'] = array('type'=>'td', 'nb_parent'=>1, 'mask'=>0, 'remove'=>1) ;
					$TMask_chp['ParentCompany'] = array('type'=>'td', 'nb_parent'=>1, 'mask'=>1, 'remove'=>0) ;
				}
				for ($ind=0 ; $ind<=5 ; $ind++) {
					if ($ind==2) // Siret
					{
					}
					else
					{
						$TMask_chp['ProfId'.$ind.'FR'] = array('type'=>'td', 'nb_parent'=>1, 'mask'=>1, 'remove'=>0) ;
						//$TMask_chp['ProfId'.$ind.'AT'] = array('type'=>'td', 'nb_parent'=>1, 'mask'=>1, 'remove'=>0) ;
						//$TMask_chp['ProfId'.$ind.'DE'] = array('type'=>'td', 'nb_parent'=>1, 'mask'=>1, 'remove'=>0) ;
					}
				}

				if ($user->id==1 // admin
					OR $user->id==3 // xavier
					OR $user->id==17 // vincent
					OR $user->id==12 // albane
					OR $user->fk_user==12 // responsable albane
				) { }
				else $js_pdp.= '  $("#societe_extras_note_dossier_'.GETPOST('socid', 'int').'").parent().addClass("mask_chp");'."\n" ;
					//$js_pdp.= '  $("td:contains(\'Note dossier\')").parent().addClass("mask_chp");'."\n" ;


				//$js_pdp.= ' alert("'.html_entity_decode($langs->trans('Capital'), ENT_COMPAT, 'UTF-8').'");'."\n" ;
				//$js_pdp.= ' alert($("td:contains(\''.html_entity_decode($langs->trans('Capital'), ENT_COMPAT, 'UTF-8').'\')").html());'."\n" ;
			}

			$js_mask = '' ;
			foreach($TMask_chp as $key=>$TInfo)
			{
				$js_mask.= "  $('".$TInfo['type'].":contains(\"".strtr (html_entity_decode($langs->trans($key), ENT_COMPAT, 'UTF-8'), array("'"=>"\'") )."\")')" ;
				for ($ind=0 ; $ind<$TInfo['nb_parent'] ; $ind++) {
					$js_mask.= ".parent()" ;
				}
				if (isset($TInfo['mask']) && $TInfo['mask']==1) $js_mask.= ".addClass(\"mask_chp\"); // mask chp ".$key."\n" ;
				if (isset($TInfo['remove']) && $TInfo['remove']==1) $js_mask.= ".remove(); // remove chp ".$key."\n" ;
				if (isset($TInfo['red']) && $TInfo['red']==1) $js_mask.= ".addClass(\"tabs_mask_chp_admin\"); // red chp ".$key."\n" ;
			}
			$js_pdp.= $js_mask ;

			// champs require => en gras
			foreach($TChp_require as $key=>$TInfo)
			{
				$js_pdp.= '  $("#'.$key.'")' ;
				for ($ind=0 ; $ind<$TInfo['nb_parent'] ; $ind++) {
					$js_pdp.= ".parent()" ;
				}

				if (isset($TInfo['type']) AND $TInfo['type']=='contains')
				{
					$js_pdp.= '.children("td:contains(\''
						.strtr( html_entity_decode((isset($TInfo['trans']) && $TInfo['trans']!='') ? $langs->trans($TInfo['trans']) : $TInfo['lib'], ENT_COMPAT, 'UTF-8'), $trans_apo )
						.'\')")' ;

				} else // type exacte libelle
				{
					$js_pdp.= '.children().filter(function() {'."\n" // recherche exacte
						//.' alert($(this).text()+\' | '.html_entity_decode( (isset($TInfo['trans']) && $TInfo['trans']!='') ? $langs->trans($TInfo['trans']) : $TInfo['lib'], ENT_COMPAT, 'UTF-8').'\');'."\n"
						.'  if ( $(this).text() == \''
						.strtr( (isset($TInfo['trans']) && $TInfo['trans']!='') ? $langs->trans($TInfo['trans']) : $TInfo['lib'], $trans_apo )
						.'\') return true;'."\n"
						.'  else if ( $(this).text() == \''
						. strtr( html_entity_decode( (isset($TInfo['trans']) && $TInfo['trans']!='') ? $langs->trans($TInfo['trans']) : $TInfo['lib'], ENT_COMPAT, 'UTF-8'), $trans_apo )
						.'\') return true;'."\n"
						.'  else if ( $(this).text() == \''
						. strtr( htmlentities( (isset($TInfo['trans']) && $TInfo['trans']!='') ? $langs->trans($TInfo['trans']) : $TInfo['lib'], ENT_COMPAT, 'UTF-8'), $trans_apo )
						.'\') return true;'."\n"
						.'  else return false;'."\n"
						.' })' ;
				}
				$js_pdp.= '.addClass("fieldrequired");'."\n" ;
			}

			$js_pdp.= '});'."\n" ;
			//$js_pdp.= $js_propo;
			$js_pdp.= "</script>\n";
			$js_pdp.= "<!-- end add js perso -->\n";

			echo $js_pdp;
			if (isset($_REQUEST['DEBUG'])) echo "<br /><br />\r\n" ;
		}
		if ($user->admin==1) echo " ************<br><br><br>\r\n" ;
	}

	/*private function add_filter_tag_affaire(){
		global $conf, $user, $langs, $db;

		$searchCategoryCustomerOperator = 0;
		$searchCategoryCustomerList = GETPOST('search_category_customer_list', 'array');
		if (GETPOSTISSET('formfilteraction')) {
			$searchCategoryCustomerOperator = GETPOST('search_category_customer_operator', 'int');
		} elseif (!empty($conf->global->MAIN_SEARCH_CAT_OR_BY_DEFAULT)) {
			$searchCategoryCustomerOperator = $conf->global->MAIN_SEARCH_CAT_OR_BY_DEFAULT;
		}

		$tmptitle = $langs->transnoentities('CustomersProspectsCategoriesShort');
		$form = new Form($db);
		$categoriesArr = $form->select_all_categories(Categorie::TYPE_CUSTOMER, '', '', 64, 0, 1);
		$formFilter = Form::multiselectarray('search_category_customer_list', $categoriesArr, $searchCategoryCustomerList, 0, 0, 'minwidth300', 0, 0, '', 'category', $tmptitle);
		$pos = strpos($formFilter, '<!-- JS CODE TO ENABLE') ;
		if ($pos!==FALSE) {
			$txt = substr($formFilter, 0, $pos) ;
			$script = substr($formFilter, $pos) ;
		
			$ico = img_picto($tmptitle, 'category', 'class="pictofixedwidth"');
			//$categoriesArr[-2] = '- '.$langs->trans('NotCategorized').' -';
			//$moreforfilter .= $form->textwithpicto('', $langs->trans('UseOrOperatorForCategories') . ' : ' . $tmptitle, 1, 'help', '', 0, 2, 'tooltip_cat_cus'); // Tooltip on click
			$picto = $form->textwithpicto('', $langs->trans('UseOrOperatorForCategories') . ' : ' . $tmptitle, 1, 'help', '', 0, 2, 'tooltip_cat_cus'); // Tooltip on click

			$js_bt = "\n";
			$js_bt.= '<script type="text/javascript">'."\n" ;
			$js_bt.= 'function add_filter_tag_affaire(){'."\n" ;
			$js_bt.= '  var bt_sel = \'\' ;'."\n" ;

			$js_bt.= '  bt_sel+= \'<div class="divsearchfield">\' ;'."\n" ;
			$js_bt.= '  bt_sel+= \''.strtr($ico, array("'"=>" ", "\r"=>'', "\n"=>'')).'\' ;'."\n" ; //
			//$js_bt.= '  bt_sel+= \''.strtr($txt, array("'"=>" ", "\r"=>'', "\n"=>'')).'\' ;\n'."\n" ; //
			$js_bt.= '  bt_sel+= \'<input type="checkbox" class="valignmiddle" id="search_category_customer_operator" name="search_category_customer_operator" value="1"'.($searchCategoryCustomerOperator == 1 ? ' checked="checked"' : '').'/>\' ;'."\n" ;
			$js_bt.= '  bt_sel+= \''.strtr($picto, array("'"=>" ", "\r"=>'', "\n"=>'')).'\' ;\n'."\n" ; //
			$js_bt.= '  bt_sel+= \'</div>\' ;'."\n" ;
			$js_bt.= '  $(".divsearchfield").last().append(bt_sel);'."\n" ;
			//$js_bt.= '  alert( $(".divsearchfield").last().html() );' ;
			$js_bt.= "}\n";
			$js_bt.= '$(document).ready(function () {'."\n" ;
			$js_bt.= '  add_filter_tag_affaire();'."\n" ;
			$js_bt.= '});'."\n" ;
			$js_bt.= "</script>\n";
			echo $js_bt;
			//echo $script;
			echo "<textarea style=\"width:95%;height:150px;\">".$js_bt."</textarea><br />\r\n";
			//echo "<textarea style=\"width:95%;height:150px;\">".strtr($txt, array("'"=>" ", "\r"=>'', "\n"=>''))."</textarea><br />\r\n";
			//echo "<textarea style=\"width:95%;height:150px;\">".$script."</textarea><br />\r\n";
		}
	}*/

	private function add_bouton_select_all_email(){
		//receiver
		$js_bt = "\n";
		$js_bt.= '<script type="text/javascript">'."\n" ;
		$js_bt.= 'function add_bouton_select_all_email(){'."\n" ;
		$js_bt.= '  var bt_sel = \'<input class="button" type="button" value="S&eacute;lectionner tous les contacts" name="allemailselect" id="allemailselect" onclick="select_all_receiver()" />\' ;' ;
		$js_bt.= '  $("#receiver").parent("td").append(bt_sel);'."\n" ;
		//$js_bt.= '  alert( $("#receiver").parent("td").html() );' ;
		$js_bt.= "}\n";
		$js_bt.= 'function select_all_receiver(){'."\n" ;
		//$js_bt.= '  alert("receiver=>");' ;
		//$js_bt.= '  var save_receiver = $("#receiver").parent("td").children("span").children("span").children("span").children("ul.select2-selection__rendered").html() ;' ;
		$js_bt.= '  var save_receiver = \'<li class="select2-search select2-search--inline"><input class="select2-search__field" type="search" tabindex="0" autocomplete="off" autocorrect="off" autocapitalize="none" spellcheck="false" role="searchbox" aria-autocomplete="list" placeholder="" style="width: 0.75em;"></li>\' ;' ;
		$js_bt.= '  var ul_receiver = "" ;' ;
		$js_bt.= '  var ind_id_l = 3000;' ;
		$js_bt.= '  $("#receiver option").each(function() {'."\n" ;
		$js_bt.= '    $( this ).prop("selected", true);' ;
		$js_bt.= '    var name_lien = $( this ).html();' ;
		$js_bt.= '    name_lien = name_lien.replace("<", "&lt;").replace(">", "&gt;");' ;
		$js_bt.= '    ul_receiver+= \'<li class="select2-selection__choice" title="\'+$( this ).html()+\'" data-select2-id="\'+ind_id_l+\'"><span class="select2-selection__choice__remove" role="presentation">×</span>\'+name_lien+\'</li>\' ;' ;
		$js_bt.= '    ind_id_l++ ;' ;
		//$js_bt.= '  alert("receiver:"+$( this ).val()+" | "+$( this ).html()+" | "+$( this ).prop("selected"));' ;
		$js_bt.= '  });' ;
		$js_bt.= '  $("#receiver").parent("td").children("span").children("span").children("span").children("ul.select2-selection__rendered").html(ul_receiver+save_receiver) ;' ;
		//$js_bt.= '  alert( ul_receiver+save_receiver );' ;
		$js_bt.= "}\n";
		$js_bt.= '$(document).ready(function () {'."\n" ;
		$js_bt.= '  add_bouton_select_all_email();' ;
		//$js_bt.= '  select_all_receiver();' ;
		//$js_bt.= '  alert("select_all_receiver");' ;
		$js_bt.= '});' ;
		$js_bt.= "</script>\n";

		echo $js_bt;
	}

	private function _str_cut($s,$len = 60){
		if (strlen($s) > $len+3){
			$r = substr($s,0,$len);
			$pos = strrpos($r, " ");
			if ($pos!==FALSE) $r = substr($r,0,$pos);

			$last_car = substr($r,-1);
			if ($last_car==".") { $r = substr($r,0,-1)."..."; }
			else{ $r = $r."..."; }
		}
		else{
			$r=$s;
		}

		return $r;
	}
	/* Add here any other hooked methods... */

	private function set_echancier_all_extra($id_affaire, $echeancier_projet) {
		global $conf, $user, $langs;

		$sql_up = "UPDATE ".MAIN_DB_PREFIX."projet_extrafields SET echeancier='".addslashes($echeancier_projet)."' WHERE fk_object=".$id_affaire ;
		$result = $this->db->query($sql_up);
		//echo "sql propal: ".$sql_up." => ".$result."<br>" ;

		$sql = "SELECT rowid FROM ".MAIN_DB_PREFIX."propal WHERE fk_projet=".$id_affaire ;
		$result = $this->db->query($sql);
		if ($result)
		{
			$TList_id_propal = array() ;
			while ($obj_prod = $this->db->fetch_object($result)) {
				$TList_id_propal[] = $obj_prod->rowid ;
				//0: Brouillon | 1: Validé (ouvert) | 2: Signée | 3: Non Signée | 4: Facturée (fermé)
			}
			//echo "<pre>".print_r($TList_id_propal, TRUE)."</pre><br />\n" ;
			foreach ($TList_id_propal as $id_propal)
			{
				$sql_up = "UPDATE ".MAIN_DB_PREFIX."propal_extrafields SET echeancier='".addslashes($echeancier_projet)."' WHERE fk_object=".$id_propal ;
				$result = $this->db->query($sql_up);
				//echo "sql propal: ".$sql_up." => ".$result."<br>" ;
			}
			//echo "<hr />\n" ;
		}

		$sql = "SELECT rowid FROM ".MAIN_DB_PREFIX."facture WHERE fk_projet=".$id_affaire ;
		$result = $this->db->query($sql);
		if ($result)
		{
			$TList_id_fact = array() ;
			while ($obj_fact = $this->db->fetch_object($result)) {
				$TList_id_fact[] = $obj_fact->rowid ;
			}
			//echo "<pre>".print_r($TList_id_fact, TRUE)."</pre><br />\n" ;
			foreach ($TList_id_fact as $id_fact)
			{
				$sql_up = "UPDATE ".MAIN_DB_PREFIX."facture_extrafields SET echeancier='".addslashes($echeancier_projet)."' WHERE fk_object=".$id_fact ;
				$result = $this->db->query($sql_up);
				//echo "sql propal: ".$sql_up." => ".$result."<br>" ;
			}
			//echo "<hr />\n" ;
		}

		//exit() ;
	}

	private function get_list_pdf_propal(&$object) {
		global $langs, $conf, $user;

		$link_pdf = '' ;

		$documenturl = DOL_URL_ROOT.'/document.php';
		$filedir = $conf->propal->multidir_output[$object->entity]."/".dol_sanitizeFileName($object->ref);
		$file_list = null;
		$modulepart = 'propal' ;
		$param = 'entity='.(!empty($object->entity) ? $object->entity : $conf->entity);

		if (!empty($filedir))
		{
			$file_list = dol_dir_list($filedir, 'files', 0, '', '(\.meta|_preview.*.*\.png)$', 'date', SORT_DESC);
		}

		if (is_array($file_list))
		{
			foreach ($file_list as $file)
			{
				$file_name = $file["name"] ;
				$relativepath = $file["name"]; // Cas general
				if ($object->ref) $relativepath = $object->ref."/".$file["name"]; // Cas propal, facture...
				//if ($modulepart == 'export') $relativepath = $file["name"]; // Other case

				$url_pdf_propal = $documenturl.'?modulepart='.$modulepart.'&amp;attachment=0&amp;file='.urlencode($relativepath).($param ? '&'.$param : '') ;
				if ($link_pdf!='') $link_pdf.= ' | ' ;

				$link_pdf.= '<a class=\"documentdownload paddingright\" href=\"'.$url_pdf_propal.'\" target=\"_blank\"><i class=\"fa fa-file-pdf-o paddingright\" title=\"Fichier: '.$file_name.'\"></i>'.$file_name.'</a>' ;
				$link_pdf.= ' <a class=\"pictopreview documentpreview\" href=\"'.$url_pdf_propal.'\" mime=\"application/pdf\" target=\"_blank\"><span class=\"fa fa-search-plus\" style=\"color: gray\"></span></a>' ;
				//$link_pdf.= '<a href=\"/comm/propal/card.php?id='.$id_prop.'\" title=\"'.$title.'\" class=\"classfortooltip\">'.$object->ref.'</a>"' ;
			}
		}

		return $link_pdf ;
	}
}
