<?php
/* Copyright (C) 2021 Administrateur
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file    core/triggers/interface_99_modBatibarrmod_BatibarrmodTriggers.class.php
 * \ingroup batibarrmod
 * \brief   Example trigger.
 *
 * Put detailed description here.
 *
 * \remarks You can create other triggers by copying this one.
 * - File name should be either:
 *      - interface_99_modBatibarrmod_MyTrigger.class.php
 *      - interface_99_all_MyTrigger.class.php
 * - The file must stay in core/triggers
 * - The class name must be InterfaceMytrigger
 * - The constructor method must be named InterfaceMytrigger
 * - The name property name must be MyTrigger
 */

require_once DOL_DOCUMENT_ROOT.'/core/triggers/dolibarrtriggers.class.php';


/**
 *  Class of triggers for Batibarrmod module
 */
class InterfaceBatibarrmodTriggers extends DolibarrTriggers
{
	/**
	 * @var DoliDB Database handler
	 */
	protected $db;

	/**
	 * Constructor
	 *
	 * @param DoliDB $db Database handler
	 */
	public function __construct($db)
	{
		$this->db = $db;

		$this->name = preg_replace('/^Interface/i', '', get_class($this));
		$this->family = "demo";
		$this->description = "Batibarrmod triggers.";
		// 'development', 'experimental', 'dolibarr' or version
		$this->version = 'development';
		$this->picto = 'batibarrmod@batibarrmod';
	}

	/**
	 * Trigger name
	 *
	 * @return string Name of trigger file
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Trigger description
	 *
	 * @return string Description of trigger file
	 */
	public function getDesc()
	{
		return $this->description;
	}


	/**
	 * @param $object Object
	 * @param $langs Translate
	 * @return int
	 */
	private function fieldMandatoryProject($object, $langs)
	{
		$error=0;

		if (empty($object->opp_status)) {
			$this->errors[] = $langs->trans("ErrorFieldRequired", $langs->transnoentities("OpportunityStatus"));
			$error++;
		}

		if (empty($object->opp_amount)) {
			$this->errors[] = $langs->trans("ErrorFieldRequired", $langs->transnoentities("OpportunityAmount"));
			$error++;
		}

		if (empty($object->date_start)) {
			$this->errors[] = $langs->trans("ErrorFieldRequired", $langs->transnoentities("DateStart")).'|'.$object->projectstart.'|';
			$error++;
		}

		if (empty($object->date_end)) {
			$this->errors[] = $langs->trans("ErrorFieldRequired", $langs->transnoentities("DateEnd"));
			$error++;
		}

		if ($object->budget_amount==='') {
			$this->errors[] = $langs->trans("ErrorFieldRequired", $langs->transnoentities("Budget"));
			$error++;
		}

		if (empty($object->array_options['options_ba_support'])) {
			$this->errors[] = $langs->trans("ErrorFieldRequired", 'Support');
			$error++;
		}

		if (empty($object->array_options['options_demand_reason'])) {
			$this->errors[] = $langs->trans("ErrorFieldRequired", 'Origine');
			$error++;
		}

		if (empty($object->array_options['options_tacite_reconduction'])) {
			$this->errors[] = $langs->trans("ErrorFieldRequired", 'Tacite reconduction');
			$error++;
		}
		$_REQUEST['error'] = $error ;

		if (!empty($error)) {
			return -1;
		}
		return 0;
	}

	/**
	 * @param $object Object
	 * @param $langs Translate
	 * @return int
	 */
	private function fieldMandatoryContact($object, $langs)
	{
		$error=0;

		if (empty($object->civility_code)) {
			$this->errors[] = $langs->trans("ErrorFieldRequired", $langs->transnoentities("UserTitle"));
			$error++;
		}

		if (empty($object->array_options['options_ba_fonction'])) {
			$this->errors[] = $langs->trans("ErrorFieldRequired", 'Fonction');
			$error++;
		}
		$_REQUEST['error'] = $error ;

		if (!empty($error)) {
			return -1;
		}
		return 0;
	}

	/**
	 * @param $object Object
	 * @param $langs Translate
	 * @return int
	 */
	private function fieldMandatoryCompany($object, $langs)
	{
		$error=0;

		if (empty($object->zip)) {
			$this->errors[] = $langs->trans("ErrorFieldRequired", $langs->transnoentities("Zip"));
			$error++;
		}

		if (empty($object->town)) {
			$this->errors[] = $langs->trans("ErrorFieldRequired", $langs->transnoentities("Town"));
			$error++;
		}

		if (empty($object->country_id)) {
			$this->errors[] = $langs->trans("ErrorFieldRequired", $langs->transnoentities("Country"));
			$error++;
		}

		if (empty($object->typent_id)) {
			$this->errors[] = $langs->trans("ErrorFieldRequired", $langs->transnoentities("ThirdPartyType"));
			$error++;
		}
		$_REQUEST['error'] = $error ;

		if (!empty($error)) {
			return -1;
		}
		return 0;
	}

	/*****************************/
	/********** Tickets **********/

	public function ticket_send_email_create($object, $user) {
		global $conf, $langs;

		//echo "TRIGGER TICKET_CREATE ticket_send_email_create<br>\r\n" ;
		//echo "<pre>".print_r($object, TRUE)."</pre><br />\r\n" ; echo "==========================<br />\r\n" ;

		/* Send email to admin */
		$TSendto = $this->get_list_send_to($object, FALSE, FALSE, FALSE, TRUE);
		if (!in_array($user->email, $TSendto)) $TSendto['createur'] = $user->email; // createur

		// cf htdocs\core\triggers\interface_50_modTicket_TicketEmail.class.php pour email envoye a Mail module

		if (count($TSendto) > 0)
		{
			if ($object->fk_soc > 0) {
				$object->fetch_thirdparty();
			}

			$subject = $this->ticket_subject($object, $langs->transnoentities('TicketNewEmailSubject_ba', $object->ref, $object->track_id));
			
			$infos_ticket = $this->ticket_infos_body($object, "Nouvelle demande") ;

			$message = "\r\n";
			$message.= $langs->transnoentities('TicketNewEmailBody_ba', $object->track_id)."\r\n"; //<p>Le ticket vient d'être créé ...</p>

			$message.= "<b>Auteur :</b> ".$user->firstname." ".$user->lastname ;
			$message.= "<br /><br />\r\n";

			$message.= $infos_ticket;

			$msg_data = $object->message;
			if (!dol_textishtml($msg_data)) {
				$msg_data = dol_nl2br($msg_data);
			}
			$message .= '<p>'.$langs->trans('Message')." :<br />\r\n".$msg_data."</p>\r\n";
			
			$this->ticket_send_email($subject, $message, $TSendto) ;
		}

		//echo "TRIGGER TICKET_CREATE ticket_send_email_create - END<br>\r\n" ;
		//exit() ;
	}

	public function ticket_send_email_msg($obj_r, $user) {
		global $conf, $langs;
		if (isset($obj_r->fk_element) AND $obj_r->fk_element > 0) // param objet message
		{
			require_once DOL_DOCUMENT_ROOT.'/ticket/class/ticket.class.php';
			require_once DOL_DOCUMENT_ROOT.'/user/class/user.class.php';

			//echo "TRIGGER ACTION_CREATE btn_add_message<br>\r\n" ;
			//echo "<pre>".print_r($object, TRUE)."</pre><br />\r\n" ; echo "==========================<br />\r\n" ;
			//echo "<pre>".print_r($_REQUEST, TRUE)."</pre><br />\r\n" ; echo "==========================<br />\r\n" ;

			$object = new Ticket($this->db);
			$res = $object->fetch($obj_r->fk_element);

			/* Send email to creater, assigned, contact */
			$TSendto = $this->get_list_send_to($object);

			if (count($TSendto) > 0)
			{
				if ($object->fk_soc > 0) {
					$object->fetch_thirdparty();
				}

				$subject = $this->ticket_subject($object, $langs->transnoentities('TicketNewMessageSubject_ba', $object->ref));

				$infos_ticket = $this->ticket_infos_body($object) ;

				$messagePost = GETPOST('message', 'restricthtml');
				if (!dol_textishtml($messagePost)) {
					$messagePost = dol_nl2br($messagePost);
				}

				$message = '';
				$message.= '<p>'.$langs->trans('TicketNewMessageEmailBody_ba')."<br />\r\n";
				$message.= $messagePost;
				$message.= "</p>\r\n";
				//$message.= "<br /><br />\r\n";
				//$message.= "Message : ".$messagePost;
				$message.= $infos_ticket;

				$this->ticket_send_email($subject, $message, $TSendto) ;
			}
			//exit() ;
		}

		//echo "TRIGGER ACTION_CREATE btn_add_message - END<br>\r\n" ; exit() ;
	}

	public function ticket_send_email_assigne($object, $user) {
		global $conf, $langs;
		/*
		TicketAssignedEmailBody_ba 	Ceci est un message automatique pour vous informer que le ticket %s vient d'être assigné
		TicketAssignedEmailSubject_ba 	Ticket %s assigné
		*/
		//echo "TRIGGER BATIBARR TICKET_ASSIGNED | obj assigned: ".$object->fk_user_assign." | param: ".$_REQUEST['fk_user_assign']."<br>\r\n" ;
		
		require_once DOL_DOCUMENT_ROOT.'/user/class/user.class.php';

		/* Send email to creater, admin */
		$TSendto = $this->get_list_send_to($object, TRUE, FALSE, FALSE, TRUE);

		//echo "<pre>".print_r($_REQUEST, TRUE)."</pre><br />\r\n" ; echo "==========================<br />\r\n" ;
		if ($object->fk_user_assign > 0)
		{
			$user_assign = new User($this->db);
			$user_assign->fetch($object->fk_user_assign);
		}

		if (count($TSendto) > 0)
		{
			if ($object->fk_soc > 0) {
				$object->fetch_thirdparty();
			}

			$subject = $this->ticket_subject($object, $langs->transnoentities('TicketAssignedEmailSubject_ba', $object->ref));

			$infos_ticket = $this->ticket_infos_body($object) ;

			$message = '';
			$message.= $langs->transnoentities('TicketAssignedEmailBody_ba', $object->track_id)."<br /><br />\r\n";
			if ($object->fk_user_assign > 0)
			{
				$message.= $langs->trans('TicketAssignedEmailTo_ba', $user_assign->firstname." ".$user_assign->lastname, $user_assign->email);
			}
			else 
			{
				$message.= '!!! none !!!';
			}
			$message.= "<br /><br />\r\n";
			$message.= $infos_ticket;

			$this->ticket_send_email($subject, $message, $TSendto) ;

			//echo "Statut: ".$object->LibStatut($object->fk_statut)."<br>\r\n" ;
			//echo "TRIGGER BATIBARR TICKET_STATUT_CHANGE - END<br>\r\n" ; exit() ;
		}
	}

	public function ticket_send_email_change($object, $user) {
		global $conf, $langs;
		/*
		TicketChangeEmailBody_ba 	Ceci est un message automatique pour vous informer que le ticket %s vient de changer de statut
		TicketChangeEmailSubject_ba 	Ticket %s mise à jour du statut
		*/
		
		$new_status_modify = GETPOST('new_status', 'string') ;
		//echo "TRIGGER BATIBARR TICKET_STATUT_CHANGE | ".$object->LibStatut($new_status_modify)." (".$new_status_modify.")<br>\r\n" ;
		
		require_once DOL_DOCUMENT_ROOT.'/user/class/user.class.php';

		/* Send email to creater, assigned, contact, admin */
		$TSendto = $this->get_list_send_to($object);

		if (count($TSendto) > 0)
		{
			if ($object->fk_soc > 0) {
				$object->fetch_thirdparty();
			}

			$subject = $this->ticket_subject($object, $langs->transnoentities('TicketChangeEmailSubject_ba', $object->ref));

			$infos_ticket = $this->ticket_infos_body($object, $object->LibStatut($new_status_modify)) ;

			$message = '';
			$message.= $langs->transnoentities('TicketChangeEmailBody_ba', $object->track_id)."<br /><br />\r\n";
			$message.= $infos_ticket;

			$this->ticket_send_email($subject, $message, $TSendto) ;

			//echo "Statut: ".$object->LibStatut($object->fk_statut)."<br>\r\n" ;
			//echo "TRIGGER BATIBARR TICKET_STATUT_CHANGE - END<br>\r\n" ; exit() ;
		}
	}

	public function ticket_send_email_modify($object, $user) {
		global $conf, $langs;
		/*
		TicketChangeEmailBody_ba 	Ceci est un message automatique pour vous informer que le ticket %s vient de changer de statut
		TicketChangeEmailSubject_ba 	Ticket %s mise à jour du statut
		*/

		$action_modify = GETPOST('action', 'string') ;
		//echo "TRIGGER BATIBARR TICKET_STATUT_CHANGE | ".$action_modify."<br>\r\n" ;

		if ($action_modify=='mark_ticket_read')
		{
			require_once DOL_DOCUMENT_ROOT.'/user/class/user.class.php';

			/* Send email to creater, assigned, contact, admin */
			$TSendto = $this->get_list_send_to($object);

			if (count($TSendto) > 0)
			{
				if ($object->fk_soc > 0) {
					$object->fetch_thirdparty();
				}

				$subject = $this->ticket_subject($object, $langs->transnoentities('TicketChangeEmailSubject_ba', $object->ref));

				$infos_ticket = $this->ticket_infos_body($object, $object->LibStatut(1)) ;

				$message = '';
				$message.= $langs->transnoentities('TicketChangeEmailBody_ba', $object->track_id)."<br /><br />\r\n";
				$message.= $infos_ticket;

				$this->ticket_send_email($subject, $message, $TSendto) ;

				//echo "Statut: ".$object->LibStatut($object->fk_statut)."<br>\r\n" ;
				//echo "TRIGGER BATIBARR TICKET_STATUT_CHANGE - END<br>\r\n" ; exit() ;
			}
		}
		//exit('TICKET_MODIFY') ;
	}

	public function ticket_send_email_close($object, $user) {
		global $conf, $langs;
		/*
		TicketChangeEmailBody_ba 	Ceci est un message automatique pour vous informer que le ticket %s vient de changer de statut
		TicketChangeEmailSubject_ba 	Ticket %s mise à jour du statut
		*/
		//echo "TRIGGER BATIBARR TICKET_CLOSE<br>\r\n" ;
		
		require_once DOL_DOCUMENT_ROOT.'/user/class/user.class.php';

		/* Send email to creater, assigned, contact, admin */
		$TSendto = $this->get_list_send_to($object);

		if (count($TSendto) > 0)
		{
			if ($object->fk_soc > 0) {
				$object->fetch_thirdparty();
			}
			
			// Send email to customer
			$subject = $this->ticket_subject($object, $langs->transnoentities('TicketCloseEmailSubject_ba', $object->ref));

			$infos_ticket = $this->ticket_infos_body($object, $object->LibStatut(8)) ; //STATUS_CLOSED

			$message = '';
			$message.= $langs->transnoentities('TicketCloseEmailBody_ba', $object->track_id)."<br /><br />\r\n";
			$message.= $infos_ticket;

			$this->ticket_send_email($subject, $message, $TSendto) ;

			//echo "Statut: ".$object->LibStatut($object->fk_statut)."<br>\r\n" ;
			//echo "TRIGGER BATIBARR TICKET_CLOSE - END<br>\r\n" ; exit() ;
		}
	}

	private function get_list_send_to(&$object, $with_create=TRUE, $with_assigned=TRUE, $with_contact=TRUE, $with_notif=FALSE) {
		global $conf, $langs;
		$TSendto = array();
		if ($with_notif && !empty($conf->global->TICKET_NOTIFICATION_EMAIL_TO)) $TSendto['TICKET_NOTIFICATION_EMAIL_TO'] = $conf->global->TICKET_NOTIFICATION_EMAIL_TO;

		if ($with_create)
		{
			$user_create = new User($this->db);
			$user_create->fetch($object->fk_user_create);
			if ($user_create->email!='' && !in_array($user_create->email, $TSendto)) $TSendto['user_create'] = $user_create->email;
		}

		if ($with_assigned && $object->fk_user_assign > 0)
		{
			$user_assign = new User($this->db);
			$user_assign->fetch($object->fk_user_assign);
			if ($user_assign->email!='' && !in_array($user_assign->email, $TSendto)) $TSendto['user_assign'] = $user_assign->email;
		}

		if ($with_contact)
		{
			$sql_contact = "SELECT sc.firstname as firstname, sc.lastname as lastname, sc.email as email, ec.fk_socpeople as id_contact" ;
			$sql_contact.= " FROM ".MAIN_DB_PREFIX."user sc JOIN ".MAIN_DB_PREFIX."element_contact ec ON sc.rowid = ec.fk_socpeople" ;
			$sql_contact.= " WHERE ec.fk_c_type_contact='155' AND ec.element_id=".$object->id ; // 155 => Utilisateur assigné | 156 => Contributeur
			$sql_contact.= " ORDER BY ec.fk_c_type_contact ASC, sc.lastname ASC, sc.firstname ASC";

			//echo "CONTACT de ".$object->id."<br>\r\n" ;
			//echo "<pre>".print_r($object, TRUE)."</pre><br />\r\n" ; echo "==========================<br />\r\n" ;

			$resql = $this->db->query($sql_contact);
			if ($resql)
			{
				$nb_lie = 0 ;
				while ($obj_con = $this->db->fetch_object($resql)) {
					$contact_lie = $obj_con->firstname.(($obj_con->firstname!='' && $obj_con->lastname!='') ? ' ' : '').$obj_con->lastname ;
					if ($contact_lie!='') $contact_lie.= ' <'.$obj_con->email.'>' ;
					else $contact_lie = $obj_con->email ;
					
					if ($obj_con->email!='' && !in_array($obj_con->email, $TSendto) && !in_array($contact_lie, $TSendto))
					{
						$TSendto['contact_lie_'.$nb_lie] = $contact_lie ;
						$nb_lie++ ;
					}
				}
			}
		}
		//else echo "NO CONTACT<br>\r\n" ;

		if (!in_array('Robin Yann <yann.robin@batiactugroupe.com>', $TSendto)) $TSendto['force_ry'] = 'Robin Yann <yann.robin@batiactugroupe.com>' ;
		
		return $TSendto ;
	}

	private function ticket_subject(&$object, $v_action) {
		$subject = '';
		$subject.= '[TICKET] ';

		if ($object->fk_soc > 0) $subject.= $object->thirdparty->name.' - ';
		if ($object->type_label!='') $subject.= $object->type_label.' - ';
		if ($object->severity_label!='') $subject.= $object->severity_label.' - ';

		$subject.= $v_action;
		
		return $subject;
	}
	
	private function ticket_infos_body(&$object, $new_statut='') {
		global $conf, $langs;

		$infos_ticket = '' ;
		$infos_ticket.= "<ul>\r\n";
		$infos_ticket.= '<li>'.$langs->trans('Reference').' : '.$object->ref."</li>\r\n";
		$infos_ticket.= '<li>'.$langs->trans('Title').' : '.$object->subject."</li>\r\n";
		$infos_ticket.= '<li>'.$langs->trans('Type').' : '.$object->type_label."</li>\r\n";
		$infos_ticket.= '<li>'.$langs->trans('Category').' : '.$object->category_label."</li>\r\n";
		$infos_ticket.= '<li>'.$langs->trans('Severity').' : '.$object->severity_label."</li>\r\n";
		//$infos_ticket.= '<li>'.$langs->trans('From').' : '.$object->origin_email."</li>\r\n";

		$infos_ticket.= '<li>'.$langs->trans("Status").": ";
		if ($object->fk_statut!='') $infos_ticket.= $object->LibStatut($object->fk_statut);
		if ($object->fk_statut!='' && $new_statut!='') $infos_ticket.= ' => ' ;
		if ($new_statut!='') $infos_ticket.= '<b>'.$new_statut.'</b>';
		$infos_ticket.= "</li>\r\n";

		// Extrafields
		/*require_once DOL_DOCUMENT_ROOT.'/core/class/extrafields.class.php';
		$extraFields = new ExtraFields($this->db);
		$extraFields->fetch_name_optionals_label($object->table_element);
		if (is_array($object->array_options) && count($object->array_options) > 0) {
			foreach ($object->array_options as $key => $value) {
				$key = substr($key, 8); // remove "options_"
				$infos_ticket .= '<li>'.$langs->trans($extraFields->attributes[$object->element]['label'][$key]).' : '.$extraFields->showOutputField($key, $value).'</li>';
			}
		}*/

		if ($object->fk_soc > 0) {
			$infos_ticket.= '<li><a href="'.dol_buildpath('/societe/card.php', 2).'?mainmenu=companies&socid='.$object->fk_soc.'">'.$langs->trans('ThirdParty').' : '.$object->thirdparty->name."</a></li>\r\n";
		}

		$infos_ticket.= "</ul>\r\n";

		if ($object->track_id!='')
		{
			//SeeThisTicketIntomanagementInterface 	Voir ticket dans l'interface de gestion
			//$url_internal_ticket = dol_buildpath('/ticket/card.php', 2).'?track_id='.$object->track_id;
			//$url_internal_ticket_id = dol_buildpath('/ticket/card.php', 2).'?id='.$object->id;
			//$url_public_ticket = ($conf->global->TICKET_URL_PUBLIC_INTERFACE ? $conf->global->TICKET_URL_PUBLIC_INTERFACE.'/view.php' : dol_buildpath('/public/ticket/view.php', 2)).'?track_id='.$object->track_id;

			$infos_ticket.= '<p><a href="'.dol_buildpath('/ticket/card.php', 2).'?mainmenu=ticket&track_id='.$object->track_id.'">'.$langs->trans('SeeThisTicketIntomanagementInterface')."</a></p>\r\n";
		}

		return $infos_ticket ;
	}

	private function ticket_send_email($subject, $message, $TSendto, $sendtocc='') {
		global $conf, $langs;

		// add auto signature
		$message.= $conf->global->TICKET_MESSAGE_MAIL_SIGNATURE ? $conf->global->TICKET_MESSAGE_MAIL_SIGNATURE : $langs->transnoentities('TicketMessageMailSignatureText');

		$from = $conf->global->MAIN_INFO_SOCIETE_NOM.'<'.$conf->global->TICKET_NOTIFICATION_EMAIL_FROM.'>';
		$replyto = $from;
		
		/*$message.= "<hr />\r\n" ;
		foreach ($TSendto as $k=>$em) {
			$message.= "<br />\r\n".$k." => ".$em ;
		}
		$message.= "<hr />\r\n" ;*/

		if ($_SERVER['SERVER_NAME']=='dev.batibarr.batiactugroupe.com' 
			|| $_SERVER['SERVER_NAME']=='dev.batibarr-16.batiactugroupe.com'
			//|| $_SERVER['SERVER_NAME']=='batibarr.batiactu.space'
		) {
			//echo "Sujet: ".$subject."<br />\r\n" ;
			//echo "Send to: <pre>".print_r($TSendto, TRUE)."</pre><br>\r\n" ;
			//echo "Send to:<br>\r\n" ;
			//foreach ($TSendto as $emailto) echo htmlentities($emailto)."<br>\r\n" ; echo "<hr>\r\n" ;
			$TSendto = array('Robin Yann <yann.robin@batiactugroupe.com>') ;
			//echo "Send to force: <pre>".print_r($TSendto, TRUE)."</pre><br>\r\n" ;
			//echo "From: ".$conf->global->MAIN_INFO_SOCIETE_NOM.' - '.$conf->global->TICKET_NOTIFICATION_EMAIL_FROM."<br>\r\n" ;
			//exit() ;
		}
		
		//if ($object->fk_user_assign > 0) echo "fk_user_assign:<pre>".print_r($user_assign, TRUE)."</pre><br>\r\n" ;
		
		//echo "user:<pre>".print_r($user, TRUE)."</pre><br>\r\n" ;
		//echo "object:<pre>".print_r($object, TRUE)."</pre><br>\r\n" ;

		if (!empty($conf->global->TICKET_DISABLE_MAIL_AUTOCOPY_TO)) {
			$old_MAIN_MAIL_AUTOCOPY_TO = $conf->global->MAIN_MAIL_AUTOCOPY_TO;
			$conf->global->MAIN_MAIL_AUTOCOPY_TO = '';
		}

		// Init to avoid errors
		$filepath = array();
		$filename = array();
		$mimetype = array();

		include_once DOL_DOCUMENT_ROOT.'/core/class/CMailFile.class.php';
		foreach ($TSendto as $email_to)
		{
			include_once DOL_DOCUMENT_ROOT.'/core/class/html.formmail.class.php';
			include_once DOL_DOCUMENT_ROOT.'/core/lib/files.lib.php';
			require_once DOL_DOCUMENT_ROOT.'/core/class/extrafields.class.php';
			$mailfile = new CMailFile($subject, $email_to, $from, $message, $filepath, $mimetype, $filename, $sendtocc, '', 0, -1, '', '', $trackid, '', 'ticket');
			if ($mailfile->error || $mailfile->errors) {
				setEventMessages($mailfile->error, $mailfile->errors, 'errors');
				//echo "ERREUR SEND: ".$mailfile->error." | ".$mailfile->errors."<br>\r\n" ;
			} else {
				$result = $mailfile->sendfile();
				//echo "OK SEND: ".$email_to."<br>\r\n" ;
			}
			if (!empty($conf->global->TICKET_DISABLE_MAIL_AUTOCOPY_TO)) {
				$conf->global->MAIN_MAIL_AUTOCOPY_TO = $old_MAIN_MAIL_AUTOCOPY_TO;
			}
		}
	}

	/*****************************/
	
	/**
	 * @param $propal Object
	 * @param $myproject Object
	 * @param $user Object
	 * @return int
	 */
	public function create_auto_tacite_recond($propal, $myproject, $user) {
		require_once DOL_DOCUMENT_ROOT.'/projet/class/project.class.php';
	
		$clone_contacts = 1;
		$clone_tasks = 0;
		$clone_project_files = 0;
		$clone_task_files = 0;
		$clone_notes = 0;
		$move_date = 0;
		$clone_thirdparty = 0;

		$result = $myproject->createFromClone($user, $myproject->id, $clone_contacts, $clone_tasks, $clone_project_files, $clone_task_files, $clone_notes, $move_date, 0, $clone_thirdparty);
		file_put_contents($conf->mycompany->dir_output.'/log_tacite_'.date('Ymd').'.txt', date('Y-m-d H:i:s')." * create_auto_tacite_recond | id source: ".$myproject->id." | result: ".$result."\r\n",  FILE_APPEND) ;
		if ($result <= 0)
		{
			setEventMessages($myproject->error, $myproject->errors, 'errors');
			return 0 ;
		} else {
			// Load new object
			$newproject = new Project($this->db);
			$newproject->fetch($result);
			$newproject->fetch_optionals();
			$newproject->fetch_thirdparty(); // Load new object
			
			$newproject->ref = substr($newproject->ref,0,2).date('ym', $newproject->array_options['options_date_fin_contrat']).substr($newproject->ref,6) ;
			$newproject->title = 'Tacite - '.$myproject->title ;
			$newproject->statut = 1; //Project::STATUS_VALIDATED
			$newproject->opp_status = 5; // tacite
			$newproject->opp_percent = '75.00'; // tacite
			$newproject->budget_amount = round($newproject->array_options['options_amount_real'], 2);
			$newproject->opp_amount = round($newproject->array_options['options_amount_real'] * 1.03, 2); //+3%
			$time = strtotime( date('Y-m-d', $newproject->array_options['options_date_fin_contrat']).' -1 month') ;
			$newproject->date_start = $time;
			//$newproject->date_start = '';
			$newproject->date_end = $newproject->array_options['options_date_fin_contrat'];
			
			//$TDate_f = explode('/', $newproject->array_options['options_date_fin_contrat']) ;
			//$newproject->date_start = dol_mktime(0, 0, 0, (int)$TDate_f[1], (int)$TDate_f[0], $TDate_f[2]) ;
			//echo "ref: ".$newproject->ref." | ".date('d/m/Y', $newproject->date_start)."<br>" ;
			//exit() ;

			$newproject->array_options['options_tacite_reconduction'] = 3; // Oui renouvellement
			$newproject->array_options['options_demand_reason'] = 5; // Origine sur renouvellement
			$newproject->array_options['options_signe_auto'] = 0;
			$newproject->array_options['options_amount_real'] = '';
			$newproject->array_options['options_date_signe'] = '';
			$newproject->array_options['options_ca_annee'] = '';
			$newproject->array_options['options_particularite_facture'] = '';
			//$newproject->array_options['options_duree_contrat'] = '';
			$newproject->array_options['options_date_fin_contrat'] = '';

			//echo "id: ".$newproject->id ;exit();
			$result = $newproject->update($user);
			//file_put_contents(DOL_DOCUMENT_ROOT.'/log_'.date('Ymd').'.txt', date('Y-m-d H:i:s')." * create_auto_tacite_recond | id recond: ".$newproject->id." | statut: ".$newproject->statut." | opp_status: ".$newproject->opp_status."\r\n",  FILE_APPEND) ;

			return $newproject->id ;
		}
	}

	public function close_auto_project($object, $user, $statut) {
		//global $conf, $langs;
		$error=0;

		require_once DOL_DOCUMENT_ROOT.'/projet/class/project.class.php';
		//file_put_contents($conf->mycompany->dir_output.'/log_'.date('Ymd').'.txt', date('Y-m-d H:i:s').' * close_auto_project | id: '.$object->id.' | id projet '.$object->fk_project."\r\n",  FILE_APPEND) ;
		$myproject = new Project($this->db);
		$result = $myproject->fetch($object->fk_project);
		if ($result < 0) {
			//file_put_contents($conf->mycompany->dir_output.'/log_'.date('Ymd').'.txt', date('Y-m-d H:i:s')." * close_auto_project | Error load\r\n",  FILE_APPEND) ;
			$this->errors[] = $myproject->error;
			$error++;
		}
		if (empty($error)) {
			//file_put_contents($conf->mycompany->dir_output.'/log_'.date('Ymd').'.txt', date('Y-m-d H:i:s')." * close_auto_project | load | statut: ".$statut."\r\n",  FILE_APPEND) ;
			$myproject->opp_status = $statut;
			if ($statut==6)
			{
				$myproject->opp_percent ='100.00' ;
				$myproject->array_options['options_signe_auto'] = $object->id ; // $object->id
				//$myproject->array_options['options_date_signe'] = GETPOST('date_signe', 'string') ;
				$myproject->array_options['options_date_signe'] = dol_mktime(0, 0, 0, GETPOST('date_signemonth', 'int'), GETPOST('date_signeday', 'int'), GETPOST('date_signeyear', 'int'));
				$myproject->array_options['options_type_contrat'] = GETPOST('type_contrat', 'string') ;
				$myproject->array_options['options_echeancier'] = GETPOST('echeancier', 'string') ;
				$myproject->array_options['options_ca_annee'] = GETPOST('ca_annee', 'string') ;
				$myproject->array_options['options_particularite_facture'] = GETPOST('particularite_facture', 'string') ;
				$myproject->array_options['options_duree_contrat'] = GETPOST('duree_contrat', 'string') ;
				//$myproject->array_options['options_date_fin_contrat'] = GETPOST('date_fin_contrat', 'string') ;
				$myproject->array_options['options_date_fin_contrat'] = dol_mktime(0, 0, 0, GETPOST('date_fin_contratmonth', 'int'), GETPOST('date_fin_contratday', 'int'), GETPOST('date_fin_contratyear', 'int'));
				$myproject->array_options['options_amount_real'] = $object->total_ht;
				$myproject->array_options['options_motif_perdu'] = null ;

				if ($myproject->array_options['options_tacite_reconduction'] == 1) // on modifie l'etat tacite seulement si c sur Non
				{
					if (GETPOST('signe_tacite_reconduction', 'string') =='on')
					{
						$myproject->array_options['options_tacite_reconduction'] = 2; // Oui nouvelle
					}
					else 
					{
						$myproject->array_options['options_tacite_reconduction'] = 1; // Non
					}
				}
			}
			else 
			{
				$myproject->opp_percent ='0.00';
				$myproject->array_options['options_signe_auto'] = null ;
				$myproject->array_options['options_date_signe'] = null;
				$myproject->array_options['options_type_contrat'] = 0;
				$myproject->array_options['options_echeancier'] = null;
				$myproject->array_options['options_ca_annee'] = null ;
				$myproject->array_options['options_particularite_facture'] = null ;
				$myproject->array_options['options_duree_contrat'] = null ;
				$myproject->array_options['options_date_fin_contrat'] = null ;
				$myproject->array_options['options_amount_real'] = null;
				$myproject->array_options['options_motif_perdu'] = GETPOST('motif_perdu', 'string') ;
			}

			$myproject->statut=Project::STATUS_CLOSED;
			//file_put_contents($conf->mycompany->dir_output.'/log_'.date('Ymd').'.txt', date('Y-m-d H:i:s')." * close_auto_project | date_end: ".date('Y-m-d H:i:s',$myproject->date_end)."\r\n",  FILE_APPEND) ;
			
			if ($myproject->date_end==0)
			{
				$myproject->date_end = dol_mktime(0, 0, 0, date('n'), date('j'), date('Y'));
			}

			if ($myproject->date_end<$myproject->date_start)
			{
				$myproject->date_end = $myproject->date_start ;
			}

			$myproject->update($user);
			//echo 'date_start: '.$myproject->date_start.' | date_end: '.$myproject->date_end.' | options_date_fin_contrat: '.$myproject->array_options['options_date_fin_contrat']."<br>";
			//echo "<pre>" ; var_dump($_REQUEST) ; echo "<br><br>" ;
			//var_dump($myproject) ;
			//echo 'motif_perdu: '.GETPOST('motif_perdu', 'string') ;
			//exit() ;
			//file_put_contents($conf->mycompany->dir_output.'/log_'.date('Ymd').'.txt', date('Y-m-d H:i:s')." * close_auto_project | update | project statut: ".$myproject->statut." | date_end: ".date('Y-m-d H:i:s',$myproject->date_end)."\r\n",  FILE_APPEND) ;
			
			if ($statut==6) {
				$id_project_tacite = 0 ;
				if (GETPOST('signe_tacite_reconduction', 'string') =='on')
				{
					$id_project_tacite = $this->create_auto_tacite_recond($object, $myproject, $user); // creer affaire tacite
					//echo "Create_tacite_reconduction" ; exit() ;
				}

				$this->send_mail_propal('SIGNED', $user, $object, $myproject, $id_project_tacite) ;
			}
			
			$url_maj = DIR_HTTP."custom/batibarrmod/scripts/interface.php?mode=ANALYSE_STATUS_CLIENT&DEBUG&id_soc=".$object->socid ;
			file_get_contents($url_maj) ;
			//echo $url_maj." !!!<br>\r\n" ;
		}

		if (!empty($error)) {
			return -1;
		}
		return 0;
	}

	private function send_mail_propal($status, $user, $propal, $myproject, $id_project_tacite){
		$label_support = '' ;
		$contact_doc = '' ;
		$lib_societe = '' ;
		
		require_once DOL_DOCUMENT_ROOT.'/core/class/extrafields.class.php';
		require_once DOL_DOCUMENT_ROOT.'/societe/class/societe.class.php';

		$companystatic = new Societe($this->db);
		$result = $companystatic->fetch($myproject->socid);
		if ($result < 0) {
			$this->errors[] = $companystatic->error;
			$error++;
		}
		else {
			$lib_societe = $companystatic->nom;
		}

		//$email_title = utf8_decode('[Batiactu Groupe] La proposition '.$propal->ref.' a été clôturée signée') ;

		if ($myproject->array_options['options_ba_support']!='')
		{
			$sql_sup = "SELECT label FROM ".MAIN_DB_PREFIX.'c_ba_support';
			$sql_sup.= " WHERE code='".$myproject->array_options['options_ba_support']."'";
	
			$resql = $this->db->query($sql_sup);
			if ($resql)
			{
				$obj = $this->db->fetch_object($resql);
				$label_support = $obj->label ;
			}
			else $label_support = $myproject->array_options['options_ba_support'] ;
		}

		 // recupere les contacts du project
		$nb_contact = 0 ;
		$f_user_found = FALSE ;
		$sql_contact = "SELECT sc.firstname as firstname, sc.lastname as lastname, ec.fk_socpeople as id_contact";
		$sql_contact.= " FROM ".MAIN_DB_PREFIX."user sc JOIN ".MAIN_DB_PREFIX."element_contact ec ON sc.rowid = ec.fk_socpeople";
		$sql_contact.= " WHERE ec.element_id = ".$myproject->id." AND (ec.fk_c_type_contact=160 OR ec.fk_c_type_contact=161)";
		$sql_contact.= " ORDER BY ec.fk_c_type_contact ASC, sc.lastname ASC, sc.firstname ASC";
		//$contact_doc.= $sql_contact."<br>\r\n" ;
		$resql = $this->db->query($sql_contact);
		if ($resql)
		{
			while ($obj_con = $this->db->fetch_object($resql)) {
				if ($contact_doc!='') $contact_doc.= "<br>\r\n" ;
				$contact_doc.= $obj_con->firstname.' '.$obj_con->lastname ;
				$nb_contact++ ;
				
				if ($user->id==$obj_con->id_contact) $f_user_found = TRUE ;
			}
		}

		if ($nb_contact==0) { // recupere les contacts de la propal si pas de contact project
			$sql_contact = "SELECT sc.firstname as firstname, sc.lastname as lastname, ec.fk_socpeople as id_contact";
			$sql_contact.= " FROM ".MAIN_DB_PREFIX."user sc JOIN ".MAIN_DB_PREFIX."element_contact ec ON sc.rowid = ec.fk_socpeople";
			$sql_contact.= " WHERE ec.element_id = ".$propal->id." AND ec.fk_c_type_contact=31";
			$sql_contact.= " ORDER BY sc.lastname ASC, sc.firstname ASC";
			//$contact_doc.= $sql_contact."<br>\r\n" ;
			$resql = $this->db->query($sql_contact);
			if ($resql)
			{
				while ($obj_con = $this->db->fetch_object($resql)) {
					if ($contact_doc!='') $contact_doc.= "<br>\r\n" ;
					$contact_doc.= $obj_con->firstname.' '.$obj_con->lastname ;
					$nb_contact++ ;
					
					if ($user->id==$obj_con->id_contact) $f_user_found = TRUE ;
				}
			}
		}

		$extrafields = new ExtraFields($this->db);
		$extrafields->fetch_name_optionals_label('projet');
		$TList_contrat = $extrafields->attributes['projet']['param']['type_contrat']['options'] ;
		//echo "<pre>".print_r($TList_contrat, TRUE) ; exit() ;
		$TTacite_reconduction = $extrafields->attributes['projet']['param']['tacite_reconduction']['options'] ;
		//echo "<pre>".print_r($TTacite_reconduction, TRUE) ; exit() ;

		$email_title = utf8_decode('[Batibarr] Affaire signée : '.$label_support.' - '.$lib_societe.'') ; // - '.$myproject->ref.'

		$email_corps = '' ;
		$email_corps.= "A facturer en ".$myproject->array_options['options_ca_annee']." :<br>\r\n";
		$email_corps.= "<b>Société :</b> ".$lib_societe." (Id : ".$myproject->socid.")<br>\r\n";
		$email_corps.= "<br>\r\n";
		$email_corps.= "<b>Montant de l'affaire : ".number_format($myproject->array_options['options_amount_real'], 2, ',', ' ')." € HT</b><br>\r\n";
		$email_corps.= "<b>Affaire :</b> ".$label_support." - ".$myproject->title." (Réf. : ".$myproject->ref." | Id : ".$myproject->id.")<br>\r\n";
		$email_corps.= "<b>Référence client :</b> ".$propal->ref_client."<br>\r\n";
		if (GETPOSTISSET('particularite_facture')) $email_corps.= "<b>Particularité de Facturation :</b> ".GETPOST('particularite_facture', 'string')."<br>\r\n" ;
		if (GETPOSTISSET('note_private')) $email_corps.= "<b>Note :</b> ".GETPOST('note_private', 'string')."<br>\r\n" ;
		$email_corps.= "<b>Compl&eacute;ment :</b> ".$myproject->array_options['options_ba_complement']."<br>\r\n" ;
		$email_corps.= "<b>Tacite reconduction :</b> ".(isset($TTacite_reconduction[ $myproject->array_options['options_tacite_reconduction'] ]) ? $TTacite_reconduction[ $myproject->array_options['options_tacite_reconduction'] ] : $myproject->array_options['options_tacite_reconduction'] ) ."<br>\r\n" ;

		if (!$f_user_found) $email_corps.= "<b>Affaire validée par :</b> ".$user->firstname." ".$user->lastname."<br>\r\n";

		if ($nb_contact > 1)
		{
			$email_corps.= "<b>Responsables de l'affaire :</b><br>\r\n";
			$email_corps.= $contact_doc;
		}
		else 
		{
			$email_corps.= "<b>Responsable de l'affaire :</b> ".$contact_doc."<br>\r\n";
		}

		$email_corps.= "<br>\r\n";
		$email_corps.= "<b>Date de signature :</b> ".date('d/m/Y', $myproject->array_options['options_date_signe'])."<br>\r\n";
		$email_corps.= "<b>Contrat :</b> ".(isset($TList_contrat[ $myproject->array_options['options_type_contrat'] ]) ? $TList_contrat[ $myproject->array_options['options_type_contrat'] ] : $myproject->array_options['options_type_contrat'] )."<br>\r\n";
		$email_corps.= "<b>Durée :</b> ".$myproject->array_options['options_duree_contrat']." Mois<br>\r\n";
		$email_corps.= "<b>Date de fin de contrat :</b> ".date('d/m/Y', $myproject->array_options['options_date_fin_contrat'])."<br>\r\n";
		$email_corps.= "<br>\r\n";
		//$email_corps.= "<b>Echéancier de facturation :</b> ".$myproject->array_options['options_echeancier']."<br>\r\n";
		$email_corps.= "<b>Echéancier de facturation :</b><br>\r\n";
		$email_corps.= $myproject->array_options['options_echeancier']."<br>\r\n";
		$email_corps.= "<br>\r\n";
		$email_corps.= "<b>Budget N-1 :</b> ".number_format($myproject->budget_amount, 2, ',', ' ')." € HT<br>\r\n" ;
		$email_corps.= "<b>Proposition :</b> ".$propal->ref." (Id : ".$propal->id.")<br>\r\n";
		if ($id_project_tacite > 0) $email_corps.= "<br>\r\nCréation d'une affaire de tacite reconduction (Id : ".$id_project_tacite.")<br>\r\n";
		
		$Tparams = array(
			'emailto'=>( ($_SERVER['SERVER_NAME']=='dev.batibarr.batiactugroupe.com' || $_SERVER['SERVER_NAME']=='dev.batibarr-16.batiactugroupe.com') ? 'yann.robin@batiactugroupe.com' : 'affaires.signees@batiactugroupe.com,yann.robin@batiactugroupe.com'), //jessica.davin@batiactugroupe.com,thomas.colencon@batiactugroupe.com,
			'title'=>$email_title,
			'body'=>$email_corps,
			'priority'=>5,
			'type_send'=>'batibarr-signed',
			'sender_name'=>utf8_decode('Batibarr Affaire signée'),
		);

		if ($user->email!='') $Tparams['reply_to'] = $user->email ;

		$this->send_email_via_sendmail($Tparams, 'batibarr') ;
		
		//echo "<pre>" ;
		//var_dump($user) ;
		//echo "</pre><hr><pre>" ;
		//var_dump($propal) ;
		//echo "</pre><hr><pre>" ;
		//var_dump($myproject) ;
		//exit() ;

		//return $env ;
	}

	function send_email_via_sendmail($TData, $projet, $service='envoiSimple'){
		global $conf, $langs;
		$url = 'https://bo-sendmail.batiactu.com/index.php?PROJECT='.$projet;
	
		if (isset($TData['emailto'])) {
			@set_time_limit(300);
			$emailfrom = 'batibarr@batiactu.info';
	
			$params = array();
			$params['UTF8ENCODE'] = 1;
			$params['PROJECT'] = $projet;
			$params['service'] = $service;
			$params['EMAIL_TO'] = $TData['emailto'];
			$params['priority'] = isset($TData['priority']) ? $TData['priority'] : 4;
			$params['type_send'] = isset($TData['type_send']) ? $TData['type_send'] : 'batibarr-def';
			//$params['email_from'] = "alerte-produitheque"; deja defini dans .ini
			//$params['sender_name'] = "Batiactu Alertes Produitheque"; deja defini dans .ini
			if (isset($TData['sender_name'])) $params['sender_name'] = $TData['sender_name'] ;
			if (isset($TData['reply_to'])) $params['reply_to'] = $TData['reply_to'] ;
			$params['TITLE'] = isset($TData['title']) ? $TData['title'] : 'NO TITLE' ;
			$params['BODY'] = isset($TData['body']) ? $TData['body'] : 'NO DATA' ;

			file_put_contents($conf->mycompany->dir_output."/sendmail_".date('Ymd').".txt", "# ".date('Y-m-d H:i:s')
				." # SEND EMAIL TO ".$params['EMAIL_TO']." #\r\n"
				.$url."\r\n\r\n"
				.print_r($params, TRUE)."\r\n\r\n", FILE_APPEND) ;
			$res = $this->file_post_contents_curl($url, $params);
			//file_put_contents($conf->mycompany->dir_output."/sendmail_".date('Ymd').".txt", $res, FILE_APPEND) ;
			//echo $res ;
			//echo $conf->mycompany->dir_output."/sendmail_".date('Ymd').".txt" ;
			//exit() ;
	
			//if (isset($_REQUEST['DEBUG'])) echo "<br>\r\n* ".date('H:i:s')." * SENDMAIL PROJET Terminé<br>\r\n" ;
			//flush() ;
			//if (isset($_REQUEST['DEBUG'])) echo $res."<br>\r\n******************************************<br>\r\n" ;
			//return $res;
		}
		/*else {
			return FALSE ;
		}*/
	}

	function file_post_contents_curl($url, $postdata=array(), $debug = FALSE) {

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, false);

		if(strpos($url,'?')===FALSE){
			$url .= "?FROM=".urlencode($_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
		}
		else{
			$url .= "&FROM=".urlencode($_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
		}

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		if (is_array($postdata)) {
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postdata));
		}
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20);

		$result = curl_exec($ch);
		if(curl_errno($ch)) {
			echo 'Erreur Curl : ' . curl_error($ch)."<br>\r\n";
			error_log ( 'Erreur Curl : ' . curl_error($ch).' (call: '.$url.' | '.serialize($postdata).')' ) ;
		}
		curl_close($ch);
		//echo "termine";
		return $result;
	}

	/**
	 * Function called when a Dolibarrr business event is done.
	 * All functions "runTrigger" are triggered if file
	 * is inside directory core/triggers
	 *
	 * @param string 		$action 	Event action code
	 * @param CommonObject 	$object 	Object
	 * @param User 			$user 		Object user
	 * @param Translate 	$langs 		Object langs
	 * @param Conf 			$conf 		Object conf
	 * @return int              		<0 if KO, 0 if no triggered ran, >0 if OK
	 */
	public function runTrigger($action, $object, User $user, Translate $langs, Conf $conf)
	{
		if (empty($conf->batibarrmod->enabled)) return 0; // If module is not enabled, we do nothing

		// Put here code you want to execute when a Dolibarr business events occurs.
		// Data and type of action are stored into $object and $action

		/*if ($user->admin==1) {
			echo "*************************** runTrigger action: ".$action." - ".$object->fk_soc." *************<br>\n" ;
			//echo "REQUEST: <pre>".print_r($_REQUEST, TRUE)."</pre><br />\r\n" ;
			//echo "object: <pre>".print_r($object, TRUE)."</pre><br />\r\n" ;
			//echo "*************<br>\n" ;
			//exit() ;
		}
		*/

		switch ($action) {
			case 'printFieldListTitle':
				echo "!!! printFieldListTitle !!!" ;
				break;
			case 'COMPANY_SENTBYMAIL':
				if (isset($object->sendtoid) && $object->sendtoid > 0) {
					$id_contact = $object->sendtoid ;
					
					require_once DOL_DOCUMENT_ROOT.'/comm/action/class/actioncomm.class.php';
					$elementid = $object->id; // id of object
					$elementtype = $object->element;
					$now = dol_now();

					$sql = "SELECT fk_soc, email FROM ".MAIN_DB_PREFIX."socpeople WHERE rowid = ".$id_contact ;
					//echo $sql."<br>\r\n" ;
					$resql = $this->db->query($sql);
					if ($resql)
					{
						$obj = $this->db->fetch_object($resql);
						$id_soc = $obj->fk_soc ;

						/*echo "*************<br>\n" ;
						echo "REQUEST: <pre>".print_r($_REQUEST, TRUE)."</pre><br />\r\n" ;
						echo "*************<br>\n" ;
						echo "object: <pre>".print_r($object, TRUE)."</pre><br />\r\n" ;
						echo "*************<br>\n" ;
						echo "user: <pre>".print_r($user, TRUE)."</pre><br />\r\n" ;
						echo "*************<br>\n" ;*/

						$fromtype = GETPOST('fromtype');
						if ($fromtype === 'user') {
							$from = $user->getFullName($langs).' &lt;'.$user->email.'&gt;';
						} elseif ($fromtype === 'company') {
							$from = $conf->global->MAIN_INFO_SOCIETE_NOM.' &lt;'.$conf->global->MAIN_INFO_SOCIETE_MAIL.'&gt;';
						} elseif (preg_match('/user_aliases_(\d+)/', $fromtype, $reg)) {
							$tmp = explode(',', $user->email_aliases);
							$from = trim($tmp[($reg[1] - 1)]);
						} elseif (preg_match('/global_aliases_(\d+)/', $fromtype, $reg)) {
							$tmp = explode(',', $conf->global->MAIN_INFO_SOCIETE_MAIL_ALIASES);
							$from = trim($tmp[($reg[1] - 1)]);
						} elseif (preg_match('/senderprofile_(\d+)_(\d+)/', $fromtype, $reg)) {
							$sql = 'SELECT rowid, label, email FROM '.MAIN_DB_PREFIX.'c_email_senderprofile WHERE rowid = '.(int) $reg[1];
							$resql = $db->query($sql);
							$obj = $db->fetch_object($resql);
							if ($obj)
							{
								$from = $obj->label.' &lt;'.$obj->email.'&gt;';
							}
						} else {
							$from = $_POST['fromname'].' &lt;'.$_POST['frommail'].'&gt;';
						}

						$author = dolGetFirstLastname($user->firstname, $user->lastname); ;
						$sendto = $obj->email ;
						$sendtocc = $_REQUEST['sendtocc'] ;
						$subject = $_REQUEST['subject'] ;
						$message = $_REQUEST['message'] ;
						//echo "******* fromtype: ".$fromtype." => ".$from." ******<br>\n" ;

						$note_private = '' ;
						if ($author!='') $note_private = dol_concatdesc($note_private, $langs->transnoentities('Author').': '.dol_escape_htmltag($author));
						if ($from!='') $note_private = dol_concatdesc($note_private, $langs->transnoentities('MailFrom').': '.dol_escape_htmltag($from));
						$note_private = dol_concatdesc($note_private, $langs->transnoentities('MailTo').': '.dol_escape_htmltag($sendto));
						if ($sendtocc) $note_private = dol_concatdesc($note_private, $langs->transnoentities('Bcc').": ".dol_escape_htmltag($sendtocc));
						$note_private = dol_concatdesc($note_private, $langs->transnoentities('MailTopic').": ".$subject);
						$note_private = dol_concatdesc($note_private, $langs->transnoentities('TextUsedInTheMessageBody').":");
						$note_private = dol_concatdesc($note_private, $message);

						/*if (isset($object->note_private) AND trim($object->note_private)!='') {
						}
						else {
							$note_private = $langs->transnoentities('MailGroupeAction_ba', $subject) ;
						}*/

						// Insertion action
						$actioncomm = new ActionComm($this->db);
						$actioncomm->type_code   = 'AC_OTH_AUTO'; // Type of event ('AC_OTH', 'AC_OTH_AUTO', 'AC_XXX'...)
						$actioncomm->code        = 'AC_EMAIL';
						$actioncomm->label       = $langs->transnoentities('MailGroupeAction_ba', $subject);
						$actioncomm->note_private = $note_private;
						$actioncomm->datep       = $now;
						$actioncomm->datef       = $now;
						$actioncomm->durationp   = 0;
						$actioncomm->percentage  = -1; // Not applicable
						$actioncomm->socid       = $id_soc;
						$actioncomm->contact_id  = $id_contact; // deprecated, use ->socpeopleassigned instead
						$actioncomm->authorid    = $user->id; // User saving action
						$actioncomm->userownerid = $user->id; // Owner of action
						// Fields defined when action is an email (content should be into object->actionmsg to be added into note, subject into object->actionms2 to be added into label)
						$actioncomm->email_msgid   = empty($object->email_msgid) ? null : $object->email_msgid;
						$actioncomm->email_from    = empty($object->email_from) ? null : $object->email_from;
						$actioncomm->email_sender  = empty($object->email_sender) ? null : $object->email_sender;
						$actioncomm->email_to      = empty($object->email_to) ? null : $object->email_to;
						$actioncomm->email_tocc    = empty($object->email_tocc) ? null : $object->email_tocc;
						$actioncomm->email_tobcc   = empty($object->email_tobcc) ? null : $object->email_tobcc;
						$actioncomm->email_subject = empty($object->email_subject) ? null : $object->email_subject;
						$actioncomm->errors_to     = empty($object->errors_to) ? null : $object->errors_to;

						// Object linked (if link is for thirdparty, contact, project it is a recording error. We should not have links in link table
						// for such objects because there is already a dedicated field into table llx_actioncomm or llx_actioncomm_resources.
						if (!in_array($elementtype, array('societe', 'contact', 'project'))) {
							$actioncomm->fk_element  = $elementid;
							$actioncomm->elementtype = $elementtype.($elementmodule ? '@'.$elementmodule : '');
						}

						if (property_exists($object, 'attachedfiles') && is_array($object->attachedfiles) && count($object->attachedfiles) > 0) {
							$actioncomm->attachedfiles = $object->attachedfiles;
						}
						if (property_exists($object, 'sendtouserid') && is_array($object->sendtouserid) && count($object->sendtouserid) > 0) {
							$actioncomm->userassigned = $object->sendtouserid;
						}
						
						$actioncomm->socpeopleassigned[$object->sendtoid] = $object->sendtoid;

						$ret = $actioncomm->create($user); // User creating action
					}
				}

				/*echo "=============== COMM ====================" ;
				echo "<pre>".print_r($actioncomm, TRUE)."</pre><br />\r\n" ;
				echo "=============== OBJECT ====================" ;
				echo "<pre>".print_r($object, TRUE)."</pre><br />\r\n" ;
				echo "=============== REQUEST ====================" ;
				echo "<pre>".print_r($_REQUEST, TRUE)."</pre><br />\r\n" ;
				exit() ;*/

				break;
			// Tickets
			case 'TICKET_CREATE':
				$error=0;
				//echo "*************************** runTrigger action: ".$action." - ".$object->fk_soc." *************<br>\n" ;

				if (empty($object->fk_soc)) {
					$msg = $langs->trans("ErrorFieldRequired", $langs->transnoentities("ThirdParty")) ;
					if (!in_array($msg, $this->errors)) $this->errors[] = $msg;
					$error++;
				}
				else if (empty($object->fk_project)) {
					$msg = $langs->trans("ErrorFieldRequired", $langs->transnoentities("Project"));
					$this->errors[] = $msg;
					$error++;
				}
				
				$_REQUEST['error'] = $error ;
				if (!empty($error)) {
					return -1;
				}

				$this->ticket_send_email_create($object, $user);

				return 0;
				break;
			case 'TICKET_CLOSE':
				$this->ticket_send_email_close($object, $user);
				break;
			case 'TICKET_MODIFY':
				$this->ticket_send_email_modify($object, $user);
				break;
			//case 'TICKET_DELETE':
				//exit('TICKET_DELETE') ;
				//break;
			case 'TICKET_ASSIGNED':
				$this->ticket_send_email_assigne($object, $user);
				break;
			case 'TICKET_STATUT_CHANGE':
				$this->ticket_send_email_change($object, $user);
				//echo "*************************** runTrigger action: ".$action." - ".$object->fk_soc." *************<br>\n" ;
				//exit('TICKET_STATUT_CHANGE') ;
				break;
			case 'ACTION_CREATE':
				if (GETPOSTISSET('btn_add_message'))
				{
					$this->ticket_send_email_msg($object, $user);
				}
				break;

			// Users
			//case 'USER_CREATE':
			//case 'USER_MODIFY':
			//case 'USER_NEW_PASSWORD':
			//case 'USER_ENABLEDISABLE':
			//case 'USER_DELETE':
			//case 'USER_SETINGROUP':
			//case 'USER_REMOVEFROMGROUP':

			// Actions
			//case 'ACTION_MODIFY':
			//case 'ACTION_CREATE':
			//case 'ACTION_DELETE':

			// Groups
			//case 'USERGROUP_CREATE':
			//case 'USERGROUP_MODIFY':
			//case 'USERGROUP_DELETE':

			// Companies
			case 'COMPANY_CREATE':
				return $this->fieldMandatoryCompany($object, $langs);
			break;
			case 'COMPANY_MODIFY':
				return $this->fieldMandatoryCompany($object, $langs);
			break;
			//case 'COMPANY_DELETE':

			// Contacts
			case 'CONTACT_CREATE':
				return $this->fieldMandatoryContact($object, $langs);
			break;
			case 'CONTACT_MODIFY':

				$id_update = GETPOSTISSET('action') ? GETPOST('id', 'int') : 0 ;

				$action = GETPOSTISSET('action') ? GETPOST('action', 'alpha') : 'view';
				//echo $id_update."<br>\r\n" ;
				//echo $action."<br>\r\n" ;

				if (!isset($_POST['options_ba_contact_tag']) && $id_update > 0 && $action='update') {
					$sql = "UPDATE ".MAIN_DB_PREFIX."socpeople_extrafields SET ba_contact_tag='' WHERE fk_object = ".$id_update ;
					//echo $sql."<br>\r\n" ;
					$resql = $this->db->query($sql);
				}
				//exit() ;

				return $this->fieldMandatoryContact($object, $langs);
			break;
			//case 'CONTACT_DELETE':
			//case 'CONTACT_ENABLEDISABLE':

			// Products
			//case 'PRODUCT_CREATE':
			//case 'PRODUCT_MODIFY':
			//case 'PRODUCT_DELETE':
			//case 'PRODUCT_PRICE_MODIFY':
			//case 'PRODUCT_SET_MULTILANGS':
			//case 'PRODUCT_DEL_MULTILANGS':

			//Stock mouvement
			//case 'STOCK_MOVEMENT':

			//MYECMDIR
			//case 'MYECMDIR_CREATE':
			//case 'MYECMDIR_MODIFY':
			//case 'MYECMDIR_DELETE':

			// Customer orders
			//case 'ORDER_CREATE':
			//case 'ORDER_MODIFY':
			//case 'ORDER_VALIDATE':
			//case 'ORDER_DELETE':
			//case 'ORDER_CANCEL':
			//case 'ORDER_SENTBYMAIL':
			//case 'ORDER_CLASSIFY_BILLED':
			//case 'ORDER_SETDRAFT':
			//case 'LINEORDER_INSERT':
			//case 'LINEORDER_UPDATE':
			//case 'LINEORDER_DELETE':

			// Supplier orders
			//case 'ORDER_SUPPLIER_CREATE':
			//case 'ORDER_SUPPLIER_MODIFY':
			//case 'ORDER_SUPPLIER_VALIDATE':
			//case 'ORDER_SUPPLIER_DELETE':
			//case 'ORDER_SUPPLIER_APPROVE':
			//case 'ORDER_SUPPLIER_REFUSE':
			//case 'ORDER_SUPPLIER_CANCEL':
			//case 'ORDER_SUPPLIER_SENTBYMAIL':
			//case 'ORDER_SUPPLIER_DISPATCH':
			//case 'LINEORDER_SUPPLIER_DISPATCH':
			//case 'LINEORDER_SUPPLIER_CREATE':
			//case 'LINEORDER_SUPPLIER_UPDATE':
			//case 'LINEORDER_SUPPLIER_DELETE':

			// Proposals
			//case 'PROPAL_CREATE':
			//case 'PROPAL_MODIFY':
			//case 'PROPAL_VALIDATE':
			//case 'PROPAL_SENTBYMAIL':
			case 'PROPAL_CLOSE_SIGNED':
				$object->ref_client = GETPOST('ref_client', 'string') ;
				$object->array_options['options_echeancier'] = GETPOST('echeancier', 'string') ;
				$object->array_options['options_date_signe'] = dol_mktime(0, 0, 0, GETPOST('date_signemonth', 'int'), GETPOST('date_signeday', 'int'), GETPOST('date_signeyear', 'int')); ;
				$object->update($user);

				$this->close_auto_project($object, $user, 6); // signe
				break;
			case 'PROPAL_CLOSE_REFUSED':
				$var_refus = GETPOST('close_project_refus', 'string') ;
				if ($var_refus=='on')
				{
					$this->close_auto_project($object, $user, 7); // perdu
				}
				break;
				
			// Bills
			case 'BILL_SENTBYMAIL':
				//global $object, $from, $sendto ;
				$object->actionmsg2 = $langs->transnoentities('BillMailSentBy_BA') ;
				require_once DOL_DOCUMENT_ROOT.'/comm/action/class/actioncomm.class.php';
				$actioncomm = new ActionComm($this->db);
				$actioncomm->fetch($_SESSION['LAST_ACTION_CREATED']);
				$actioncomm->label = $object->actionmsg2 ;
				$ret = $actioncomm->update($user); // Modify label

				//echo "TRIGGER MOD BILL_SENTBYMAIL ".$object->actionmsg2."<br />\r\n" ;
				//$actionmsg2 = $langs->transnoentities('MailSentBy').' '.CMailFile::getValidAddress($from, 4, 0, 1).' '.$langs->transnoentities('at').' '.CMailFile::getValidAddress($sendto, 4, 0, 1);
				//$actionmsg2 = strtr($actionmsg2, array($langs->transnoentities('MailSentBy')=>'Facture envoyée par mail')) ;
				//echo "=> ".$object->actionmsg2."<br />\r\n" ;
				//exit("LAST_ACTION_CREATED: ".$_SESSION['LAST_ACTION_CREATED']);
				if ($ret > 0) {
					//$_SESSION['LAST_ACTION_CREATED'] = $ret;
					return 1;
				} else {
					return -1;
				}
				break;
			//case 'PROPAL_DELETE':
			//case 'LINEPROPAL_INSERT':
			//case 'LINEPROPAL_UPDATE':
			//case 'LINEPROPAL_DELETE':

			// SupplierProposal
			//case 'SUPPLIER_PROPOSAL_CREATE':
			//case 'SUPPLIER_PROPOSAL_MODIFY':
			//case 'SUPPLIER_PROPOSAL_VALIDATE':
			//case 'SUPPLIER_PROPOSAL_SENTBYMAIL':
			//case 'SUPPLIER_PROPOSAL_CLOSE_SIGNED':
			//case 'SUPPLIER_PROPOSAL_CLOSE_REFUSED':
			//case 'SUPPLIER_PROPOSAL_DELETE':
			//case 'LINESUPPLIER_PROPOSAL_INSERT':
			//case 'LINESUPPLIER_PROPOSAL_UPDATE':
			//case 'LINESUPPLIER_PROPOSAL_DELETE':

			// Contracts
			//case 'CONTRACT_CREATE':
			//case 'CONTRACT_MODIFY':
			//case 'CONTRACT_ACTIVATE':
			//case 'CONTRACT_CANCEL':
			//case 'CONTRACT_CLOSE':
			//case 'CONTRACT_DELETE':
			//case 'LINECONTRACT_INSERT':
			//case 'LINECONTRACT_UPDATE':
			//case 'LINECONTRACT_DELETE':

			// Bills
			//case 'BILL_CREATE':
			//case 'BILL_MODIFY':
			//case 'BILL_VALIDATE':
			//case 'BILL_UNVALIDATE':
			//case 'BILL_SENTBYMAIL':
			//case 'BILL_CANCEL':
			//case 'BILL_DELETE':
			//case 'BILL_PAYED':
			//case 'LINEBILL_INSERT':
			//case 'LINEBILL_UPDATE':
			//case 'LINEBILL_DELETE':

			//Supplier Bill
			//case 'BILL_SUPPLIER_CREATE':
			//case 'BILL_SUPPLIER_UPDATE':
			//case 'BILL_SUPPLIER_DELETE':
			//case 'BILL_SUPPLIER_PAYED':
			//case 'BILL_SUPPLIER_UNPAYED':
			//case 'BILL_SUPPLIER_VALIDATE':
			//case 'BILL_SUPPLIER_UNVALIDATE':
			//case 'LINEBILL_SUPPLIER_CREATE':
			//case 'LINEBILL_SUPPLIER_UPDATE':
			//case 'LINEBILL_SUPPLIER_DELETE':

			// Payments
			//case 'PAYMENT_CUSTOMER_CREATE':
			//case 'PAYMENT_SUPPLIER_CREATE':
			//case 'PAYMENT_ADD_TO_BANK':
			//case 'PAYMENT_DELETE':

			// Online
			//case 'PAYMENT_PAYBOX_OK':
			//case 'PAYMENT_PAYPAL_OK':
			//case 'PAYMENT_STRIPE_OK':

			// Donation
			//case 'DON_CREATE':
			//case 'DON_UPDATE':
			//case 'DON_DELETE':

			// Interventions
			//case 'FICHINTER_CREATE':
			//case 'FICHINTER_MODIFY':
			//case 'FICHINTER_VALIDATE':
			//case 'FICHINTER_DELETE':
			//case 'LINEFICHINTER_CREATE':
			//case 'LINEFICHINTER_UPDATE':
			//case 'LINEFICHINTER_DELETE':

			// Members
			//case 'MEMBER_CREATE':
			//case 'MEMBER_VALIDATE':
			//case 'MEMBER_SUBSCRIPTION':
			//case 'MEMBER_MODIFY':
			//case 'MEMBER_NEW_PASSWORD':
			//case 'MEMBER_RESILIATE':
			//case 'MEMBER_DELETE':

			// Categories
			//case 'CATEGORY_CREATE':
			//case 'CATEGORY_MODIFY':
			//case 'CATEGORY_DELETE':
			//case 'CATEGORY_SET_MULTILANGS':

			// Projects
			case 'PROJECT_CREATE':
				return $this->fieldMandatoryProject($object, $langs);
			break;
			case 'PROJECT_MODIFY':
				return $this->fieldMandatoryProject($object, $langs);
			break;
			//case 'PROJECT_DELETE':

			// Project tasks
			//case 'TASK_CREATE':
			//case 'TASK_MODIFY':
			//case 'TASK_DELETE':

			// Task time spent
			//case 'TASK_TIMESPENT_CREATE':
			//case 'TASK_TIMESPENT_MODIFY':
			//case 'TASK_TIMESPENT_DELETE':
			//case 'PROJECT_ADD_CONTACT':
			//case 'PROJECT_DELETE_CONTACT':
			//case 'PROJECT_DELETE_RESOURCE':

			// Shipping
			//case 'SHIPPING_CREATE':
			//case 'SHIPPING_MODIFY':
			//case 'SHIPPING_VALIDATE':
			//case 'SHIPPING_SENTBYMAIL':
			//case 'SHIPPING_BILLED':
			//case 'SHIPPING_CLOSED':
			//case 'SHIPPING_REOPEN':
			//case 'SHIPPING_DELETE':

			// and more...

			default:
				dol_syslog("Trigger '".$this->name."' for action '$action' launched by ".__FILE__.". id=".$object->id);
				break;
		}

		return 0;
	}
}
