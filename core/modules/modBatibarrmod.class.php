<?php
/* Copyright (C) 2004-2018  Laurent Destailleur     <eldy@users.sourceforge.net>
 * Copyright (C) 2018-2019  Nicolas ZABOURI         <info@inovea-conseil.com>
 * Copyright (C) 2019-2020  Frédéric France         <frederic.france@netlogic.fr>
 * Copyright (C) 2021 Administrateur
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * 	\defgroup   batibarrmod     Module Batibarrmod
 *  \brief      Batibarrmod module descriptor.
 *
 *  \file       htdocs/batibarrmod/core/modules/modBatibarrmod.class.php
 *  \ingroup    batibarrmod
 *  \brief      Description and activation file for module Batibarrmod
 */
include_once DOL_DOCUMENT_ROOT.'/core/modules/DolibarrModules.class.php';

/**
 *  Description and activation class for module Batibarrmod
 */
class modBatibarrmod extends DolibarrModules
{
	/**
	 * Constructor. Define names, constants, directories, boxes, permissions
	 *
	 * @param DoliDB $db Database handler
	 */
	public function __construct($db)
	{
		global $langs, $conf, $hookmanager;
		$this->db = $db;
		// Add pdfgeneration hook
		if (! is_object($hookmanager))
		{
			include_once DOL_DOCUMENT_ROOT.'/core/class/hookmanager.class.php';
			$hookmanager=new HookManager($this->db);
		}

		// Id for module (must be unique).
		// Use here a free id (See in Home -> System information -> Dolibarr for list of used modules id).
		$this->numero = 428253; // TODO Go on page https://wiki.dolibarr.org/index.php/List_of_modules_id to reserve an id number for your module
		// Key text used to identify module (for permissions, menus, etc...)
		$this->rights_class = 'batibarrmod';
		// Family can be 'base' (core modules),'crm','financial','hr','projects','products','ecm','technic' (transverse modules),'interface' (link with external tools),'other','...'
		// It is used to group modules by family in module setup page
		$this->family = "other";
		// Module position in the family on 2 digits ('01', '10', '20', ...)
		$this->module_position = '90';
		// Gives the possibility for the module, to provide his own family info and position of this family (Overwrite $this->family and $this->module_position. Avoid this)
		//$this->familyinfo = array('myownfamily' => array('position' => '01', 'label' => $langs->trans("MyOwnFamily")));
		// Module label (no space allowed), used if translation string 'ModuleBatibarrmodName' not found (Batibarrmod is name of module).
		$this->name = preg_replace('/^mod/i', '', get_class($this));
		// Module description, used if translation string 'ModuleBatibarrmodDesc' not found (Batibarrmod is name of module).
		$this->description = "BatibarrmodDescription";
		// Used only if file README.md and README-LL.md not found.
		$this->descriptionlong = "Batibarrmod description (Long)";
		$this->editor_name = 'Editor name';
		$this->editor_url = 'https://www.example.com';
		// Possible values for version are: 'development', 'experimental', 'dolibarr', 'dolibarr_deprecated' or a version string like 'x.y.z'
		$this->version = '1.0';
		// Url to the file with your last numberversion of this module
		//$this->url_last_version = 'http://www.example.com/versionmodule.txt';

		// Key used in llx_const table to save module status enabled/disabled (where BATIBARRMOD is value of property name of module in uppercase)
		$this->const_name = 'MAIN_MODULE_'.strtoupper($this->name);
		// Name of image file used for this module.
		// If file is in theme/yourtheme/img directory under name object_pictovalue.png, use this->picto='pictovalue'
		// If file is in module/img directory under name object_pictovalue.png, use this->picto='pictovalue@module'
		$this->picto = 'generic';
		// Define some features supported by module (triggers, login, substitutions, menus, css, etc...)
		$this->module_parts = array(
			// Set this to 1 if module has its own trigger directory (core/triggers)
			'triggers' => 1,
			// Set this to 1 if module has its own login method file (core/login)
			'login' => 0,
			// Set this to 1 if module has its own substitution function file (core/substitutions)
			'substitutions' => 0,
			// Set this to 1 if module has its own menus handler directory (core/menus)
			'menus' => 1,
			// Set this to 1 if module overwrite template dir (core/tpl)
			'tpl' => 0,
			// Set this to 1 if module has its own barcode directory (core/modules/barcode)
			'barcode' => 0,
			// Set this to 1 if module has its own models directory (core/modules/xxx)
			'models' => 1,
			// Set this to 1 if module has its own theme directory (theme)
			'theme' => 0,
			// Set this to relative path of css file if module has its own css file
			'css' => array(
				'/batibarrmod/css/batibarrmod.css.php',
			),
			// Set this to relative path of js file if module must load a js on all pages
			'js' => array(
				'/batibarrmod/js/batibarrmod.js.php',
			),
			// Set here all hooks context managed by module. To find available hook context, make a "grep -r '>initHooks(' *" on source code. You can also set hook context to 'all'
			'hooks' => array(
				'all'=>1,
				'data' => array(
					'index',
					'accountancyindex',
					'accountancycustomerlist',
					'sellsjournal',
					'purchasesjournal',
					'bookkeepinglist',
					'bookkeepingbyaccountlist',
					'admin',
					'comptafileslist',
					'categoryindex',
					'thirdpartiesindex',
					'thirdpartylist',
					'thirdpartycard',
					'thirdpartycontact',
					'thirdpartydao',
					'consumptionthirdparty',
					'thirdpartyticket',
					'thirdpartycomm',
					'projectthirdparty',
					'thirdpartynotification',
					'thirdpartynote',
					'thirdpartydocument',
					'agendathirdparty',
					'commercialindex',
					'productservicelist',
					'propallist',
					'propalcard',
					'projectsindex',
					'productindex',
					'projectlist',
					'projectcard',
					'projectcontactcard',
					'projectticket',
					'projectOverview',
					'projecttaskscard',
					'projectnote',
					'projectdocument',
					'projectinfo',
					'contactlist',
					'contactcard',
					'consumptioncontact',
					'contactnote',
					'contactdocument',
					'contactagenda',
					'usercard',
					'createFrom',
					'productcard',
					'productstatsinvoice',
					'productpricecard',
					'productstatscard',
					'agenda',
					'agendalist',
					'mailingcard',
					'ticketsindex',
					'ticketlist',
					'ticketcard',
					'mail',
					'sendMail',
					'invoicelist',
					'invoicecard',
					'paymentlist',
					'specialexpensesindex',
					'sclist',
					'variouscard',
					'doActions',
					'categoryindex',
					'bankaccountlist',
					'bankcard',
					'banktransactionlist',
					'banktransfer',
					'banktreso',
					'sendinblueindex',
					'sendinbluecontactcard',
					'sendinbluedestlist',
					'actioncard',
					'restrictedArea',
					//'hookcontext1',
					//'hookcontext2',
					//'globallist',
					//'globalcard',
				),
				'entity' => '0',
			),
			// Set this to 1 if features of module are opened to external users
			'moduleforexternal' => 0,
		);
		// Data directories to create when module is enabled.
		// Example: this->dirs = array("/batibarrmod/temp","/batibarrmod/subdir");
		$this->dirs = array("/batibarrmod/temp");
		// Config pages. Put here list of php page, stored into batibarrmod/admin directory, to use to setup module.
		$this->config_page_url = array("setup.php@batibarrmod");
		// Dependencies
		// A condition to hide module
		$this->hidden = false;
		// List of module class names as string that must be enabled if this module is enabled. Example: array('always1'=>'modModuleToEnable1','always2'=>'modModuleToEnable2', 'FR1'=>'modModuleToEnableFR'...)
		$this->depends = array();
		$this->requiredby = array(); // List of module class names as string to disable if this one is disabled. Example: array('modModuleToDisable1', ...)
		$this->conflictwith = array(); // List of module class names as string this module is in conflict with. Example: array('modModuleToDisable1', ...)
		$this->langfiles = array("batibarrmod@batibarrmod");
		$this->phpmin = array(5, 5); // Minimum version of PHP required by module
		$this->need_dolibarr_version = array(11, -3); // Minimum version of Dolibarr required by module
		$this->warnings_activation = array(); // Warning to show when we activate module. array('always'='text') or array('FR'='textfr','ES'='textes'...)
		$this->warnings_activation_ext = array(); // Warning to show when we activate an external module. array('always'='text') or array('FR'='textfr','ES'='textes'...)
		//$this->automatic_activation = array('FR'=>'BatibarrmodWasAutomaticallyActivatedBecauseOfYourCountryChoice');
		//$this->always_enabled = true;								// If true, can't be disabled

		// Constants
		// List of particular constants to add when module is enabled (key, 'chaine', value, desc, visible, 'current' or 'allentities', deleteonunactive)
		// Example: $this->const=array(1 => array('BATIBARRMOD_MYNEWCONST1', 'chaine', 'myvalue', 'This is a constant to add', 1),
		//                             2 => array('BATIBARRMOD_MYNEWCONST2', 'chaine', 'myvalue', 'This is another constant to add', 0, 'current', 1)
		// );
		$this->const = array(
			1 => array('SOCIETE_DISABLE_STATE', 'chaine', 1, 'Département / Canton des contacts', 1, 'current', 1),
			2 => array('SOCIETE_DISABLE_BANKACCOUNT', 'chaine', 1,'Information de paiement', 1, 'current', 1),
			3 => array('CONTACT_DISABLE_PERSONALINFO', 'chaine', 1, 'Informations personnelles', 1, 'current', 1),
			4 => array('PROPALE_ADDON_PDF', 'chaine', "batiactu",'PDF 1 proposition générique', 0, 'current', 0),
			5 => array('PROPALE_ADDON_PDF_EN', 'chaine', "batiactu_en",'PDF 2 proposition english', 0, 'current', 0),
			6 => array('PROPALE_ADDON_PDF_PF', 'chaine', "batiactu_pf",'PDF 3 proposition Proforma', 0, 'current', 0),
			6 => array('FACTURE_ADDON_PDF_BA', 'chaine', "batiactu_invoice",'PDF 1 Facture Batiactu Groupe', 0, 'current', 0),
			//7 => array('PROJECT_DISABLE_PRIVATE_PROJECT', 'chaine', 1,'Visibility private mask', 1, 'current', 1),
		);

		// Some keys to add into the overwriting translation tables
		/*$this->overwrite_translation = array(
			'en_US:ParentCompany'=>'Parent company or reseller',
			'fr_FR:ParentCompany'=>'Maison mère ou revendeur'
		)*/

		if (!isset($conf->batibarrmod) || !isset($conf->batibarrmod->enabled)) {
			$conf->batibarrmod = new stdClass();
			$conf->batibarrmod->enabled = 0;
		}

		// Array to add new pages in new tabs
		$this->tabs = array();
		// Example:
		// $this->tabs[] = array('data'=>'objecttype:+tabname1:Title1:mylangfile@batibarrmod:$user->rights->batibarrmod->read:/batibarrmod/mynewtab1.php?id=__ID__');  					// To add a new tab identified by code tabname1
		// $this->tabs[] = array('data'=>'objecttype:+tabname2:SUBSTITUTION_Title2:mylangfile@batibarrmod:$user->rights->othermodule->read:/batibarrmod/mynewtab2.php?id=__ID__',  	// To add another new tab identified by code tabname2. Label will be result of calling all substitution functions on 'Title2' key.
		// $this->tabs[] = array('data'=>'objecttype:-tabname:NU:conditiontoremove');                                                     										// To remove an existing tab identified by code tabname
		//
		// Where objecttype can be
		// 'categories_x'	  to add a tab in category view (replace 'x' by type of category (0=product, 1=supplier, 2=customer, 3=member)
		// 'contact'          to add a tab in contact view
		// 'contract'         to add a tab in contract view
		// 'group'            to add a tab in group view
		// 'intervention'     to add a tab in intervention view
		// 'invoice'          to add a tab in customer invoice view
		// 'invoice_supplier' to add a tab in supplier invoice view
		// 'member'           to add a tab in fundation member view
		// 'opensurveypoll'	  to add a tab in opensurvey poll view
		// 'order'            to add a tab in customer order view
		// 'order_supplier'   to add a tab in supplier order view
		// 'payment'		  to add a tab in payment view
		// 'payment_supplier' to add a tab in supplier payment view
		// 'product'          to add a tab in product view
		// 'propal'           to add a tab in propal view
		// 'project'          to add a tab in project view
		// 'stock'            to add a tab in stock view
		// 'thirdparty'       to add a tab in third party view
		// 'user'             to add a tab in user view

		// Dictionaries
		$this->dictionaries = array(
			'langs'=>'batibarrmod@batibarrmod',
			// List of tables we want to see into dictonnary editor
			'tabname'=>array(
				MAIN_DB_PREFIX."c_ba_contact",
				MAIN_DB_PREFIX."c_ba_support",
				MAIN_DB_PREFIX."c_ba_fonction",
				MAIN_DB_PREFIX."c_ba_motif_perdu",
				MAIN_DB_PREFIX."c_ba_complement",
			),
			// Label of tables
			'tablib'=>array(
				"Contact Batiactu",
				"Support Batiactu",
				"Fonction Batiactu",
				"Motif perdu Batiactu",
				"Complément",
			),
			// Request to select fields
			'tabsql'=>array(
					'SELECT f.rowid as rowid, f.code, f.label, f.position, f.active FROM '.MAIN_DB_PREFIX.'c_ba_contact as f',
					'SELECT f.rowid as rowid, f.code, f.label, f.url, f.url_lib, f.pdf_mode, f.picto, f.logo, f.no_garde, f.link_cgv, f.color_support, f.accountancy_code_sell,f.accountancy_code_sell_intra,f.accountancy_code_sell_export, f.position, f.active FROM '.MAIN_DB_PREFIX.'c_ba_support as f',
					'SELECT f.rowid as rowid, f.code, f.label, f.position, f.active FROM '.MAIN_DB_PREFIX.'c_ba_fonction as f',
					'SELECT f.rowid as rowid, f.code, f.label, f.position, f.active FROM '.MAIN_DB_PREFIX.'c_ba_motif_perdu as f',
					'SELECT f.rowid as rowid, f.code, f.label, f.annee_event, f.position, f.active FROM '.MAIN_DB_PREFIX.'c_ba_complement as f',
				),
			// Sort order
			'tabsqlsort'=>array(
				"f.position ASC",
				"f.position ASC",
				"f.position ASC",
				"f.position ASC",
				"f.position ASC",
			),
			// List of fields (result of select to show dictionary)
			'tabfield'=>array(
				"code,label,position",
				"code,label,url,url_lib,pdf_mode,picto,logo,no_garde,link_cgv,color_support,accountancy_code_sell,accountancy_code_sell_intra,accountancy_code_sell_export,position",
				"code,label,position",
				"code,label,position",
				"code,label,annee_event,position",
			),
			// List of fields (list of fields to edit a record)
			'tabfieldvalue'=>array(
				"code,label,position",
				"code,label,url,url_lib,pdf_mode,picto,logo,no_garde,link_cgv,color_support,accountancy_code_sell,accountancy_code_sell_intra,accountancy_code_sell_export,position",
				"code,label,position",
				"code,label,position",
				"code,label,annee_event,position",
			),
			// List of fields (list of fields for insert)
			'tabfieldinsert'=>array(
				"code,label,position",
				"code,label,url,url_lib,pdf_mode,picto,logo,no_garde,link_cgv,color_support,accountancy_code_sell,accountancy_code_sell_intra,accountancy_code_sell_export,position",
				"code,label,position",
				"code,label,position",
				"code,label,annee_event,position",
			),
			// Name of columns with primary key (try to always name it 'rowid')
			'tabrowid'=>array(
				"rowid",
				"rowid",
				"rowid",
				"rowid",
				"rowid"
			),
			// Condition to show each dictionary
			'tabcond'=>array(
				$conf->batibarrmod->enabled,
				$conf->batibarrmod->enabled,
				$conf->batibarrmod->enabled,
				$conf->batibarrmod->enabled,
				$conf->batibarrmod->enabled,
			)
		);
		/* Example:
		$this->dictionaries=array(
			'langs'=>'batibarrmod@batibarrmod',
			// List of tables we want to see into dictonnary editor
			'tabname'=>array(MAIN_DB_PREFIX."table1", MAIN_DB_PREFIX."table2", MAIN_DB_PREFIX."table3"),
			// Label of tables
			'tablib'=>array("Table1", "Table2", "Table3"),
			// Request to select fields
			'tabsql'=>array('SELECT f.rowid as rowid, f.code, f.label, f.active FROM '.MAIN_DB_PREFIX.'table1 as f', 'SELECT f.rowid as rowid, f.code, f.label, f.active FROM '.MAIN_DB_PREFIX.'table2 as f', 'SELECT f.rowid as rowid, f.code, f.label, f.active FROM '.MAIN_DB_PREFIX.'table3 as f'),
			// Sort order
			'tabsqlsort'=>array("label ASC", "label ASC", "label ASC"),
			// List of fields (result of select to show dictionary)
			'tabfield'=>array("code,label", "code,label", "code,label"),
			// List of fields (list of fields to edit a record)
			'tabfieldvalue'=>array("code,label", "code,label", "code,label"),
			// List of fields (list of fields for insert)
			'tabfieldinsert'=>array("code,label", "code,label", "code,label"),
			// Name of columns with primary key (try to always name it 'rowid')
			'tabrowid'=>array("rowid", "rowid", "rowid"),
			// Condition to show each dictionary
			'tabcond'=>array($conf->batibarrmod->enabled, $conf->batibarrmod->enabled, $conf->batibarrmod->enabled)
		);
		*/

		// Boxes/Widgets
		// Add here list of php file(s) stored in batibarrmod/core/boxes that contains a class to show a widget.
		$this->boxes = array(
			0 => array(
				'file' => 'box_graph_stats_comm.php@batibarrmod',
				'note' => "Chiffre d'affaires par commercial",
				'enabledbydefaulton' => 'Home',
			),
			1 => array(
				'file' => 'box_graph_stats_support.php@batibarrmod',
				'note' => "Chiffre d'affaires par support",
				'enabledbydefaulton' => 'Home',
			),
			2 => array(
				'file' => 'box_graph_stats_month.php@batibarrmod',
				'note' => "Chiffre d'affaires par mois",
				'enabledbydefaulton' => 'Home',
			),
			//  0 => array(
			//      'file' => 'batibarrmodwidget1.php@batibarrmod',
			//      'note' => 'Widget provided by Batibarrmod',
			//      'enabledbydefaulton' => 'Home',
			//  ),
			//  ...
		);

		// Cronjobs (List of cron jobs entries to add when module is enabled)
		// unit_frequency must be 60 for minute, 3600 for hour, 86400 for day, 604800 for week
		$this->cronjobs = array(
			//  0 => array(
			//      'label' => 'MyJob label',
			//      'jobtype' => 'method',
			//      'class' => '/batibarrmod/class/myobject.class.php',
			//      'objectname' => 'MyObject',
			//      'method' => 'doScheduledJob',
			//      'parameters' => '',
			//      'comment' => 'Comment',
			//      'frequency' => 2,
			//      'unitfrequency' => 3600,
			//      'status' => 0,
			//      'test' => '$conf->batibarrmod->enabled',
			//      'priority' => 50,
			//  ),
		);
		// Example: $this->cronjobs=array(
		//    0=>array('label'=>'My label', 'jobtype'=>'method', 'class'=>'/dir/class/file.class.php', 'objectname'=>'MyClass', 'method'=>'myMethod', 'parameters'=>'param1, param2', 'comment'=>'Comment', 'frequency'=>2, 'unitfrequency'=>3600, 'status'=>0, 'test'=>'$conf->batibarrmod->enabled', 'priority'=>50),
		//    1=>array('label'=>'My label', 'jobtype'=>'command', 'command'=>'', 'parameters'=>'param1, param2', 'comment'=>'Comment', 'frequency'=>1, 'unitfrequency'=>3600*24, 'status'=>0, 'test'=>'$conf->batibarrmod->enabled', 'priority'=>50)
		// );

		// Permissions provided by this module
		$this->rights = array();
		$r = 0;
		// Add here entries to declare new permissions
		/* BEGIN MODULEBUILDER PERMISSIONS */
		$this->rights[$r][0] = $this->numero + $r; // Permission id (must not be already used)
		$this->rights[$r][1] = 'Read objects of Batibarrmod'; // Permission label
		$this->rights[$r][4] = 'myobject'; // In php code, permission will be checked by test if ($user->rights->batibarrmod->level1->level2)
		$this->rights[$r][5] = 'read'; // In php code, permission will be checked by test if ($user->rights->batibarrmod->level1->level2)
		$r++;
		$this->rights[$r][0] = $this->numero + $r; // Permission id (must not be already used)
		$this->rights[$r][1] = 'Create/Update objects of Batibarrmod'; // Permission label
		$this->rights[$r][4] = 'myobject'; // In php code, permission will be checked by test if ($user->rights->batibarrmod->level1->level2)
		$this->rights[$r][5] = 'write'; // In php code, permission will be checked by test if ($user->rights->batibarrmod->level1->level2)
		$r++;
		$this->rights[$r][0] = $this->numero + $r; // Permission id (must not be already used)
		$this->rights[$r][1] = 'Delete objects of Batibarrmod'; // Permission label
		$this->rights[$r][4] = 'myobject'; // In php code, permission will be checked by test if ($user->rights->batibarrmod->level1->level2)
		$this->rights[$r][5] = 'delete'; // In php code, permission will be checked by test if ($user->rights->batibarrmod->level1->level2)
		$r++;
		/* END MODULEBUILDER PERMISSIONS */

		// Main menu entries to add
		$this->menu = array();
		$r = 0;
		// Add here entries to declare new menus
		/* BEGIN MODULEBUILDER TOPMENU */
		/*$this->menu[$r++] = array(
			'fk_menu'=>'', // '' if this is a top menu. For left menu, use 'fk_mainmenu=xxx' or 'fk_mainmenu=xxx,fk_leftmenu=yyy' where xxx is mainmenucode and yyy is a leftmenucode
			'type'=>'top', // This is a Top menu entry
			'titre'=>'ModuleBatibarrmodName',
			'mainmenu'=>'batibarrmod',
			'leftmenu'=>'',
			'url'=>'/batibarrmod/batibarrmodindex.php',
			'langs'=>'batibarrmod@batibarrmod', // Lang file to use (without .lang) by module. File must be in langs/code_CODE/ directory.
			'position'=>1000 + $r,
			'enabled'=>'$conf->batibarrmod->enabled', // Define condition to show or hide menu entry. Use '$conf->batibarrmod->enabled' if entry must be visible if module is enabled.
			'perms'=>'1', // Use 'perms'=>'$user->rights->batibarrmod->myobject->read' if you want your menu with a permission rules
			'target'=>'',
			'user'=>2, // 0=Menu for internal users, 1=external users, 2=both
		);*/
		/* END MODULEBUILDER TOPMENU */
		/* BEGIN MODULEBUILDER LEFTMENU MYOBJECT
		$this->menu[$r++]=array(
			'fk_menu'=>'fk_mainmenu=batibarrmod',      // '' if this is a top menu. For left menu, use 'fk_mainmenu=xxx' or 'fk_mainmenu=xxx,fk_leftmenu=yyy' where xxx is mainmenucode and yyy is a leftmenucode
			'type'=>'left',                          // This is a Top menu entry
			'titre'=>'MyObject',
			'mainmenu'=>'batibarrmod',
			'leftmenu'=>'myobject',
			'url'=>'/batibarrmod/batibarrmodindex.php',
			'langs'=>'batibarrmod@batibarrmod',	        // Lang file to use (without .lang) by module. File must be in langs/code_CODE/ directory.
			'position'=>1000+$r,
			'enabled'=>'$conf->batibarrmod->enabled',  // Define condition to show or hide menu entry. Use '$conf->batibarrmod->enabled' if entry must be visible if module is enabled.
			'perms'=>'$user->rights->batibarrmod->myobject->read',			                // Use 'perms'=>'$user->rights->batibarrmod->level1->level2' if you want your menu with a permission rules
			'target'=>'',
			'user'=>2,				                // 0=Menu for internal users, 1=external users, 2=both
		);
		$this->menu[$r++]=array(
			'fk_menu'=>'fk_mainmenu=batibarrmod,fk_leftmenu=myobject',	    // '' if this is a top menu. For left menu, use 'fk_mainmenu=xxx' or 'fk_mainmenu=xxx,fk_leftmenu=yyy' where xxx is mainmenucode and yyy is a leftmenucode
			'type'=>'left',			                // This is a Left menu entry
			'titre'=>'List MyObject',
			'mainmenu'=>'batibarrmod',
			'leftmenu'=>'batibarrmod_myobject_list',
			'url'=>'/batibarrmod/myobject_list.php',
			'langs'=>'batibarrmod@batibarrmod',	        // Lang file to use (without .lang) by module. File must be in langs/code_CODE/ directory.
			'position'=>1000+$r,
			'enabled'=>'$conf->batibarrmod->enabled',  // Define condition to show or hide menu entry. Use '$conf->batibarrmod->enabled' if entry must be visible if module is enabled. Use '$leftmenu==\'system\'' to show if leftmenu system is selected.
			'perms'=>'$user->rights->batibarrmod->myobject->read',			                // Use 'perms'=>'$user->rights->batibarrmod->level1->level2' if you want your menu with a permission rules
			'target'=>'',
			'user'=>2,				                // 0=Menu for internal users, 1=external users, 2=both
		);
		$this->menu[$r++]=array(
			'fk_menu'=>'fk_mainmenu=batibarrmod,fk_leftmenu=myobject',	    // '' if this is a top menu. For left menu, use 'fk_mainmenu=xxx' or 'fk_mainmenu=xxx,fk_leftmenu=yyy' where xxx is mainmenucode and yyy is a leftmenucode
			'type'=>'left',			                // This is a Left menu entry
			'titre'=>'New MyObject',
			'mainmenu'=>'batibarrmod',
			'leftmenu'=>'batibarrmod_myobject_new',
			'url'=>'/batibarrmod/myobject_card.php?action=create',
			'langs'=>'batibarrmod@batibarrmod',	        // Lang file to use (without .lang) by module. File must be in langs/code_CODE/ directory.
			'position'=>1000+$r,
			'enabled'=>'$conf->batibarrmod->enabled',  // Define condition to show or hide menu entry. Use '$conf->batibarrmod->enabled' if entry must be visible if module is enabled. Use '$leftmenu==\'system\'' to show if leftmenu system is selected.
			'perms'=>'$user->rights->batibarrmod->myobject->write',			                // Use 'perms'=>'$user->rights->batibarrmod->level1->level2' if you want your menu with a permission rules
			'target'=>'',
			'user'=>2,				                // 0=Menu for internal users, 1=external users, 2=both
		);
		END MODULEBUILDER LEFTMENU MYOBJECT */

		// Exports profiles provided by this module
		$r = 1;
		/* BEGIN MODULEBUILDER EXPORT MYOBJECT */
		/*
		$langs->load("batibarrmod@batibarrmod");
		$this->export_code[$r]=$this->rights_class.'_'.$r;
		$this->export_label[$r]='MyObjectLines';	// Translation key (used only if key ExportDataset_xxx_z not found)
		$this->export_icon[$r]='myobject@batibarrmod';
		// Define $this->export_fields_array, $this->export_TypeFields_array and $this->export_entities_array
		$keyforclass = 'MyObject'; $keyforclassfile='/batibarrmod/class/myobject.class.php'; $keyforelement='myobject@batibarrmod';
		include DOL_DOCUMENT_ROOT.'/core/commonfieldsinexport.inc.php';
		//$this->export_fields_array[$r]['t.fieldtoadd']='FieldToAdd'; $this->export_TypeFields_array[$r]['t.fieldtoadd']='Text';
		//unset($this->export_fields_array[$r]['t.fieldtoremove']);
		//$keyforclass = 'MyObjectLine'; $keyforclassfile='/batibarrmod/class/myobject.class.php'; $keyforelement='myobjectline@batibarrmod'; $keyforalias='tl';
		//include DOL_DOCUMENT_ROOT.'/core/commonfieldsinexport.inc.php';
		$keyforselect='myobject'; $keyforaliasextra='extra'; $keyforelement='myobject@batibarrmod';
		include DOL_DOCUMENT_ROOT.'/core/extrafieldsinexport.inc.php';
		//$keyforselect='myobjectline'; $keyforaliasextra='extraline'; $keyforelement='myobjectline@batibarrmod';
		//include DOL_DOCUMENT_ROOT.'/core/extrafieldsinexport.inc.php';
		//$this->export_dependencies_array[$r] = array('myobjectline'=>array('tl.rowid','tl.ref')); // To force to activate one or several fields if we select some fields that need same (like to select a unique key if we ask a field of a child to avoid the DISTINCT to discard them, or for computed field than need several other fields)
		//$this->export_special_array[$r] = array('t.field'=>'...');
		//$this->export_examplevalues_array[$r] = array('t.field'=>'Example');
		//$this->export_help_array[$r] = array('t.field'=>'FieldDescHelp');
		$this->export_sql_start[$r]='SELECT DISTINCT ';
		$this->export_sql_end[$r]  =' FROM '.MAIN_DB_PREFIX.'myobject as t';
		//$this->export_sql_end[$r]  =' LEFT JOIN '.MAIN_DB_PREFIX.'myobject_line as tl ON tl.fk_myobject = t.rowid';
		$this->export_sql_end[$r] .=' WHERE 1 = 1';
		$this->export_sql_end[$r] .=' AND t.entity IN ('.getEntity('myobject').')';
		$r++; */
		/* END MODULEBUILDER EXPORT MYOBJECT */

		// Imports profiles provided by this module
		$r = 1;
		/* BEGIN MODULEBUILDER IMPORT MYOBJECT */
		/*
		 $langs->load("batibarrmod@batibarrmod");
		 $this->export_code[$r]=$this->rights_class.'_'.$r;
		 $this->export_label[$r]='MyObjectLines';	// Translation key (used only if key ExportDataset_xxx_z not found)
		 $this->export_icon[$r]='myobject@batibarrmod';
		 $keyforclass = 'MyObject'; $keyforclassfile='/batibarrmod/class/myobject.class.php'; $keyforelement='myobject@batibarrmod';
		 include DOL_DOCUMENT_ROOT.'/core/commonfieldsinexport.inc.php';
		 $keyforselect='myobject'; $keyforaliasextra='extra'; $keyforelement='myobject@batibarrmod';
		 include DOL_DOCUMENT_ROOT.'/core/extrafieldsinexport.inc.php';
		 //$this->export_dependencies_array[$r]=array('mysubobject'=>'ts.rowid', 't.myfield'=>array('t.myfield2','t.myfield3')); // To force to activate one or several fields if we select some fields that need same (like to select a unique key if we ask a field of a child to avoid the DISTINCT to discard them, or for computed field than need several other fields)
		 $this->export_sql_start[$r]='SELECT DISTINCT ';
		 $this->export_sql_end[$r]  =' FROM '.MAIN_DB_PREFIX.'myobject as t';
		 $this->export_sql_end[$r] .=' WHERE 1 = 1';
		 $this->export_sql_end[$r] .=' AND t.entity IN ('.getEntity('myobject').')';
		 $r++; */
		/* END MODULEBUILDER IMPORT MYOBJECT */
	}

	/**
	 *  Function called when module is enabled.
	 *  The init function add constants, boxes, permissions and menus (defined in constructor) into Dolibarr database.
	 *  It also creates data directories
	 *
	 *  @param      string  $options    Options when enabling module ('', 'noboxes')
	 *  @return     int             	1 if OK, 0 if KO
	 */
	public function init($options = '')
	{
		global $conf, $langs;

		$result = $this->_load_tables('/batibarrmod/sql/');
		if ($result < 0) return -1; // Do not activate module if error 'not allowed' returned when loading module SQL queries (the _load_table run sql with run_sql with the error allowed parameter set to 'default')

		// Create extrafields during init
		include_once DOL_DOCUMENT_ROOT.'/core/class/extrafields.class.php';
		$extrafields = new ExtraFields($this->db);
		$TList_extra = array();

		$t = array();
		$t['attrname'] = 'ba_support' ;
		$t['label'] = 'Support' ;
		$t['type'] = 'sellist' ;
		$t['pos'] = '100' ;
		$t['size'] = '' ;
		$t['elementtype'] = 'product' ;
		$t['unique'] = '0' ;
		$t['required'] = '1' ;
		$t['default_value'] = '' ;
		$t['param'] = 'a:1:{s:7:"options";a:1:{s:33:"c_ba_support:label:code::active=1";N;}}' ;
		$t['alwayseditable'] = '0' ;
		$t['perms'] = '' ;
		$t['list'] = '1' ;
		$t['help'] = '' ;
		$t['computed'] = '' ;
		$t['entity'] = '1' ;
		$t['langfile'] = '' ;
		$t['enabled'] = '1' ;
		$t['totalizable'] = '0' ;
		$t['printable'] = '0' ;
		$TList_extra[] = $t ;

		$t = array();
		$t['attrname'] = 'ba_support' ;
		$t['label'] = 'Support' ;
		$t['type'] = 'sellist' ;
		$t['pos'] = '100' ;
		$t['size'] = '' ;
		$t['elementtype'] = 'projet' ;
		$t['unique'] = '0' ;
		$t['required'] = '0' ;
		$t['default_value'] = '' ;
		$t['param'] = 'a:1:{s:7:"options";a:1:{s:33:"c_ba_support:label:code::active=1";N;}}' ;
		$t['alwayseditable'] = '0' ;
		$t['perms'] = '' ;
		$t['list'] = '1' ;
		$t['help'] = '' ;
		$t['computed'] = '' ;
		$t['entity'] = '1' ;
		$t['langfile'] = '' ;
		$t['enabled'] = '1' ;
		$t['totalizable'] = '0' ;
		$t['printable'] = '0' ;
		$TList_extra[] = $t ;

		$t = array();
		$t['attrname'] = 'ba_complement' ;
		$t['label'] = 'Complément' ;
		$t['type'] = 'sellist' ;
		$t['pos'] = '110' ;
		$t['size'] = '' ;
		$t['elementtype'] = 'projet' ;
		$t['unique'] = '0' ;
		$t['required'] = '0' ;
		$t['default_value'] = '' ;
		$t['param'] = 'a:1:{s:7:"options";a:1:{s:36:"c_ba_complement:label:code::active=1";N;}}' ;
		$t['alwayseditable'] = '0' ;
		$t['perms'] = '' ;
		$t['list'] = '1' ;
		$t['help'] = '' ;
		$t['computed'] = '' ;
		$t['entity'] = '1' ;
		$t['langfile'] = '' ;
		$t['enabled'] = '1' ;
		$t['totalizable'] = '0' ;
		$t['printable'] = '0' ;
		$TList_extra[] = $t ;

		$t = array();
		$t['attrname'] = 'demand_reason' ;
		$t['label'] = 'Origine' ;
		$t['type'] = 'sellist' ;
		$t['pos'] = '120' ;
		$t['size'] = '' ;
		$t['elementtype'] = 'projet' ;
		$t['unique'] = '0' ;
		$t['required'] = '0' ;
		$t['default_value'] = '' ;
		$t['param'] = 'a:1:{s:7:"options";a:1:{s:36:"c_input_reason:label:rowid::active=1";N;}}' ;
		$t['alwayseditable'] = '0' ;
		$t['perms'] = '' ;
		$t['list'] = '1' ;
		$t['help'] = '' ;
		$t['computed'] = '' ;
		$t['entity'] = '1' ;
		$t['langfile'] = '' ;
		$t['enabled'] = '1' ;
		$t['totalizable'] = '0' ;
		$t['printable'] = '0' ;
		$TList_extra[] = $t ;

		$t = array();
		$t['attrname'] = 'type_contrat' ;
		$t['label'] = 'Niveau de prestation' ;
		$t['type'] = 'select' ;
		$t['pos'] = '130' ;
		$t['size'] = '' ;
		$t['elementtype'] = 'projet' ;
		$t['unique'] = '0' ;
		$t['required'] = '0' ;
		$t['default_value'] = '' ;
		$t['param'] = 'a:1:{s:7:"options";a:7:{i:1;s:9:"Annonceur";i:2;s:10:"Partenaire";i:3;s:18:"Partenaire premium";i:4;s:5:"-----";i:5;s:8:"Exposant";i:6;s:8:"Start up";i:7;s:7:"Sponsor";}}' ;
		$t['alwayseditable'] = '0' ;
		$t['perms'] = '' ;
		$t['list'] = '1' ;
		$t['help'] = '' ;
		$t['computed'] = '' ;
		$t['entity'] = '1' ;
		$t['langfile'] = '' ;
		$t['enabled'] = '1' ;
		$t['totalizable'] = '0' ;
		$t['printable'] = '0' ;
		$TList_extra[] = $t ;

		$t = array();
		$t['attrname'] = 'tacite_reconduction' ;
		$t['label'] = 'Tacite reconduction' ;
		$t['type'] = 'select' ;
		$t['pos'] = '140' ;
		$t['size'] = '' ;
		$t['elementtype'] = 'projet' ;
		$t['unique'] = '0' ;
		$t['required'] = '0' ;
		$t['default_value'] = '' ;
		$t['param'] = 'a:1:{s:7:"options";a:3:{i:1;s:3:"Non";i:2;s:12:"Oui nouvelle";i:3;s:18:"Oui renouvellement";}}' ;
		$t['alwayseditable'] = '0' ;
		$t['perms'] = '' ;
		$t['list'] = '1' ;
		$t['help'] = '' ;
		$t['computed'] = '' ;
		$t['entity'] = '1' ;
		$t['langfile'] = '' ;
		$t['enabled'] = '1' ;
		$t['totalizable'] = '0' ;
		$t['printable'] = '0' ;
		$TList_extra[] = $t ;

		$t = array();
		$t['attrname'] = 'motif_perdu' ;
		$t['label'] = 'Motif d\'affaire perdu' ;
		$t['type'] = 'sellist' ;
		$t['pos'] = '150' ;
		$t['size'] = '' ;
		$t['elementtype'] = 'projet' ;
		$t['unique'] = '0' ;
		$t['required'] = '0' ;
		$t['default_value'] = '' ;
		$t['param'] = 'a:1:{s:7:"options";a:1:{s:37:"c_ba_motif_perdu:label:code::active=1";N;}}' ;
		$t['alwayseditable'] = '0' ;
		$t['perms'] = '' ;
		$t['list'] = '(($object->opp_status==7||!isset($object->opp_status))?1:0)' ;
		$t['help'] = '' ;
		$t['computed'] = '' ;
		$t['entity'] = '1' ;
		$t['langfile'] = '' ;
		$t['enabled'] = '1' ;
		$t['totalizable'] = '0' ;
		$t['printable'] = '0' ;
		$TList_extra[] = $t ;

		$t = array();
		$t['attrname'] = 'signe_auto' ;
		$t['label'] = 'Signé' ;
		$t['type'] = 'int' ;
		$t['pos'] = '195' ;
		$t['size'] = '10' ;
		$t['elementtype'] = 'projet' ;
		$t['unique'] = '0' ;
		$t['required'] = '0' ;
		$t['default_value'] = '0' ;
		$t['param'] = 'a:1:{s:7:"options";a:1:{s:0:"";N;}}' ;
		$t['alwayseditable'] = '0' ;
		$t['perms'] = '' ;
		$t['list'] = '0' ;
		$t['help'] = '' ;
		$t['computed'] = '' ;
		$t['entity'] = '1' ;
		$t['langfile'] = '' ;
		$t['enabled'] = '1' ;
		$t['totalizable'] = '0' ;
		$t['printable'] = '0' ;
		$TList_extra[] = $t ;

		$t = array();
		$t['attrname'] = 'amount_real' ;
		$t['label'] = 'Montant réel de l\'affaire' ;
		$t['type'] = 'price' ;
		$t['pos'] = '200' ;
		$t['size'] = '' ;
		$t['elementtype'] = 'projet' ;
		$t['unique'] = '0' ;
		$t['required'] = '0' ;
		$t['default_value'] = '' ;
		$t['param'] = 'a:1:{s:7:"options";a:1:{s:0:"";N;}}' ;
		$t['alwayseditable'] = '0' ;
		$t['perms'] = '' ;
		$t['list'] = '(($object->opp_status==6||!isset($object->opp_status))?1:0)' ;
		$t['help'] = 'Montant total réel de la proposition validée' ;
		$t['computed'] = '' ;
		$t['entity'] = '1' ;
		$t['langfile'] = '' ;
		$t['enabled'] = '1' ;
		$t['totalizable'] = '0' ;
		$t['printable'] = '0' ;
		$TList_extra[] = $t ;

		$t = array();
		$t['attrname'] = 'date_signe' ;
		$t['label'] = 'Date de signature' ;
		$t['type'] = 'date' ;
		$t['pos'] = '210' ;
		$t['size'] = '' ;
		$t['elementtype'] = 'projet' ;
		$t['unique'] = '0' ;
		$t['required'] = '0' ;
		$t['default_value'] = '' ;
		$t['param'] = 'a:1:{s:7:"options";a:1:{s:0:"";N;}}' ;
		$t['alwayseditable'] = '0' ;
		$t['perms'] = '' ;
		$t['list'] = '(($object->opp_status==6||!isset($object->opp_status))?1:0)' ;
		$t['help'] = '' ;
		$t['computed'] = '' ;
		$t['entity'] = '1' ;
		$t['langfile'] = '' ;
		$t['enabled'] = '1' ;
		$t['totalizable'] = '0' ;
		$t['printable'] = '0' ;
		$TList_extra[] = $t ;

		$t = array();
		$t['attrname'] = 'echeancier' ;
		$t['label'] = 'Echéancier de facturation' ;
		$t['type'] = 'text' ;
		$t['pos'] = '220' ;
		$t['size'] = '2000' ;
		$t['elementtype'] = 'projet' ;
		$t['unique'] = '0' ;
		$t['required'] = '0' ;
		$t['default_value'] = '' ;
		$t['param'] = 'a:1:{s:7:"options";a:1:{s:0:"";N;}}' ;
		$t['alwayseditable'] = '0' ;
		$t['perms'] = '' ;
		$t['list'] = '(($object->opp_status==6||!isset($object->opp_status))?1:0)' ;
		$t['help'] = '' ;
		$t['computed'] = '' ;
		$t['entity'] = '1' ;
		$t['langfile'] = '' ;
		$t['enabled'] = '1' ;
		$t['totalizable'] = '0' ;
		$t['printable'] = '0' ;
		$TList_extra[] = $t ;

		$t = array();
		$t['attrname'] = 'ca_annee' ;
		$t['label'] = 'CA facturé sur' ;
		$t['type'] = 'varchar' ;
		$t['pos'] = '230' ;
		$t['size'] = '255' ;
		$t['elementtype'] = 'projet' ;
		$t['unique'] = '0' ;
		$t['required'] = '0' ;
		$t['default_value'] = '' ;
		$t['param'] = 'a:1:{s:7:"options";a:1:{s:0:"";N;}}' ;
		$t['alwayseditable'] = '0' ;
		$t['perms'] = '' ;
		$t['list'] = '(($object->opp_status==6||!isset($object->opp_status))?1:0)' ;
		$t['help'] = '' ;
		$t['computed'] = '' ;
		$t['entity'] = '1' ;
		$t['langfile'] = '' ;
		$t['enabled'] = '1' ;
		$t['totalizable'] = '0' ;
		$t['printable'] = '0' ;
		$TList_extra[] = $t ;

		$t = array();
		$t['attrname'] = 'duree_contrat' ;
		$t['label'] = 'Durée de contrat en mois' ;
		$t['type'] = 'varchar' ;
		$t['pos'] = '240' ;
		$t['size'] = '10' ;
		$t['elementtype'] = 'projet' ;
		$t['unique'] = '0' ;
		$t['required'] = '0' ;
		$t['default_value'] = '' ;
		$t['param'] = 'a:1:{s:7:"options";a:1:{s:0:"";N;}}' ;
		$t['alwayseditable'] = '0' ;
		$t['perms'] = '' ;
		$t['list'] = '(($object->opp_status==6||!isset($object->opp_status))?1:0)' ;
		$t['help'] = '' ;
		$t['computed'] = '' ;
		$t['entity'] = '1' ;
		$t['langfile'] = '' ;
		$t['enabled'] = '1' ;
		$t['totalizable'] = '0' ;
		$t['printable'] = '0' ;
		$TList_extra[] = $t ;

		$t = array();
		$t['attrname'] = 'date_fin_contrat' ;
		$t['label'] = 'Date de fin de contrat' ;
		$t['type'] = 'date' ;
		$t['pos'] = '250' ;
		$t['size'] = '' ;
		$t['elementtype'] = 'projet' ;
		$t['unique'] = '0' ;
		$t['required'] = '0' ;
		$t['default_value'] = '' ;
		$t['param'] = 'a:1:{s:7:"options";a:1:{s:0:"";N;}}' ;
		$t['alwayseditable'] = '0' ;
		$t['perms'] = '' ;
		$t['list'] = '(($object->opp_status==6||!isset($object->opp_status))?1:0)' ;
		$t['help'] = '' ;
		$t['computed'] = '' ;
		$t['entity'] = '1' ;
		$t['langfile'] = '' ;
		$t['enabled'] = '1' ;
		$t['totalizable'] = '0' ;
		$t['printable'] = '0' ;
		$TList_extra[] = $t ;

		$t = array();
		$t['attrname'] = 'echeancier' ;
		$t['label'] = 'Echéancier de facturation' ;
		$t['type'] = 'text' ;
		$t['pos'] = '100' ;
		$t['size'] = '2000' ;
		$t['elementtype'] = 'propal' ;
		$t['unique'] = '0' ;
		$t['required'] = '0' ;
		$t['default_value'] = '2' ;
		$t['param'] = 'a:1:{s:7:"options";a:1:{s:0:"";N;}}' ;
		$t['alwayseditable'] = '0' ;
		$t['perms'] = '' ;
		$t['list'] = '1' ;
		$t['help'] = '' ;
		$t['computed'] = '' ;
		$t['entity'] = '1' ;
		$t['langfile'] = '' ;
		$t['enabled'] = '1' ;
		$t['totalizable'] = '0' ;
		$t['printable'] = '0' ;
		$TList_extra[] = $t ;

		$t = array();
		$t['attrname'] = 'ba_support' ;
		$t['label'] = 'Support' ;
		$t['type'] = 'sellist' ;
		$t['pos'] = '120' ;
		$t['size'] = '' ;
		$t['elementtype'] = 'propal' ;
		$t['unique'] = '0' ;
		$t['required'] = '0' ;
		$t['default_value'] = '' ;
		$t['param'] = 'a:1:{s:7:"options";a:1:{s:33:"c_ba_support:label:code::active=1";N;}}' ;
		$t['alwayseditable'] = '0' ;
		$t['perms'] = '' ;
		$t['list'] = '1' ;
		$t['help'] = '' ;
		$t['computed'] = '' ;
		$t['entity'] = '1' ;
		$t['langfile'] = '' ;
		$t['enabled'] = '1' ;
		$t['totalizable'] = '0' ;
		$t['printable'] = '0' ;
		$TList_extra[] = $t ;

		$t = array();
		$t['attrname'] = 'date_signe' ;
		$t['label'] = 'Date de signature' ;
		$t['type'] = 'date' ;
		$t['pos'] = '200' ;
		$t['size'] = '' ;
		$t['elementtype'] = 'propal' ;
		$t['unique'] = '0' ;
		$t['required'] = '0' ;
		$t['default_value'] = '' ;
		$t['param'] = 'a:1:{s:7:"options";a:1:{s:0:"";N;}}' ;
		$t['alwayseditable'] = '0' ;
		$t['perms'] = '' ;
		$t['list'] = '($object->statut==$object::STATUS_SIGNED?1:0)' ;
		$t['help'] = '' ;
		$t['computed'] = '' ;
		$t['entity'] = '1' ;
		$t['langfile'] = '' ;
		$t['enabled'] = '1' ;
		$t['totalizable'] = '0' ;
		$t['printable'] = '0' ;
		$TList_extra[] = $t ;

		$t = array();
		$t['attrname'] = 'npai' ;
		$t['label'] = 'NPAI' ;
		$t['type'] = 'select' ;
		$t['pos'] = '120' ;
		$t['size'] = '' ;
		$t['elementtype'] = 'societe' ;
		$t['unique'] = '0' ;
		$t['required'] = '0' ;
		$t['default_value'] = '1' ;
		$t['param'] = 'a:1:{s:7:"options";a:2:{i:1;s:3:"Non";i:2;s:3:"Oui";}}' ;
		$t['alwayseditable'] = '0' ;
		$t['perms'] = '' ;
		$t['list'] = '1' ;
		$t['help'] = '' ;
		$t['computed'] = '' ;
		$t['entity'] = '1' ;
		$t['langfile'] = '' ;
		$t['enabled'] = '1' ;
		$t['totalizable'] = '0' ;
		$t['printable'] = '0' ;
		$TList_extra[] = $t ;

		$t = array();
		$t['attrname'] = 'contentieux' ;
		$t['label'] = 'Contentieux' ;
		$t['type'] = 'select' ;
		$t['pos'] = '140' ;
		$t['size'] = '' ;
		$t['elementtype'] = 'societe' ;
		$t['unique'] = '0' ;
		$t['required'] = '0' ;
		$t['default_value'] = '1' ;
		$t['param'] = 'a:1:{s:7:"options";a:2:{i:1;s:3:"Non";i:2;s:3:"Oui";}}' ;
		$t['alwayseditable'] = '0' ;
		$t['perms'] = '' ;
		$t['list'] = '1' ;
		$t['help'] = '' ;
		$t['computed'] = '' ;
		$t['entity'] = '1' ;
		$t['langfile'] = '' ;
		$t['enabled'] = '1' ;
		$t['totalizable'] = '0' ;
		$t['printable'] = '0' ;
		$TList_extra[] = $t ;

		$t = array();
		$t['attrname'] = 'comcontentieux' ;
		$t['label'] = 'Commentaire sur le contentieux' ;
		$t['type'] = 'text' ;
		$t['pos'] = '150' ;
		$t['size'] = '200' ;
		$t['elementtype'] = 'societe' ;
		$t['unique'] = '0' ;
		$t['required'] = '0' ;
		$t['default_value'] = '' ;
		$t['param'] = 'a:1:{s:7:"options";a:1:{s:0:"";N;}}' ;
		$t['alwayseditable'] = '0' ;
		$t['perms'] = '' ;
		$t['list'] = '1' ;
		$t['help'] = '' ;
		$t['computed'] = '' ;
		$t['entity'] = '1' ;
		$t['langfile'] = '' ;
		$t['enabled'] = '1' ;
		$t['totalizable'] = '0' ;
		$t['printable'] = '0' ;
		$TList_extra[] = $t ;

		$t = array();
		$t['attrname'] = 'ba_support' ;
		$t['label'] = 'Client de' ;
		$t['type'] = 'chkbxlst' ;
		$t['pos'] = '160' ;
		$t['size'] = '' ;
		$t['elementtype'] = 'societe' ;
		$t['unique'] = '0' ;
		$t['required'] = '0' ;
		$t['default_value'] = '' ;
		$t['param'] = 'a:1:{s:7:"options";a:1:{s:33:"c_ba_support:label:code::active=1";N;}}' ;
		$t['alwayseditable'] = '0' ;
		$t['perms'] = '' ;
		$t['list'] = '1' ;
		$t['help'] = '' ;
		$t['computed'] = '' ;
		$t['entity'] = '1' ;
		$t['langfile'] = '' ;
		$t['enabled'] = '1' ;
		$t['totalizable'] = '0' ;
		$t['printable'] = '0' ;
		$TList_extra[] = $t ;

		$t = array();
		$t['attrname'] = 'ba_not_support' ;
		$t['label'] = 'Non client de' ;
		$t['type'] = 'chkbxlst' ;
		$t['pos'] = '165' ;
		$t['size'] = '' ;
		$t['elementtype'] = 'societe' ;
		$t['unique'] = '0' ;
		$t['required'] = '0' ;
		$t['default_value'] = '' ;
		$t['param'] = 'a:1:{s:7:"options";a:1:{s:33:"c_ba_support:label:code::active=1";N;}}' ;
		$t['alwayseditable'] = '0' ;
		$t['perms'] = '' ;
		$t['list'] = '1' ;
		$t['help'] = '' ;
		$t['computed'] = '' ;
		$t['entity'] = '1' ;
		$t['langfile'] = '' ;
		$t['enabled'] = '1' ;
		$t['totalizable'] = '0' ;
		$t['printable'] = '0' ;
		$TList_extra[] = $t ;

		$t = array();
		$t['attrname'] = 'note_dossier' ;
		$t['label'] = 'Note dossier' ;
		$t['type'] = 'text' ;
		$t['pos'] = '170' ;
		$t['size'] = '600' ;
		$t['elementtype'] = 'societe' ;
		$t['unique'] = '0' ;
		$t['required'] = '0' ;
		$t['default_value'] = '' ;
		$t['param'] = 'a:1:{s:7:"options";a:1:{s:0:"";N;}}' ;
		$t['alwayseditable'] = '0' ;
		$t['perms'] = '' ;
		$t['list'] = '1' ;
		$t['help'] = '' ;
		$t['computed'] = '' ;
		$t['entity'] = '1' ;
		$t['langfile'] = '' ;
		$t['enabled'] = '1' ;
		$t['totalizable'] = '0' ;
		$t['printable'] = '0' ;
		$TList_extra[] = $t ;

		$t = array();
		$t['attrname'] = 'ideudonet' ;
		$t['label'] = 'ID Eudonet' ;
		$t['type'] = 'varchar' ;
		$t['pos'] = '100' ;
		$t['size'] = '20' ;
		$t['elementtype'] = 'socpeople' ;
		$t['unique'] = '0' ;
		$t['required'] = '0' ;
		$t['default_value'] = '' ;
		$t['param'] = 'a:1:{s:7:"options";a:1:{s:0:"";N;}}' ;
		$t['alwayseditable'] = '0' ;
		$t['perms'] = '' ;
		$t['list'] = '0' ;
		$t['help'] = '' ;
		$t['computed'] = '' ;
		$t['entity'] = '1' ;
		$t['langfile'] = '' ;
		$t['enabled'] = '1' ;
		$t['totalizable'] = '0' ;
		$t['printable'] = '0' ;
		$TList_extra[] = $t ;

		$t = array();
		$t['attrname'] = 'ba_fonction' ;
		$t['label'] = 'Fonction' ;
		$t['type'] = 'sellist' ;
		$t['pos'] = '110' ;
		$t['size'] = '' ;
		$t['elementtype'] = 'socpeople' ;
		$t['unique'] = '0' ;
		$t['required'] = '0' ;
		$t['default_value'] = '' ;
		$t['param'] = 'a:1:{s:7:"options";a:1:{s:34:"c_ba_fonction:label:code::active=1";N;}}' ;
		$t['alwayseditable'] = '0' ;
		$t['perms'] = '' ;
		$t['list'] = '1' ;
		$t['help'] = '' ;
		$t['computed'] = '' ;
		$t['entity'] = '1' ;
		$t['langfile'] = '' ;
		$t['enabled'] = '1' ;
		$t['totalizable'] = '0' ;
		$t['printable'] = '0' ;
		$TList_extra[] = $t ;

		$t = array();
		$t['attrname'] = 'ba_contact_tag' ;
		$t['label'] = 'Contact principal pour' ;
		$t['type'] = 'chkbxlst' ;
		$t['pos'] = '120' ;
		$t['size'] = '' ;
		$t['elementtype'] = 'socpeople' ;
		$t['unique'] = '0' ;
		$t['required'] = '0' ;
		$t['default_value'] = '' ;
		$t['param'] = 'a:1:{s:7:"options";a:1:{s:33:"c_ba_contact:label:code::active=1";N;}}' ;
		$t['alwayseditable'] = '0' ;
		$t['perms'] = '' ;
		$t['list'] = '1' ;
		$t['help'] = '' ;
		$t['computed'] = '' ;
		$t['entity'] = '1' ;
		$t['langfile'] = '' ;
		$t['enabled'] = '1' ;
		$t['totalizable'] = '0' ;
		$t['printable'] = '0' ;
		$TList_extra[] = $t ;

		$t = array();
		$t['attrname'] = 'mail2' ;
		$t['label'] = 'E-mail 2' ;
		$t['type'] = 'varchar' ;
		$t['pos'] = '50' ;
		$t['size'] = '255' ;
		$t['elementtype'] = 'socpeople' ;
		$t['unique'] = '0' ;
		$t['required'] = '0' ;
		$t['default_value'] = '' ;
		$t['param'] = 'a:1:{s:7:"options";a:1:{s:0:"";N;}}' ;
		$t['alwayseditable'] = '0' ;
		$t['perms'] = '' ;
		$t['list'] = '1' ;
		$t['help'] = '' ;
		$t['computed'] = '' ;
		$t['entity'] = '1' ;
		$t['langfile'] = '' ;
		$t['enabled'] = '1' ;
		$t['totalizable'] = '0' ;
		$t['printable'] = '0' ;
		$TList_extra[] = $t ;

		foreach ($TList_extra as $TExtra) {
			$result1 = $extrafields->addExtraField($TExtra['attrname'], $TExtra['label'], $TExtra['type'], $TExtra['pos'],  $TExtra['size'], $TExtra['elementtype'], $TExtra['unique'], $TExtra['required'], $TExtra['default_value'], $TExtra['param'], $TExtra['alwayseditable'], $TExtra['perms'], $TExtra['list'], $TExtra['help'], $TExtra['computed'], $TExtra['entity'], $TExtra['langfile'], $TExtra['enabled'], $TExtra['totalizable'], $TExtra['printable']);
		}

		//$result1=$extrafields->addExtraField('batibarrmod_myattr1', "New Attr 1 label", 'boolean', 1,  3, 'thirdparty',   0, 0, '', '', 1, '', 0, 0, '', '', 'batibarrmod@batibarrmod', '$conf->batibarrmod->enabled');
		//$result2=$extrafields->addExtraField('batibarrmod_myattr2', "New Attr 2 label", 'varchar', 1, 10, 'project',      0, 0, '', '', 1, '', 0, 0, '', '', 'batibarrmod@batibarrmod', '$conf->batibarrmod->enabled');
		//$result3=$extrafields->addExtraField('batibarrmod_myattr3', "New Attr 3 label", 'varchar', 1, 10, 'bank_account', 0, 0, '', '', 1, '', 0, 0, '', '', 'batibarrmod@batibarrmod', '$conf->batibarrmod->enabled');
		//$result4=$extrafields->addExtraField('batibarrmod_myattr4', "New Attr 4 label", 'select',  1,  3, 'thirdparty',   0, 1, '', array('options'=>array('code1'=>'Val1','code2'=>'Val2','code3'=>'Val3')), 1,'', 0, 0, '', '', 'batibarrmod@batibarrmod', '$conf->batibarrmod->enabled');
		//$result5=$extrafields->addExtraField('batibarrmod_myattr5', "New Attr 5 label", 'text',    1, 10, 'user',         0, 0, '', '', 1, '', 0, 0, '', '', 'batibarrmod@batibarrmod', '$conf->batibarrmod->enabled');

		// Permissions
		$this->remove($options);

		// Gestions des liens du menu en bdd
		$this->add_link_menu('&amp;sortfield=a.datep,a.id&amp;sortorder=desc,desc', '%list.php?action=show_list%mainmenu=agenda%status=todo%', 'agenda', 'Menu%');
		$this->add_link_menu('&amp;sortfield=a.datep,a.id&amp;sortorder=desc,desc', '%list.php?action=show_list%mainmenu=agenda%status=done%', 'agenda', 'Menu%');
		$this->add_link_menu('&amp;sortfield=a.datep,a.id&amp;sortorder=desc,desc', '%list.php?action=show_list%mainmenu=agenda%', 'agenda', 'List%');
		$this->add_link_menu('?sortfield=a.datep,a.id&amp;sortorder=desc,desc', '/comm/action/index.php', 'agenda', '', 'top');
		//exit() ;

		$sql = array();

		// Document templates
		$moduledir = 'batibarrmod';
		$myTmpObjects = array();
		$myTmpObjects['MyObject']=array('includerefgeneration'=>0, 'includedocgeneration'=>0);

		foreach ($myTmpObjects as $myTmpObjectKey => $myTmpObjectArray) {
			if ($myTmpObjectKey == 'MyObject') continue;
			if ($myTmpObjectArray['includerefgeneration']) {
				$src=DOL_DOCUMENT_ROOT.'/install/doctemplates/batibarrmod/template_myobjects.odt';
				$dirodt=DOL_DATA_ROOT.'/doctemplates/batibarrmod';
				$dest=$dirodt.'/template_myobjects.odt';

				if (file_exists($src) && ! file_exists($dest))
				{
					require_once DOL_DOCUMENT_ROOT.'/core/lib/files.lib.php';
					dol_mkdir($dirodt);
					$result=dol_copy($src, $dest, 0, 0);
					if ($result < 0)
					{
						$langs->load("errors");
						$this->error=$langs->trans('ErrorFailToCopyFile', $src, $dest);
						return 0;
					}
				}

				$sql = array_merge($sql, array(
					"DELETE FROM ".MAIN_DB_PREFIX."document_model WHERE nom = 'standard_".strtolower($myTmpObjectKey)."' AND type = '".strtolower($myTmpObjectKey)."' AND entity = ".$conf->entity,
					"INSERT INTO ".MAIN_DB_PREFIX."document_model (nom, type, entity) VALUES('standard_".strtolower($myTmpObjectKey)."','".strtolower($myTmpObjectKey)."',".$conf->entity.")",
					"DELETE FROM ".MAIN_DB_PREFIX."document_model WHERE nom = 'generic_".strtolower($myTmpObjectKey)."_odt' AND type = '".strtolower($myTmpObjectKey)."' AND entity = ".$conf->entity,
					"INSERT INTO ".MAIN_DB_PREFIX."document_model (nom, type, entity) VALUES('generic_".strtolower($myTmpObjectKey)."_odt', '".strtolower($myTmpObjectKey)."', ".$conf->entity.")"
				));
			}
		}

		$sql = array_merge($sql, array(
			"DELETE FROM ".MAIN_DB_PREFIX."document_model WHERE nom= 'batiactu' AND type = 'propal' AND entity = ".$conf->entity,
			"INSERT INTO ".MAIN_DB_PREFIX."document_model (nom, type, entity, libelle) VALUES('batiactu','propal',".$conf->entity.", 'PDF 1 proposition générique') ON DUPLICATE KEY UPDATE `nom` = 'batiactu'",

			"DELETE FROM ".MAIN_DB_PREFIX."const WHERE name = 'PROPALE_ADDON_PDF' AND entity = ".$conf->entity,
			"INSERT INTO ".MAIN_DB_PREFIX."const (name, value, type, visible, note, entity) VALUES('PROPALE_ADDON_PDF', 'batiactu','chaine', 1, 'PDF 1 proposition générique',".$conf->entity.")",

			"DELETE FROM ".MAIN_DB_PREFIX."document_model WHERE nom= 'batiactu_en' AND type = 'propal' AND entity = ".$conf->entity,
			"INSERT INTO ".MAIN_DB_PREFIX."document_model (nom, type, entity, libelle) VALUES('batiactu_en','propal',".$conf->entity.", 'PDF 2 proposition english') ON DUPLICATE KEY UPDATE `nom` = 'batiactu_en'",

			"DELETE FROM ".MAIN_DB_PREFIX."const WHERE name = 'PROPALE_ADDON_PDF_EN' AND entity = ".$conf->entity,
			"INSERT INTO ".MAIN_DB_PREFIX."const (name, value, type, visible, note, entity) VALUES('PROPALE_ADDON_PDF_EN', 'batiactu_en','chaine', 1, 'PDF 2 proposition english',".$conf->entity.")",

			"DELETE FROM ".MAIN_DB_PREFIX."document_model WHERE nom= 'batiactu_pf' AND type = 'propal' AND entity = ".$conf->entity,
			"INSERT INTO ".MAIN_DB_PREFIX."document_model (nom, type, entity, libelle) VALUES('batiactu_pf','propal',".$conf->entity.", 'PDF 3 proposition Proforma') ON DUPLICATE KEY UPDATE `nom` = 'batiactu_en'",

			"DELETE FROM ".MAIN_DB_PREFIX."const WHERE name = 'PROPALE_ADDON_PDF_PF' AND entity = ".$conf->entity,
			"INSERT INTO ".MAIN_DB_PREFIX."const (name, value, type, visible, note, entity) VALUES('PROPALE_ADDON_PDF_PF', 'batiactu_pf','chaine', 1, 'PDF 3 proposition Proforma',".$conf->entity.")",

			"DELETE FROM ".MAIN_DB_PREFIX."document_model WHERE nom= 'batiactu_invoice' AND type = 'invoice' AND entity = ".$conf->entity,
			"INSERT INTO ".MAIN_DB_PREFIX."document_model (nom, type, entity, libelle) VALUES('batiactu_invoice','invoice',".$conf->entity.", 'PDF 1 Facture Batiactu Groupe') ON DUPLICATE KEY UPDATE `nom` = 'batiactu_invoice'",

			"DELETE FROM ".MAIN_DB_PREFIX."const WHERE name = 'FACTURE_ADDON_PDF_BA' AND entity = ".$conf->entity,
			"INSERT INTO ".MAIN_DB_PREFIX."const (name, value, type, visible, note, entity) VALUES('FACTURE_ADDON_PDF_BA', 'batiactu_invoice','chaine', 1, 'PDF 1 Facture Batiactu Groupe',".$conf->entity.")",
		));

		$res_init = $this->_init($sql, $options);

		/* GESTION DES GRAPHS PAGE ACCUEIL */
		$TList = array('box_graph_stats_support'=>0, 'box_graph_stats_comm'=>0, 'box_graph_stats_month'=>0) ;
		foreach ($TList as $key=>$ind) {
			$sql_boxe = "SELECT rowid FROM ".MAIN_DB_PREFIX."boxes_def WHERE file LIKE '".$key."%'";
			//echo $sql_boxe."<br />\r\n" ;
			$resql = $this->db->query($sql_boxe);
			if (!$resql) {
				dol_syslog(get_class($this)."::_init Warning ".$this->db->lasterror(), LOG_WARNING);
				//echo "err<br />" ;
			}else {
				$objp = $this->db->fetch_object($resql);
				$TList[$key] = $objp->rowid ;
				//echo "ok ".$objp->rowid." | ".$sql_boxe."<br />" ;
			}
		}

		//echo "<pre>".print_r($TList, TRUE)."</pre><hr />\r\n" ;

		$TUser = array() ;
		$sql_user = "SELECT rowid, login FROM ".MAIN_DB_PREFIX."user ORDER BY rowid ASC";
		//echo $sql_user."<br />\r\n" ;
		$resql = $this->db->query($sql_user);
		if (!$resql) {
			dol_syslog(get_class($this)."::_init Warning ".$this->db->lasterror(), LOG_WARNING);
		}else {
			$num = $this->db->num_rows($resql);
			$ind_u = 0;
			while ($ind_u < $num) {
				$objp = $this->db->fetch_object($resql);
				$TUser[ $objp->rowid ] = $objp->login ;
				$ind_u++ ;
			}
		}

		//echo $sql_user." | ".$num."<br />\r\n<pre>".print_r($TUser, TRUE)."</pre><hr />\r\n" ;

		$sql_user_graph = array() ;
		$TPos = array() ;
		foreach ($TList as $key=>$id_box) {
			if ($id_box > 0) {
				$sql_user_graph[] = "DELETE FROM ".MAIN_DB_PREFIX."boxes WHERE box_id=".$id_box ;
				foreach ($TUser as $id_user=>$login) {
					if (!isset($TPos[$id_user])) $TPos[$id_user] = 1;

					$sql_user_graph[] = "INSERT INTO ".MAIN_DB_PREFIX."boxes "
						."(entity,box_id,position,box_order,fk_user,maxline,params) "
						."VALUES "
						."(".$conf->entity.", ".$id_box.", 0, 'A".str_pad($TPos[$id_user], 2, '0', STR_PAD_LEFT)."', ".$id_user.", NULL, NULL)" ;

					//echo "pos: ".$TPos[$id_user]." | id_box: ".$id_box." | id_user: ".$id_user." | key box: ".$key." | login: ".$login."<br />\r\n" ;
					$TPos[$id_user]++ ;
				}
			}
		}

		//echo "<pre>".print_r($sql_user_graph, TRUE)."</pre><hr />\r\n" ;
		foreach ($sql_user_graph as $sql_l) {
			$resql = $this->db->query($sql_l);
			//echo $sql_l."<br />\r\n" ;
		}

		//$sql = array_merge($sql, $sql_user_graph);
		//exit() ;

		return $res_init ;
	}

	/**
	 *  Function called when module is disabled.
	 *  Remove from database constants, boxes and permissions from Dolibarr database.
	 *  Data directories are not deleted
	 *
	 *  @param      string	$options    Options when enabling module ('', 'noboxes')
	 *  @return     int                 1 if OK, 0 if KO
	 */
	public function remove($options = '')
	{
		$sql = array();
		return $this->_remove($sql, $options);
	}

	private function add_link_menu($add, $filter, $module, $titre, $type='')
	{
		//$sql_search = "SELECT rowid,url FROM llx_menu WHERE module='".$module."' AND titre LIKE 'Menu%' AND url LIKE '".$filter."'";
		$sql_search = "SELECT rowid,url FROM llx_menu ";
		$sql_search.= " WHERE url LIKE '".$filter."'";
		if ($module!='') $sql_search.= " AND module='".$module."'";
		if ($titre!='') $sql_search.= " AND titre LIKE '".$titre."'";
		if ($type!='') $sql_search.= " AND type='".$type."'";
		//echo $sql_search."<br />\r\n" ;
		$resql = $this->db->query($sql_search);
		if (!$resql) {
			 dol_syslog(get_class($this)."::_init Warning ".$this->db->lasterror(), LOG_WARNING);
		}else {
			$numr = $this->db->num_rows($resql);
			$ind = 0;
			$TUpdate = array() ;

			if ($numr > 0) {
				while ($ind < $numr) {
					$objp = $this->db->fetch_object($resql);
					$id_url = $objp->rowid ;
					$url = $objp->url ;

					if (strpos($url, $add)===FALSE) {
						$url.=  $add;
						$TUpdate[] = "UPDATE llx_menu SET url='".$url."' WHERE rowid=".$id_url;
					}
					$ind++;
				}

				foreach ($TUpdate as $sql_up) {
					$this->db->query($sql_up);
					//echo $sql_up."<br />\r\n" ;
				}
			}
		}
	}
}
