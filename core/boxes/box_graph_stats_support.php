<?php
/* Copyright (C) 2013 Laurent Destailleur  <eldy@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 *	\file       htdocs/core/boxes/box_graph_stats_support.php
 *	\ingroup    propales
 *	\brief      Box to show graph of proposals per month
 */
include_once DOL_DOCUMENT_ROOT.'/core/boxes/modules_boxes.php';


/**
 * Class to manage the box to show last propals
 */
class box_graph_stats_support extends ModeleBoxes
{
	public $boxcode = "statssupportflux";
	public $boximg = "object_stat";
	public $boxlabel = "Chiffre d'affaires par support";
	public $depends = array();

	/**
	 * @var DoliDB Database handler.
	 */
	public $db;

	public $info_box_head = array();
	public $info_box_contents = array();


	/**
	 *  Constructor
	 *
	 * 	@param	DoliDB	$db			Database handler
	 *  @param	string	$param		More parameters
	 */
	public function __construct($db, $param)
	{
		global $user;

		$this->db = $db;

		$this->hidden = !($user->rights->propal->lire);
	}

	/**
	 *  Load data into info_box_contents array to show array later.
	 *
	 *  @param	int		$max        Maximum number of records to load
	 *  @return	void
	 */
	public function loadBox($max = 5)
	{
		global $conf, $user, $langs;

		$this->max = $max;

		$refreshaction = 'refresh_'.$this->boxcode;

		$startmonth = $conf->global->SOCIETE_FISCAL_MONTH_START ? ($conf->global->SOCIETE_FISCAL_MONTH_START) : 1;
		if (empty($conf->global->GRAPH_USE_FISCAL_YEAR)) $startmonth = 1;

		//$langs->load("propal");

		$text = "Chiffre d'affaires par support" ;
		//$text = $langs->trans("BoxProposalsPerMonth", $max);
		$this->info_box_head = array(
			'text' => $text,
			'limit'=> dol_strlen($text),
			'graph'=> 1, // Set to 1 if it's a box graph
			'sublink'=>'',
			'subtext'=>$langs->trans("Filter"),
			'subpicto'=>'filter.png',
			'subclass'=>'linkobject boxfilter',
			'target'=>'none'	// Set '' to get target="_blank"
		);

		$dir = ''; // We don't need a path because image file will not be saved into disk
		$prefix = '';
		$socid = 0;
		if ($user->socid) $socid = $user->socid;
		if (!$user->rights->societe->client->voir || $socid) $prefix .= 'private-'.$user->id.'-'; // If user has no permission to see all, output dir is specific to user

		if ($user->rights->propal->lire)
		{
			$param_year = 'DOLUSERCOOKIE_box_'.$this->boxcode.'_year';

			include_once DOL_DOCUMENT_ROOT.'/core/class/dolgraph.class.php';
			include_once DOL_DOCUMENT_ROOT.'/comm/propal/class/propalestats.class.php';
			$autosetarray = preg_split("/[,;:]+/", GETPOST('DOL_AUTOSET_COOKIE'));
			if (in_array('DOLUSERCOOKIE_box_'.$this->boxcode, $autosetarray))
			{
				$endyear = GETPOST($param_year, 'int');
			} else {
				$tmparray = isset($_COOKIE['DOLUSERCOOKIE_box_'.$this->boxcode]) ? json_decode($_COOKIE['DOLUSERCOOKIE_box_'.$this->boxcode], true) : array('year'=>date('Y'));
				$endyear = $tmparray['year'];
			}

			$nowarray = dol_getdate(dol_now(), true);
			if (empty($endyear)) $endyear = $nowarray['year'];
			$startyear = $endyear - (empty($conf->global->MAIN_NB_OF_YEAR_IN_WIDGET_GRAPH) ? 1 : $conf->global->MAIN_NB_OF_YEAR_IN_WIDGET_GRAPH);

			$WIDTH = '540';
			$HEIGHT = '250';

			if (!defined('DIR_HTTP_BO')) {
				if ($_SERVER['SERVER_NAME']=='dev.batibarr.batiactugroupe.com' || $_SERVER['SERVER_NAME']=='dev.batibarr-16.batiactugroupe.com') {
					define('DIR_HTTP_BO','http://dev.batibarr-bo.batiactu.com/');
				}
				else if ($_SERVER['SERVER_NAME']=='batibarr.batiactu.space') {
					define('DIR_HTTP_BO','http://batibarr-bo.batiactu.space/');
				}
				else if ($_SERVER['SERVER_NAME']=='batibarr-old.batiactugroupe.com') {
					define('DIR_HTTP_BO','https://batibarr-bo-old.batiactu.com//');
				}
				else {
					define('DIR_HTTP_BO','https://batibarr-bo.batiactu.info/');
				}
			}
			$url = DIR_HTTP_BO.'scripts/interface.php?mode=GET_FLUX_TOTAL_SUPPORT&LIST' ;
			if (isset($_REQUEST['f_user_support']) && (int)$_REQUEST['f_user_support'] > 0) $url.= '&f_user='.$_REQUEST['f_user_support'] ;
			$url.= '&year_sta='.( (isset($_REQUEST['f_year_sta_support']) && (int)$_REQUEST['f_year_sta_support'] > 0) ? $_REQUEST['f_year_sta_support'] : date('Y') ) ;
			$url.= '&year_end='.( (isset($_REQUEST['f_year_end_support']) && (int)$_REQUEST['f_year_end_support'] > 0) ? $_REQUEST['f_year_end_support'] : date('Y') ) ;
			$url.= '&month_sta='.( (isset($_REQUEST['f_month_sta_support']) && (int)$_REQUEST['f_month_sta_support'] > 0) ? $_REQUEST['f_month_sta_support'] : 1 ) ;
			$url.= '&month_end='.( (isset($_REQUEST['f_month_end_support']) && (int)$_REQUEST['f_month_end_support'] > 0) ? $_REQUEST['f_month_end_support'] : 12 ) ;
			if (isset($_REQUEST['DEBUG'])) echo "GET_FLUX_TOTAL_SUPPORT URL: <a href=\"".$url."\" target=\"_blank\">".$url."</a><br>\r\n" ;
			//echo "GET_FLUX_SUPPORT: ".$url."<br>" ;
			$res = file_get_contents($url) ;
			$TRes = json_decode($res) ;
			if (isset($_REQUEST['DEBUG'])) echo "<textarea style=\"width:95%;height:120px;\">".print_r($TRes, TRUE)."</textarea><hr />\r\n" ;
			$label_title_date = '' ;
			if (isset($TRes->dt_deb) && isset($TRes->dt_fin)) {
				$label_title_date = 'du '.date('d/m/Y', strtotime($TRes->dt_deb)).' au '.date('d/m/Y', strtotime($TRes->dt_fin)) ;
			}
			else if (isset($TRes->dt_deb)) {
				$label_title_date = 'le '.date('d/m/Y', strtotime($TRes->dt_deb));
			}

			$year_start = isset($TRes->year_signe_min) ? $TRes->year_signe_min : 2019 ;

			$data2 = $data1 = array();
			$dataseries = array();
			$totaloppnb = 0;
			$ind = 0;
			$colorseries = array();
			/*$colorseries[] = '#cbd3d3'; // draft
			$colorseries[] = '#bc9526'; // validated
			$colorseries[] = '#9c9c26'; // approved
			$colorseries[] = '#bca52b';
			$colorseries[] = '#25a580'; // Color ok
			$colorseries[] = '#cad2d2';
			$colorseries[] = '#cad2d2';
			$colorseries[] = '#25a580';
			$colorseries[] = '#993013';
			$colorseries[] = '#e7f0f0';*/
			$datatype2 = $datatype1 = array('bars');

			$tr_label = array(
				'Batiactu '=>'',
				'Construcom Awards'=>'Construcom',
				'Content Builders'=>'Builders',
				'EnerJ-meeting'=>'ENJ',
				'Les Assises du Logement'=>'Assises',
				'Marketing'=>'MKT',
				'Maison à part'=>'MAP',
				'Trophées de la Construction'=>'Trophées',
				'Prestation Conférences-Animations'=>'Presta Conf.',
			) ;
			foreach($TRes->value as $code=>$TResLigne){
					$label = strtr($TResLigne->label, $tr_label) ;
					$color = $TResLigne->color ;
					//$label.= ' '.$code ;
					$totaloppnb += (float)$TResLigne->total;

					$row = array(
						0=>$label,//.' '.$code
						1=>$TResLigne->total,
					) ;
					$data1[] = $row ;
					$dataseries[] = $row;
					$colorseries[] = $color;

					$row = array(
						0=>$label,
						1=>$TResLigne->nb,
					) ;
					$data2[] = $row ;

					$ind++ ;
			}
			//echo "<pre>".print_r($data1, TRUE)."</pre><hr>" ;

			//$stats = new PropaleStats($this->db, $socid, 0);

			// Build graphic number of object. $data = array(array('Lib',val1,val2,val3),...)

			//$data1 = $stats->getNbByMonthWithPrevYear($endyear, $startyear, (GETPOST('action', 'aZ09') == $refreshaction ?-1 : (3600 * 24)), ($WIDTH < 300 ? 2 : 0), $startmonth);
			//$datatype1 = array_pad(array(), ($endyear - $startyear + 1), 'bars');
			//echo "<pre>".print_r($data1, TRUE)."</pre><hr>" ;
			//echo "<pre>".print_r($datatype1, TRUE)."</pre>" ; exit() ;
			$datatype1 = $datatype1 = array('bars');

			$filenamenb = $dir."/".$prefix."statssupportmontyear-".$endyear.".png";
			$fileurlnb = DOL_URL_ROOT.'/viewimage.php?modulepart=statssupportmont&amp;file=statssupportmontyear-'.$endyear.'.png';
			$view_graph_number = FALSE ;
			$view_graph_montant = FALSE ;

			$px1 = new DolGraph();
			$mesg = $px1->isGraphKo();
			if (!$mesg)
			{
				$px1->SetType($datatype1);
				$px1->SetData($data1);
				unset($data1);

				$px1->SetLegend(array('Montant signé'));
				$px1->SetMaxValue($px1->GetCeilMaxValue());
				$px1->SetDataColor(array(array(152, 25, 57)));
				$px1->SetWidth($WIDTH);
				$px1->SetHeight($HEIGHT);
				$px1->SetYLabel("Nombre Y");
				$px1->SetShading(3);
				$px1->SetHorizTickIncrement(1);
				$px1->SetCssPrefix("cssboxes");
				$px1->mode = 'depth';
				$px1->SetTitle("".$label_title_date);

				$px1->draw($filenamenb, $fileurlnb);

				$dolgraph = new DolGraph();
				$dolgraph->SetData($dataseries);
				$dolgraph->SetDataColor(array_values($colorseries));
				$dolgraph->setShowLegend(2);
				$dolgraph->setShowPercent(1);
				$dolgraph->SetType(array('pie'));
				$dolgraph->setWidth('400');
				$dolgraph->SetHeight('200');
				$dolgraph->draw('idgraphstatus');

				$view_graph_montant = TRUE ;
			}

			$filenamenb = $dir."/".$prefix."statssupportnbyear-".$endyear.".png";
			$fileurlnb = DOL_URL_ROOT.'/viewimage.php?modulepart=statssupportnb&amp;file=statssupportnbyear-'.$endyear.'.png';

			$px2 = new DolGraph();
			$mesg = $px2->isGraphKo();
			if (!$mesg)
			{
				$px2->SetType($datatype2);
				$px2->SetData($data2);
				unset($data2);

				$px2->SetLegend(array("Nombre d'affaires"));
				$px2->SetMaxValue($px2->GetCeilMaxValue());
				$px2->SetDataColor(array(array(152, 25, 57)));
				$px2->SetWidth($WIDTH);
				$px2->SetHeight($HEIGHT);
				$px2->SetYLabel("Stat");
				$px2->SetShading(3);
				$px2->SetHorizTickIncrement(1);
				$px2->SetCssPrefix("cssboxes");
				$px2->mode = 'depth';
				$px2->SetTitle("".$label_title_date);

				$px2->draw($filenamenb, $fileurlnb);
				$view_graph_number = TRUE ;
			}

			if (empty($conf->use_javascript_ajax))
			{
				$langs->load("errors");
				$mesg = $langs->trans("WarningFeatureDisabledWithDisplayOptimizedForBlindNoJs");
			}

			if (!$mesg)
			{
				$stringtoshow = '';
				$stringtoshow .= '<script type="text/javascript" language="javascript">
jQuery(document).ready(function() {
	jQuery("#idsubimg'.$this->boxcode.'").click(function() {
		jQuery("#idfilter'.$this->boxcode.'").toggle();
	});
});
</script>';
				$stringtoshow .= '<form name="form_statssupport" class="flat formboxfilter" method="GET" action="'.$_SERVER["PHP_SELF"].'">';
				if (isset($_REQUEST['f_user_month']) && (int)$_REQUEST['f_user_month'] >0) $stringtoshow .= '<input type="hidden" id="f_user_month" name="f_user_month" value="'.$_REQUEST['f_user_month'].'">';
				if (isset($_REQUEST['f_support_month']) && $_REQUEST['f_support_month'] != '') $stringtoshow .= '<input type="hidden" id="f_support_month" name="f_support_month" value="'.$_REQUEST['f_support_month'].'">';

				if (isset($_REQUEST['f_support_comm']) && $_REQUEST['f_support_comm'] != '') $stringtoshow .= '<input type="hidden" id="f_support_comm" name="f_support_comm" value="'.$_REQUEST['f_support_comm'].'">';
				if (isset($_REQUEST['f_year_sta_comm']) && $_REQUEST['f_year_sta_comm'] != '') $stringtoshow .= '<input type="hidden" id="f_year_sta_comm" name="f_year_sta_comm" value="'.$_REQUEST['f_year_sta_comm'].'">';
				if (isset($_REQUEST['f_year_end_comm']) && $_REQUEST['f_year_end_comm'] != '') $stringtoshow .= '<input type="hidden" id="f_year_end_comm" name="f_year_end_comm" value="'.$_REQUEST['f_year_end_comm'].'">';
				if (isset($_REQUEST['f_month_sta_comm']) && $_REQUEST['f_month_sta_comm'] != '') $stringtoshow .= '<input type="hidden" id="f_month_sta_comm" name="f_month_sta_comm" value="'.$_REQUEST['f_month_sta_comm'].'">';
				if (isset($_REQUEST['f_month_end_comm']) && $_REQUEST['f_month_end_comm'] != '') $stringtoshow .= '<input type="hidden" id="f_month_end_comm" name="f_month_end_comm" value="'.$_REQUEST['f_month_end_comm'].'">';

				$url_user = DIR_HTTP_BO.'scripts/interface.php?mode=GET_LIST_USER' ;
				$res = file_get_contents($url_user) ;
				$TUser = json_decode($res) ;

				$TMonth = array(
					1=>'Janvier',
					2=>'Février',
					3=>'Mars',
					4=>'Avril',
					5=>'Mai',
					6=>'Juin',
					7=>'Juillet',
					8=>'Aout',
					9=>'Septembre',
					10=>'Octobre',
					11=>'Novembre',
					12=>'Décembre',
				);

				$TYear = array();
				for ($ind=$year_start ; $ind<= (date('Y')) ; $ind++) {
					$TYear[$ind] = $ind ;
				}

				$sel_user = $this->_get_selecteur_table($TUser->value, 'f_user_support', (isset($_REQUEST['f_user_support']) ? $_REQUEST['f_user_support'] : ''), TRUE, TRUE) ;
				$sel_month_sta = $this->_get_selecteur_table($TMonth, 'f_month_sta_support', ( isset($_REQUEST['f_month_sta_support']) ? $_REQUEST['f_month_sta_support'] : 1 ), FALSE, TRUE) ;
				$sel_month_end = $this->_get_selecteur_table($TMonth, 'f_month_end_support', ( isset($_REQUEST['f_month_end_support']) ? $_REQUEST['f_month_end_support'] : 12 ), FALSE, TRUE) ;
				$sel_year_sta = $this->_get_selecteur_table($TYear, 'f_year_sta_support', ( isset($_REQUEST['f_year_sta_support']) ? $_REQUEST['f_year_sta_support'] : date('Y') ), FALSE, TRUE) ;
				$sel_year_end = $this->_get_selecteur_table($TYear, 'f_year_end_support', ( isset($_REQUEST['f_year_end_support']) ? $_REQUEST['f_year_end_support'] : date('Y') ), FALSE, TRUE) ;

				$stringtoshow .= '<table class="noborder centpercent" style="border:0;">';
				$stringtoshow .= '<tr class="liste_titre">';
				$stringtoshow .= '<td class="titlefield">de '.$sel_month_sta;
				$stringtoshow .= $sel_year_sta.'</td>';
				$stringtoshow .= '<td class="titlefield">&agrave; '.$sel_month_end;
				$stringtoshow .= $sel_year_end.'</td>';
				$stringtoshow .= '</tr>';
				$stringtoshow .= '<tr class="liste_titre">';
				$stringtoshow .= '<td class="titlefield">'.$sel_user.'</td>';
				$stringtoshow .= '<td class="titlefield" colspan=3 align="right"><input class="button" type="submit" value="Filtrer"></td>';
				$stringtoshow .= '</tr>';
				$stringtoshow .= '</table>';
				$stringtoshow .= '</form>'."\r\n";

				if ($view_graph_montant) {
					$stringtoshow .= '<div class="fichecenter">';
					$stringtoshow .= $px1->show();
					$stringtoshow .= '</div>';
				}
				if ($view_graph_montant) {
					$stringtoshow .= '<div class="fichecenter">';
					$stringtoshow .= $dolgraph->show($totaloppnb ? 0 : 1);
					$stringtoshow .= '</div>'."\r\n";
				}
				if ($view_graph_number) {
					$stringtoshow .= '<div class="fichecenter">';
					$stringtoshow .= $px2->show();
					$stringtoshow .= '</div>'."\r\n";
				}

				$this->info_box_contents[0][0] = array(
					'tr'=>'class="oddeven nohover"',
					'td' => 'class="nohover center"',
					'textnoformat'=>$stringtoshow,
				);
			} else {
				$this->info_box_contents[0][0] = array(
					'tr'=>'class="oddeven nohover"',
					'td' => 'class="nohover left"',
					'maxlength' => 500,
					'text' => $mesg,
				);
			}
		} else {
			$this->info_box_contents[0][0] = array(
				'td' => 'class="nohover opacitymedium left"',
				'text' => $langs->trans("ReadPermissionNotAllowed")
			);
		}
	}

	/**
	 *	Method to show box
	 *
	 *	@param	array	$head       Array with properties of box title
	 *	@param  array	$contents   Array with properties of box lines
	 *  @param	int		$nooutput	No print, only return string
	 *	@return	string
	 */
	public function showBox($head = null, $contents = null, $nooutput = 0)
	{
		return parent::showBox($this->info_box_head, $this->info_box_contents, $nooutput);
	}

	/* Create slecteur for form */
	private function _get_selecteur_table($TValue, $name, $key_def, $with_empty=TRUE, $is_int=FALSE)
	{
		//echo "<pre>".print_r($TValue, TRUE)."</pre>" ; exit() ;
		$sel_user = "<select name=\"".$name."\" id=\"".$name."\">\r\n" ;

		if ($with_empty) {
			if ($key_def === '') $sel = ' selected' ;
			else $sel = '' ;

			$sel_user.= "<option value=\"\"".$sel.">Tous</option>\r\n" ;
		}

		foreach($TValue as $key=>$label){
			if ($is_int) {
				if ($key_def!=='' AND (int)$key_def == $key) $sel = ' selected' ;
				else $sel = '' ;
			}
			else {
				if ($key_def === $key) $sel = ' selected' ;
				else $sel = '' ;
			}

			$sel_user.= "<option value=\"".$key."\"".$sel.">".$label."</option>\r\n" ;
		}
		$sel_user.= "</select>\r\n" ;

		return $sel_user ;
	}
}
