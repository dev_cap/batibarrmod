<?php
/* Copyright (C) 2013 Laurent Destailleur  <eldy@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 *	\file       htdocs/core/boxes/box_graph_stats_month.php
 *	\ingroup    propales
 *	\brief      Box to show graph of proposals per month
 */
include_once DOL_DOCUMENT_ROOT.'/core/boxes/modules_boxes.php';


/**
 * Class to manage the box to show last propals
 */
class box_graph_stats_month extends ModeleBoxes
{
	public $boxcode = "statsmonthflux";
	public $boximg = "object_stat";
	public $boxlabel = "Chiffre d'affaires par mois";
	public $depends = array();

	/**
	 * @var DoliDB Database handler.
	 */
	public $db;

	public $info_box_head = array();
	public $info_box_contents = array();


	/**
	 *  Constructor
	 *
	 * 	@param	DoliDB	$db			Database handler
	 *  @param	string	$param		More parameters
	 */
	public function __construct($db, $param)
	{
		global $user;

		$this->db = $db;

		$this->hidden = !($user->rights->propal->lire);
	}

	/**
	 *  Load data into info_box_contents array to show array later.
	 *
	 *  @param	int		$max        Maximum number of records to load
	 *  @return	void
	 */
	public function loadBox($max = 5)
	{
		global $conf, $user, $langs;

		$this->max = $max;

		$refreshaction = 'refresh_'.$this->boxcode;

		$startmonth = $conf->global->SOCIETE_FISCAL_MONTH_START ? ($conf->global->SOCIETE_FISCAL_MONTH_START) : 1;
		if (empty($conf->global->GRAPH_USE_FISCAL_YEAR)) $startmonth = 1;

		//$langs->load("propal");

		$text = "Chiffre d'affaires par mois" ;
		//$text = $langs->trans("BoxProposalsPerMonth", $max);
		$this->info_box_head = array(
				'text' => $text,
				'limit'=> dol_strlen($text),
				'graph'=> 1, // Set to 1 if it's a box graph
				'sublink'=>'',
				'subtext'=>$langs->trans("Filter"),
				'subpicto'=>'filter.png',
				'subclass'=>'linkobject boxfilter',
				'target'=>'none'	// Set '' to get target="_blank"
		);

		$dir = ''; // We don't need a path because image file will not be saved into disk
		$prefix = '';
		$socid = 0;
		if ($user->socid) $socid = $user->socid;
		if (!$user->rights->societe->client->voir || $socid) $prefix .= 'private-'.$user->id.'-'; // If user has no permission to see all, output dir is specific to user

		if ($user->rights->propal->lire)
		{
			$param_year = 'DOLUSERCOOKIE_box_'.$this->boxcode.'_year';

			include_once DOL_DOCUMENT_ROOT.'/core/class/dolgraph.class.php';
			include_once DOL_DOCUMENT_ROOT.'/comm/propal/class/propalestats.class.php';
			$autosetarray = preg_split("/[,;:]+/", GETPOST('DOL_AUTOSET_COOKIE'));
			if (in_array('DOLUSERCOOKIE_box_'.$this->boxcode, $autosetarray))
			{
				$endyear = GETPOST($param_year, 'int');
			} else {
				$tmparray = isset($_COOKIE['DOLUSERCOOKIE_box_'.$this->boxcode]) ? json_decode($_COOKIE['DOLUSERCOOKIE_box_'.$this->boxcode], true) : array('year'=>date('Y'));
				$endyear = $tmparray['year'];
			}

			$nowarray = dol_getdate(dol_now(), true);
			if (empty($endyear)) $endyear = $nowarray['year'];
			$startyear = $endyear - (empty($conf->global->MAIN_NB_OF_YEAR_IN_WIDGET_GRAPH) ? 1 : $conf->global->MAIN_NB_OF_YEAR_IN_WIDGET_GRAPH);

			$WIDTH = '480';
			$HEIGHT = '250';

			if (!defined('DIR_HTTP_BO')) {
				if ($_SERVER['SERVER_NAME']=='dev.batibarr.batiactugroupe.com' || $_SERVER['SERVER_NAME']=='dev.batibarr-16.batiactugroupe.com') {
					define('DIR_HTTP_BO','http://dev.batibarr-bo.batiactu.com/');
				}
				else if ($_SERVER['SERVER_NAME']=='batibarr.batiactu.space') {
					define('DIR_HTTP_BO','http://batibarr-bo.batiactu.space/');
				}
				else if ($_SERVER['SERVER_NAME']=='batibarr-old.batiactugroupe.com') {
					define('DIR_HTTP_BO','https://batibarr-bo-old.batiactu.com//');
				}
				else {
					define('DIR_HTTP_BO','https://batibarr-bo.batiactu.info/');
				}
			}
			$url = DIR_HTTP_BO.'scripts/interface.php?mode=GET_FLUX_MOIS&LIST' ;
			if (isset($_REQUEST['f_user_month']) && (int)$_REQUEST['f_user_month'] > 0) $url.= '&f_user='.$_REQUEST['f_user_month'] ;
			if (isset($_REQUEST['f_support_month']) && $_REQUEST['f_support_month'] != '') $url.= '&f_support='.$_REQUEST['f_support_month'] ;
			if (isset($_REQUEST['DEBUG'])) echo "GET_FLUX_MONTH URL: <a href=\"".$url.'&year_end='.date('Y')."\" target=\"_blank\">".$url.'&year_end='.date('Y')."</a><br>\r\n" ;
			//echo $url.'&year_end='.date('Y')."<br>" ;
			$res = file_get_contents( $url.'&year_end='.date('Y') ) ;
			$TRes = json_decode($res) ;
			if (isset($_REQUEST['DEBUG'])) echo "<textarea style=\"width:95%;height:120px;\">".print_r($TRes, TRUE)."</textarea><hr />\r\n" ;
			$label_title_date = '' ;

			//echo $url.'&year_end='.date('Y', strtotime('-1 year'))."<br>" ;
			$res = file_get_contents( $url.'&year_end='.date('Y', strtotime('-1 year')) ) ;
			$TRes_n1 = json_decode($res) ;
			//echo "<pre>".print_r($TRes_n1->value, TRUE)."</pre><hr>" ;

			$data2 = $data1 = array();
			$datatype2 = $datatype1 = array('bars', 'bars');

			foreach($TRes->value as $code=>$TValue){
				$label = $code ;
				//echo "* ".$code." <pre>".print_r($TRes_n1->value, TRUE)."</pre><hr>" ;

				$row = array(
					0=>$label,
					1=>round($TRes_n1->value->{$code}->total),
					2=>round($TValue->total),
				) ;
				$data1[] = $row ;

				$row = array(
					0=>$label,
					1=>$TRes_n1->value->{$code}->nb,
					2=>$TValue->nb,
				) ;
				$data2[] = $row ;
			}
			//echo "<pre>".print_r($data1, TRUE)."</pre><hr>" ;

			$stats = new PropaleStats($this->db, $socid, 0);

			// Build graphic number of object. $data = array(array('Lib',val1,val2,val3),...)

			//$data1 = $stats->getNbByMonthWithPrevYear($endyear, $startyear, (GETPOST('action', 'aZ09') == $refreshaction ?-1 : (3600 * 24)), ($WIDTH < 300 ? 2 : 0), $startmonth);
			//$datatype1 = array_pad(array(), ($endyear - $startyear + 1), 'bars');
			//echo "<pre>".print_r($data1, TRUE)."</pre><hr>" ;
			//echo "<pre>".print_r($datatype1, TRUE)."</pre>" ; exit() ;*/

			$filenamenb = $dir."/".$prefix."statsmonthmontyear-".$endyear.".png";
			$fileurlnb = DOL_URL_ROOT.'/viewimage.php?modulepart=statsmonthmont&amp;file=statsmonthmontyear-'.$endyear.'.png';

			$px1 = new DolGraph();
			$mesg = $px1->isGraphKo();
			if (!$mesg)
			{
				$px1->SetType($datatype1);
				$px1->SetData($data1);
				unset($data1);

				$px1->SetLegend( array( date('Y', strtotime('-1 year')), date('Y') ) );
				$px1->SetMaxValue($px1->GetCeilMaxValue());
				$px1->SetDataColor(array(array(251, 124, 0), array(152, 25, 57)));
				$px1->SetWidth($WIDTH);
				$px1->SetHeight($HEIGHT);
				//$px1->SetYLabel("Nombre Y");
				$px1->SetShading(3);
				$px1->SetHorizTickIncrement(1);
				$px1->SetCssPrefix("cssboxes");
				$px1->mode = 'depth';
				$px1->SetTitle("Montant sign&eacute;".$label_title_date);
				//$px1->SetDataColor( array(array(20, 10, 50), array(60, 60, 180), array(90, 190, 20)) ) ;

				$px1->draw($filenamenb, $fileurlnb);
			}


			$filenamenb = $dir."/".$prefix."statsmonthnbyear-".$endyear.".png";
			$fileurlnb = DOL_URL_ROOT.'/viewimage.php?modulepart=statsmonthnb&amp;file=statsmonthnbyear-'.$endyear.'.png';

			$px2 = new DolGraph();
			$mesg = $px2->isGraphKo();
			if (!$mesg)
			{
				$px2->SetType($datatype2);
				$px2->SetData($data2);
				unset($data2);

				$px2->SetLegend( array( date('Y', strtotime('-1 year')), date('Y') ) );
				$px2->SetMaxValue($px2->GetCeilMaxValue());
				$px2->SetDataColor(array(array(251, 124, 0), array(152, 25, 57)));
				$px2->SetWidth($WIDTH);
				$px2->SetHeight($HEIGHT);
				$px2->SetYLabel("Stat");
				$px2->SetShading(3);
				$px2->SetHorizTickIncrement(1);
				$px2->SetCssPrefix("cssboxes");
				$px2->mode = 'depth';
				$px2->SetTitle("Nombre d'affaires".$label_title_date);

				$px2->draw($filenamenb, $fileurlnb);
			}

			if (empty($conf->use_javascript_ajax))
			{
				$langs->load("errors");
				$mesg = $langs->trans("WarningFeatureDisabledWithDisplayOptimizedForBlindNoJs");
			}

			if (!$mesg)
			{
				$stringtoshow = '';
				$stringtoshow .= '<script type="text/javascript" language="javascript">
					jQuery(document).ready(function() {
						jQuery("#idsubimg'.$this->boxcode.'").click(function() {
							jQuery("#idfilter'.$this->boxcode.'").toggle();
						});
					});
					</script>';
				$stringtoshow .= '<form name="form_statsmois" class="flat formboxfilter" method="GET" action="'.$_SERVER["PHP_SELF"].'">';

				if (isset($_REQUEST['f_support_comm']) && $_REQUEST['f_support_comm'] != '') $stringtoshow .= '<input type="hidden" id="f_support_comm" name="f_support_comm" value="'.$_REQUEST['f_support_comm'].'">';
				if (isset($_REQUEST['f_year_sta_comm']) && $_REQUEST['f_year_sta_comm'] != '') $stringtoshow .= '<input type="hidden" id="f_year_sta_comm" name="f_year_sta_comm" value="'.$_REQUEST['f_year_sta_comm'].'">';
				if (isset($_REQUEST['f_year_end_comm']) && $_REQUEST['f_year_end_comm'] != '') $stringtoshow .= '<input type="hidden" id="f_year_end_comm" name="f_year_end_comm" value="'.$_REQUEST['f_year_end_comm'].'">';
				if (isset($_REQUEST['f_month_sta_comm']) && $_REQUEST['f_month_sta_comm'] != '') $stringtoshow .= '<input type="hidden" id="f_month_sta_comm" name="f_month_sta_comm" value="'.$_REQUEST['f_month_sta_comm'].'">';
				if (isset($_REQUEST['f_month_end_comm']) && $_REQUEST['f_month_end_comm'] != '') $stringtoshow .= '<input type="hidden" id="f_month_end_comm" name="f_month_end_comm" value="'.$_REQUEST['f_month_end_comm'].'">';

				if (isset($_REQUEST['f_user_support']) && (int)$_REQUEST['f_user_support'] >0) $stringtoshow .= '<input type="hidden" id="f_user_support" name="f_user_support" value="'.$_REQUEST['f_user_support'].'">';
				if (isset($_REQUEST['f_year_sta_support']) && $_REQUEST['f_year_sta_support'] != '') $stringtoshow .= '<input type="hidden" id="f_year_sta_support" name="f_year_sta_support" value="'.$_REQUEST['f_year_sta_support'].'">';
				if (isset($_REQUEST['f_year_end_support']) && $_REQUEST['f_year_end_support'] != '') $stringtoshow .= '<input type="hidden" id="f_year_end_support" name="f_year_end_support" value="'.$_REQUEST['f_year_end_support'].'">';
				if (isset($_REQUEST['f_month_sta_support']) && $_REQUEST['f_month_sta_support'] != '') $stringtoshow .= '<input type="hidden" id="f_month_sta_support" name="f_month_sta_support" value="'.$_REQUEST['f_month_sta_support'].'">';
				if (isset($_REQUEST['f_month_end_support']) && $_REQUEST['f_month_end_support'] != '') $stringtoshow .= '<input type="hidden" id="f_month_end_support" name="f_month_end_support" value="'.$_REQUEST['f_month_end_support'].'">';

				$url_support = DIR_HTTP_BO.'scripts/interface.php?mode=GET_LIST_SUPPORT' ;
				$res = file_get_contents($url_support) ;
				$TSupport = json_decode($res) ;

				$url_user = DIR_HTTP_BO.'scripts/interface.php?mode=GET_LIST_USER' ;
				$res = file_get_contents($url_user) ;
				$TUser = json_decode($res) ;

				$sel_support = $this->_get_selecteur_table($TSupport->value, 'f_support_month', ( isset($_REQUEST['f_support_month']) ? $_REQUEST['f_support_month'] : '' )) ;
				$sel_user = $this->_get_selecteur_table($TUser->value, 'f_user_month', (isset($_REQUEST['f_user_month']) ? $_REQUEST['f_user_month'] : ''), TRUE, TRUE) ;

				$stringtoshow .= '<table class="noborder centpercent" style="border:0;">';
				$stringtoshow .= '<tr class="liste_titre">';
				$stringtoshow .= '<td class="titlefield">'.$sel_user.'</td>';
				$stringtoshow .= '<td class="titlefield">'.$sel_support.'</td>';
				$stringtoshow .= '<td class="titlefield"><input class="button" type="submit" value="Filtrer"></td>';
				$stringtoshow .= '</tr>';
				$stringtoshow .= '</table>';
				$stringtoshow .= '</form>'."\r\n";

				$stringtoshow .= '<div class="fichecenter">';
				$stringtoshow .= $px1->show();
				$stringtoshow .= '</div>';
				$stringtoshow .= '<div class="fichecenter">';
				$stringtoshow .= $px2->show();
				$stringtoshow .= '</div>'."\r\n";

				$this->info_box_contents[0][0] = array(
					'tr'=>'class="oddeven nohover"',
					'td' => 'class="nohover center"',
					'textnoformat'=>$stringtoshow,
				);
			} else {
				$this->info_box_contents[0][0] = array(
					'tr'=>'class="oddeven nohover"',
					'td' => 'class="nohover left"',
					'maxlength' => 500,
					'text' => $mesg,
				);
			}
		} else {
			$this->info_box_contents[0][0] = array(
				'td' => 'class="nohover opacitymedium left"',
				'text' => $langs->trans("ReadPermissionNotAllowed")
			);
		}
	}

	/**
	 *	Method to show box
	 *
	 *	@param	array	$head       Array with properties of box title
	 *	@param  array	$contents   Array with properties of box lines
	 *  @param	int		$nooutput	No print, only return string
	 *	@return	string
	 */
	public function showBox($head = null, $contents = null, $nooutput = 0)
	{
		return parent::showBox($this->info_box_head, $this->info_box_contents, $nooutput);
	}

	/* Create slecteur for form */
	private function _get_selecteur_table($TValue, $name, $key_def, $with_empty=TRUE, $is_int=FALSE)
	{
		//echo "<pre>".print_r($TValue, TRUE)."</pre>" ; exit() ;
		$sel_user = "<select name=\"".$name."\" id=\"".$name."\">\r\n" ;

		if ($with_empty) {
			if ($key_def === '') $sel = ' selected' ;
			else $sel = '' ;

			$sel_user.= "<option value=\"\"".$sel.">Tous</option>\r\n" ;
		}

		foreach($TValue as $key=>$label){
			if ($is_int) {
				if ($key_def!=='' AND (int)$key_def == $key) $sel = ' selected' ;
				else $sel = '' ;
			}
			else {
				if ($key_def === $key) $sel = ' selected' ;
				else $sel = '' ;
			}

			$sel_user.= "<option value=\"".$key."\"".$sel.">".$label."</option>\r\n" ;
		}
		$sel_user.= "</select>\r\n" ;

		return $sel_user ;
	}
}
