<?php
/*******************************************************************************
 * Interface to BackOffice
 *******************************************************************************/
if ($_SERVER['SERVER_NAME']=='dev.batibarr.batiactugroupe.com' || $_SERVER['SERVER_NAME']=='dev.batibarr-16.batiactugroupe.com') {
	define('DIR_HTTP','http://'.$_SERVER['SERVER_NAME'].'/');
	define('DIR_HTTP_BO','http://dev.batibarr-bo.batiactu.com/');
}
else if ($_SERVER['SERVER_NAME']=='batibarr.batiactu.space') {
  define('DIR_HTTP','https://'.$_SERVER['SERVER_NAME'].'/');
  define('DIR_HTTP_BO','http://batibarr-bo.batiactu.space/');
}
else if ($_SERVER['SERVER_NAME']=='batibarr-old.batiactugroupe.com') {
	define('DIR_HTTP','https://'.$_SERVER['SERVER_NAME'].'/');
	define('DIR_HTTP_BO','https://batibarr-bo-old.batiactu.com//');
}
else {
	define('DIR_HTTP','https://'.$_SERVER['SERVER_NAME'].'/');
	define('DIR_HTTP_BO','https://batibarr-bo.batiactu.info/');
}
if (!isset($_REQUEST['mode'])) {
	exit() ;
}

switch ($_REQUEST['mode']) {
	case 'GET_USER_AGENDA':
	case 'SET_STATUT_PROPAL':
	case 'SET_STATUT_LINE_TICKET':
	case 'GET_EXTRA_AFFAIRES':
	case 'GET_CODE_COMPTA_FACTURE':
	default:
		$resultat = default_ask($_REQUEST) ;

		if (isset($_REQUEST['DEBUG'])) echo "<pre>".print_r($resultat, TRUE)."</pre><br />\r\n";
		else {
			header('Content-Type: application/json; charset=utf-8');
			echo json_encode($resultat);
		}
}


function default_ask(&$tab) {
	if(isset($_REQUEST['DEBUG'])) echo "URL : ".DIR_HTTP_BO."scripts/interface.php<br />\r\n";
	$resultat = file_post_contents(DIR_HTTP_BO."scripts/interface.php", $tab);
	$resultat = json_decode($resultat);

	return $resultat ;
}

function file_post_contents($url, $params){
	$postdata = $params;

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_URL, $url);

	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	if (is_array($postdata)) {
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postdata));
	}

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	$result = curl_exec($ch);
	if ($result === false) {
		error_log('file_post_contents : Erreur Curl : ' . curl_error($ch));
	}

	curl_close($ch);
	return $result;
}
