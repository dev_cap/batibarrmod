CREATE TABLE IF NOT EXISTS `llx_c_ba_complement` (
	`rowid` integer AUTO_INCREMENT PRIMARY KEY NOT NULL,
	`code` varchar(20) NOT NULL,
	`label` varchar(100) DEFAULT NULL,
	`annee_event` int(4) NOT NULL,
	`position` int(11) NOT NULL,
	`active` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB;