<?php
/* Copyright (C) 2021 SuperAdmin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Library javascript to enable Browser notifications
 */

if (!defined('NOREQUIREUSER'))  define('NOREQUIREUSER', '1');
if (!defined('NOREQUIREDB'))    define('NOREQUIREDB', '1');
if (!defined('NOREQUIRESOC'))   define('NOREQUIRESOC', '1');
if (!defined('NOREQUIRETRAN'))  define('NOREQUIRETRAN', '1');
if (!defined('NOCSRFCHECK'))    define('NOCSRFCHECK', 1);
if (!defined('NOTOKENRENEWAL')) define('NOTOKENRENEWAL', 1);
if (!defined('NOLOGIN'))        define('NOLOGIN', 1);
if (!defined('NOREQUIREMENU'))  define('NOREQUIREMENU', 1);
if (!defined('NOREQUIREHTML'))  define('NOREQUIREHTML', 1);
if (!defined('NOREQUIREAJAX'))  define('NOREQUIREAJAX', '1');

if (!defined('DIR_HTTP')) {
    if ($_SERVER['SERVER_NAME']=='dev.batibarr.batiactugroupe.com' || $_SERVER['SERVER_NAME']=='dev.batibarr-16.batiactugroupe.com') {
        define('DIR_HTTP','http://'.$_SERVER['SERVER_NAME'].'/');
    }
    else if ($_SERVER['SERVER_NAME']=='batibarr.batiactu.space') {
      define('DIR_HTTP','https://'.$_SERVER['SERVER_NAME'].'/');
    }
    else {
        define('DIR_HTTP','https://'.$_SERVER['SERVER_NAME'].'/');
    }
}

/**
 * \file    batibarrmod/js/batibarrmod.js.php
 * \ingroup batibarrmod
 * \brief   JavaScript file for module Batibarrmod.
 */

// Load Dolibarr environment
$res = 0;
// Try main.inc.php into web root known defined into CONTEXT_DOCUMENT_ROOT (not always defined)
if (!$res && !empty($_SERVER["CONTEXT_DOCUMENT_ROOT"])) $res = @include $_SERVER["CONTEXT_DOCUMENT_ROOT"]."/main.inc.php";
// Try main.inc.php into web root detected using web root calculated from SCRIPT_FILENAME
$tmp = empty($_SERVER['SCRIPT_FILENAME']) ? '' : $_SERVER['SCRIPT_FILENAME']; $tmp2 = realpath(__FILE__); $i = strlen($tmp) - 1; $j = strlen($tmp2) - 1;
while ($i > 0 && $j > 0 && isset($tmp[$i]) && isset($tmp2[$j]) && $tmp[$i] == $tmp2[$j]) { $i--; $j--; }
if (!$res && $i > 0 && file_exists(substr($tmp, 0, ($i + 1))."/main.inc.php")) $res = @include substr($tmp, 0, ($i + 1))."/main.inc.php";
if (!$res && $i > 0 && file_exists(substr($tmp, 0, ($i + 1))."/../main.inc.php")) $res = @include substr($tmp, 0, ($i + 1))."/../main.inc.php";
// Try main.inc.php using relative path
if (!$res && file_exists("../../main.inc.php")) $res = @include "../../main.inc.php";
if (!$res && file_exists("../../../main.inc.php")) $res = @include "../../../main.inc.php";
if (!$res) die("Include of main fails");

// Define js type
header('Content-Type: application/javascript');
// Important: Following code is to cache this file to avoid page request by browser at each Dolibarr page access.
// You can use CTRL+F5 to refresh your browser cache.
if (empty($dolibarr_nocache)) header('Cache-Control: max-age=3600, public, must-revalidate');
else header('Cache-Control: no-cache');

if (!isset($langs) || !is_object($langs)) // This can occurs when calling page with NOREQUIRETRAN defined, however we need langs for error messages.
{
  include_once DOL_DOCUMENT_ROOT.'/core/class/translate.class.php';
  $langs = new Translate("", $conf);
  $langcode = (GETPOST('lang', 'aZ09', 1) ?GETPOST('lang', 'aZ09', 1) : (empty($conf->global->MAIN_LANG_DEFAULT) ? 'auto' : $conf->global->MAIN_LANG_DEFAULT));
  if (defined('MAIN_LANG_DEFAULT')) $langcode = constant('MAIN_LANG_DEFAULT');
  $langs->setDefaultLang($langcode);
}

//require_once DOL_DOCUMENT_ROOT.'/core/class/translate.class.php';
$langs->loadLangs(array('main',"users", "companies", 'products', 'categories', "agenda", "commercial", "other", "orders", "bills", "compta", "accountancy"));
?>
/* Javascript library of module Batibarrmod */

function modif_statut_propal(id_propal, new_st){
  //alert(id_propal);
  if (confirm("Voulez-vous vraiment modifier le statut ?")) {

    $.ajax({
      url: '<?php echo DIR_HTTP ; ?>custom/batibarrmod/scripts/interface.php'
      ,data: ({mode:'SET_STATUT_PROPAL',id_propal:id_propal, statut:new_st})
      ,contentType: 'application/json; charset=ISO-8859-1'
      ,cache: false
      ,async: false
      ,dataType:'json'
      , success: function(TRes){
        if (TRes['RES']=='OK'){
          //alert(TRes['up']);
          var url = "<?php echo DIR_HTTP ; ?>comm/propal/card.php?id="+id_propal ;
          document.location=url ;
        }
      }
      ,error: function(){
        alert('ERROR');
      }
    });
  }
}
function select_actioncode() {
 $( "#addreminder" ).show();
 $( "#select2-select_offsetunittype_duration-container" ).parent().show();
 $( "#select2-select_actioncommsendmodel_mail-container" ).parent().show();
 $( "#select2-selectremindertype-container" ).parent().show();
 $( ".display_txt_hide" ).remove();

 $( "#actioncode" ).each(function() {
   if ($( this ).val()=="FAC") {

    $( "#addreminder" ).prop( "checked", true );
    $( "#addreminder" ).after("<span class=\"display_txt_hide\"><b>Oui</b></span>");
    $( "#addreminder" ).hide();

    $("#select_offsetunittype_duration").val("i");
    $("#select2-select_offsetunittype_duration-container").attr("title","Minutes");
    $("#select2-select_offsetunittype_duration-container").html("Minutes");
    $( "#select_offsetunittype_duration" ).after("<span class=\"display_txt_hide\"><b>Minutes</b></span>");
    $( "#select2-select_offsetunittype_duration-container" ).parent().hide();

    $("#selectremindertype").val("email");
    $("#select2-selectremindertype-container").attr("title","EMail");
    $("#select2-selectremindertype-container").html("EMail");
    $( "#selectremindertype" ).after("<span class=\"display_txt_hide\"><b>EMail</b></span>");
    $( "#select2-selectremindertype-container" ).parent().hide();
					
    $("#select_actioncommsendmodel_mail").val("emaiil");
    $("#select2-select_actioncommsendmodel_mail-container").attr("title","Notif_facture_a_faire");
    $("#select2-select_actioncommsendmodel_mail-container").html("Notif_facture_a_faire");
    $( "#select_actioncommsendmodel_mail" ).after("<span class=\"display_txt_hide\"><b>Notif_facture_a_faire</b></span>");
    $( "#select2-select_actioncommsendmodel_mail-container" ).parent().hide();

    $("#select_actioncommsendmodel_mail").val("46");
    $("#select2-select_actioncommsendmodel_mail-container").attr("title","Notif_facture_a_faire");
    $("#select2-select_actioncommsendmodel_mail-container").html("Notif_facture_a_faire");
    $("#select_actioncommsendmodel_mail").closest("tr").show();

    $( ".reminderparameters" ).show();
   }
 });
}

function auto_compta_produit() {
  var ind_col_id = -1;
  var ind_col_facture = -1;
  var ind_col_account = -1;
  var ind_col_suggest = -1;
  var ind_col_prod = -1;
  $("tr.liste_titre").each( function() {
    var ind_col = 0 ;
    $( this ).children("th").each( function() {
      if ( $.type( $( this ).children("a").html() ) === "undefined" ) {
        var lib_col = $( this ).html() ;
        //alert(ind_col+' | html: '+lib_col+' | '+$.type( $( this ).children("a").html() )) ;
      }
      else {
        var lib_col = $( this ).children("a").html() ;
        //alert(ind_col+' | a html: '+lib_col+' | '+$.type( $( this ).children("a").html() )) ;
      }

      if (lib_col == "Id ligne") {
        ind_col_id = ind_col;
      }
      else if (lib_col == "<?php echo html_entity_decode($langs->trans('Invoice'), ENT_COMPAT, 'UTF-8') ; ?>") {
        ind_col_facture = ind_col;
      }
      else if (lib_col == "<?php echo html_entity_decode($langs->trans('ProductRef'), ENT_COMPAT, 'UTF-8') ; ?>") {
        ind_col_prod = ind_col;
      }
      else if (lib_col == "<?php echo html_entity_decode($langs->trans('IntoAccount'), ENT_COMPAT, 'UTF-8') ; ?>") {
        ind_col_account = ind_col;
      }
      else if (lib_col == "<?php echo html_entity_decode($langs->trans('AccountAccountingSuggest'), ENT_COMPAT, 'UTF-8') ; ?>") {
        ind_col_suggest = ind_col;
      }
      ind_col++ ;
    }) ;
  }) ;

  //alert('<?php echo html_entity_decode($langs->trans('AccountAccountingSuggest'), ENT_COMPAT, 'UTF-8') ; ?>') ;
  //alert('col_id: '+ind_col_id+' | col_facture: '+ind_col_facture+' | col_prod: '+ind_col_prod+' | col_account: '+ind_col_account+' | col_suggest: '+ind_col_suggest) ;

  var ind_ligne = 0 ;
  $("tr.oddeven").each( function() {
    var code_comptable = 0 ;
    var ind_col = 0 ;
    var id_ligne = 0;
    var url_fact_ligne = "";
    var url_prod_ligne = "";
    var obj_suggest = "";
    $( this ).children("td").each( function() { ;
      if (ind_col==ind_col_id) id_ligne = $( this ).html() ;
      else if (ind_col==ind_col_facture) {
        url_fact_ligne = $( this ).children("a").attr("href") ;
      }
      else if (ind_col==ind_col_prod) {
        url_prod_ligne = $( this ).children("a").attr("href") ;
        if (url_prod_ligne==undefined) url_prod_ligne="" ;
      }
      else if (ind_col==ind_col_account) {
      }
      else if (ind_col==ind_col_suggest) {
        obj_suggest = $( this ) ;
      }
      ind_col++ ;
    }) ;

    //alert('<?php echo DIR_HTTP ; ?>custom/batibarrmod/scripts/interface.php') ;
    if (id_ligne>0 && url_fact_ligne!="" && url_prod_ligne=="") {
					
      $.ajax({
      url: '<?php echo DIR_HTTP ; ?>custom/batibarrmod/scripts/interface.php'
        ,data: ({mode:'GET_CODE_COMPTA_FACTURE',id_line:id_ligne,fact:url_fact_ligne})
        ,contentType: 'application/json; charset=ISO-8859-1'
        ,cache: true
        ,async: false
        ,dataType:'json'
        , success: function(TRes){
          if (TRes["RES"] == "OK") {
            if (TRes["id_code_comptable"] > 0) {
              $("#codeventil"+TRes["id_line"]).val(TRes["id_code_comptable"]);
              $("#select2-codeventil"+TRes["id_line"]+"-container").attr("title",TRes["code_comptable"]+" - "+TRes["lib_code_comptable"]);
              $("#select2-codeventil"+TRes["id_line"]+"-container").html(TRes["code_comptable"]+" - "+TRes["lib_code_comptable"]);
              if (typeof(obj_suggest)=="object") {
                var lib = obj_suggest.html();
                obj_suggest.html(lib+"<br />"+TRes["txt_code_comptable"]+"");
              }
            }
          }
        }
        ,error: function(){
        }
      });
    } ;
    //alert( ind_ligne+" | id: "+id_ligne+" | url facture: "+url_fact_ligne+" | url prod: "+url_prod_ligne ) ;
    ind_ligne++ ;
  }) ;
} // end auto_compta_produit

function agenda_add_other_user() {
  var ind_col_id = -1; // Ref
  var ind_col_owner = -1; // Proprietaire
  $("tr.liste_titre").each( function() {
    var ind_col = 0 ;
    $( this ).children("th").each( function() { ;
      if ($( this ).children("a").html() == "Réf.") {
        ind_col_id = ind_col;
      }
      else if ($( this ).html() == "<?php echo html_entity_decode($langs->trans('Owner'), ENT_COMPAT, 'UTF-8') ; ?>") {
        ind_col_owner = ind_col;
      }
      ind_col++ ;
    }) ;
  }) ;
  //alert("col id: "+ind_col_id+" | col owner: "+ind_col_owner+" | <?php echo html_entity_decode($langs->trans('Owner'), ENT_COMPAT, 'UTF-8') ; ?>");
  
  var ind_ligne = 0 ;
  $("tr.oddeven").each( function() {
    var ind_col = 0 ;
    var url_agenda_ligne = '';
    $( this ).children("td").each( function() { ;
      if (ind_col==ind_col_id) {
        url_agenda_ligne = $( this ).children("a").attr("href") ;
      }
      else if (ind_col==ind_col_owner) {
        if (url_agenda_ligne!='') get_html_user_line(url_agenda_ligne, ind_col_owner, $( this )) ;
        //alert($( this ).html()) ;
      }
      ind_col++ ;
    }) ;
  }) ;
} // end agenda_add_other_user

function get_html_user_line(url_ligne, ind_col_owner, obj_case){
  //alert(url_ligne) ;
  $.ajax({
  url: '<?php echo DIR_HTTP ; ?>custom/batibarrmod/scripts/interface.php'
    ,data: ({mode:'GET_USER_AGENDA',url_ligne:url_ligne})
    ,contentType: 'application/json; charset=ISO-8859-1'
    ,cache: true
    ,async: false
    ,dataType:'json'
    , success: function(TRes){
      if (TRes["RES"] == "OK") {
        //var html_lu = obj_case.html() ;
        obj_case.html(TRes["html"]) ; //html_lu+'<hr>'+
        //alert(TRes["RES"]+' | '+obj_case.html()) ;
      }
    }
    ,error: function(){
    }
  });

} // end get_html_user_line

function docopy(key) {
  //alert(key.value);
  key.select();
  document.execCommand('copy');
}
